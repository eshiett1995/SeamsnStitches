package com.erondu.ngozi.web.interceptors;

import com.erondu.ngozi.web.entity.Admin;
import com.erondu.ngozi.web.service.implementation.CookieService;
import com.erondu.ngozi.web.utility.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;


@Component
public class AdminPageInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    CookieService cookieService;



    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {


        Object storedObject = cookieService.getStoredObject(AppConstants.CookieName,httpServletRequest);

        if(Objects.isNull(storedObject)){

            if(httpServletRequest.getHeader("Referer") == null){

                httpServletResponse.sendRedirect("/");

            }else{

                httpServletResponse.sendRedirect(httpServletRequest.getHeader("Referer"));

            }

            return false; }

        if(storedObject instanceof Admin){

            return true;

        }else{

            if(httpServletRequest.getHeader("Referer") == null){

                httpServletResponse.sendRedirect("/");

            }else{

                httpServletResponse.sendRedirect(httpServletRequest.getHeader("Referer"));

            }

            return false;
        }

    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }


}
