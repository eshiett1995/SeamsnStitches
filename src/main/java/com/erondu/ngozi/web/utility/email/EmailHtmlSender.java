package com.erondu.ngozi.web.utility.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
public class EmailHtmlSender {

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Async("asyncExecutor")
    public EmailStatus send(String to, String subject, String templateName, Context context) {

        String body = templateEngine.process(templateName, context);

        return emailSender.sendHtml(to, subject, body);
    }
}