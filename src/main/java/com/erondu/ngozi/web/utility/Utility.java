package com.erondu.ngozi.web.utility;


import com.erondu.ngozi.web.controller.designers.jsonmodel.UnsentMail;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Utility {

    public static String getFileExtension(File file) {
        String fileName = file.getName();

        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)

            return fileName.substring(fileName.lastIndexOf(".")+1);

        else return "";
    }

    public static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException
    {
        File convFile = new File( multipart.getOriginalFilename());
        multipart.transferTo(convFile);
        return convFile;
    }

    public  static Long hoursDifference(Date firstDate, Date secondDate){

        ZoneId defaultZoneId = ZoneId.of("UTC");

        LocalDateTime oldDate = firstDate.toInstant().atZone(defaultZoneId).toLocalDateTime();

        LocalDateTime presentDate = secondDate.toInstant().atZone(defaultZoneId).toLocalDateTime();

        Duration duration = Duration.between(oldDate, presentDate);

        return Math.abs(duration.toHours());

    }

    public  static Date getPresentDateInUTC(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return dateFormat.parse(dateFormat.format(new Date()));

        } catch (ParseException e) {

            e.printStackTrace();

            return new Date();
        }

    }

    public  static String namesOfNonEmailRecipient(List<UnsentMail> unsentMails){

        String names = "";

        for (UnsentMail unsentMail: unsentMails) {

            if(unsentMails.indexOf(unsentMail) == 0){

                names = unsentMail.getRecipient().getFirstName() + " " + unsentMail.getRecipient().getFirstName();

            }else{

                names = names + ", " + unsentMail.getRecipient().getFirstName() + " " + unsentMail.getRecipient().getLastName();
            }

        }

        return names;

    }

}
