package com.erondu.ngozi.web.utility;

import com.erondu.ngozi.web.utility.annotations.GsonRepellent;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class MyExclusionStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {

        return fieldAttributes.getAnnotation(GsonRepellent.class) != null;

    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }
}
