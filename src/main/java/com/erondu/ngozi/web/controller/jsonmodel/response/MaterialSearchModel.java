package com.erondu.ngozi.web.controller.jsonmodel.response;

import com.erondu.ngozi.web.entity.cloths.Material;
import org.springframework.data.domain.Page;

public class MaterialSearchModel {

    public MaterialSearchModel(Page<Material> materialPage, ResponseModel responseModel){

        this.materialPage = materialPage;

        this.responseModel = responseModel;
    }

    private Page<Material> materialPage;

    private ResponseModel responseModel;

    public Page<Material> getMaterialPage() {
        return materialPage;
    }

    public MaterialSearchModel setMaterialPage(Page<Material> materialPage) {
        this.materialPage = materialPage;
        return this;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public MaterialSearchModel setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
        return this;
    }
}