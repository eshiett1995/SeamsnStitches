package com.erondu.ngozi.web.controller;

import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.service.implementation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * Created by Oto-obong on 11/01/2018.
 */

@Controller
public class SwitchController {

    @Autowired
    ClothingTypeService clothingTypeService;

    @Autowired
    MaterialService materialService;

    @Autowired
    CookieService cookieService;

    @Autowired
    UserService userService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    RatingService ratingService;

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    TaskService taskService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String GambeatNetworkPage() throws IOException {

        return "index";

    }


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String ThisIsTesting() throws IOException {

        return "email/token";

    }

    @RequestMapping(value = "/3", method = RequestMethod.GET)
    public String SecondPage(Model model) throws IOException {


        Page<FashionBrand> fashionBrandPage = fashionBrandService.getFashionBrand(new PageRequest(0, 20));

        for (FashionBrand fashionBrand: fashionBrandPage.getContent()) {

            fashionBrand.setRating(ratingService.getFashionBrandRating(fashionBrand));

            BrandStats brandStats = new BrandStats();

            brandStats.setTotalOrders(ordersService.getTotalOrders(fashionBrand))

                    .setNoEmployees(employeeService.countAllEmployee(fashionBrand));

            fashionBrand.setStats(brandStats);

        }

        model.addAttribute("fashionBrandPage", fashionBrandPage);

        model.addAttribute("fashionbrand", new FashionBrand());

        model.addAttribute("materialPage", materialService.getPaginatedList(new PageRequest(0,20)));

        return "second";

    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String GoHome(Model model) throws IOException {

        Map<String, Object> map = new HashMap<String,Object>();


        map.put("clothingTypes", clothingTypeService.getAllByGenderIsNot(ClothingType.Gender.Unisex));

        map.put("HappyClients", userService.countAll(true));

        map.put("ProjectsCompleted",ordersService.countAllByStatus(Enum.Status.DELIVERED));

        map.put("FashionBrands", fashionBrandService.countAllFashionBrand());

        map.put("Tailors", employeeService.countAllEmployee(true));

        model.addAllAttributes(map);

        return "start";

    }

    @RequestMapping(value = "/designers", method = RequestMethod.GET)
    public String ShowDesigners(Model model) throws IOException {


       Page<FashionBrand> fashionBrandsPage =  fashionBrandService.getFashionBrand(new PageRequest(0, 20));

        for (FashionBrand fashionBrand: fashionBrandsPage.getContent()) {

            fashionBrand.setRating(ratingService.getFashionBrandRating(fashionBrand));

            BrandStats brandStats = new BrandStats();

            brandStats.setTotalOrders(ordersService.getTotalOrders(fashionBrand))

                    .setNoEmployees(employeeService.countAllEmployee(fashionBrand));

            fashionBrand.setStats(brandStats);

        }



        model.addAttribute("FashionBrands",fashionBrandsPage);

        model.addAttribute("FashionBrand", new FashionBrand().setStats(new BrandStats()));



        return "fashionbrands";

    }


    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public String ToRegister() throws IOException {

        return "register";

    }

    @RequestMapping(value = "/menu/1", method = RequestMethod.GET)
    public String ToShowMenu() throws IOException {

        return "menu";

    }



    @RequestMapping(value = "/security/user/forgotten-password", method = RequestMethod.GET)
    public String UserForgottenPassword() throws IOException {

        return "forgotten-password";

    }

    @RequestMapping(value = "/security/user/change-password", method = RequestMethod.GET)
    public String UserChangePassword() throws IOException {

        return "change-password";

    }

    @RequestMapping(value = "/security/designer/forgotten-password", method = RequestMethod.GET)
    public String DesignerForgottenPassword() throws IOException {

        return "designers/forgotten-password";

    }

    @RequestMapping(value = "/security/designer/change-password", method = RequestMethod.GET)
    public String DesignerChangePassword() throws IOException {

        return "designers/change-password";

    }

    @RequestMapping(value = "/showcase", method = RequestMethod.GET)
    public String Show() throws IOException {

        return "viewer";

    }

    @RequestMapping(value = "/design/{gender}/{clothing-type}", method = RequestMethod.GET)
    public String ShowCustomizationPage(@PathVariable("gender") String gender, @PathVariable("clothing-type") String clothing_type, Model model, HttpServletRequest httpServletRequest) throws IOException {

       User serializedUser = (User) this.cookieService.getStoredObject("branded", httpServletRequest);

        Page<FashionBrand> fashionBrandPage;

       if(Objects.isNull(serializedUser) || serializedUser.getId() < 1){

           fashionBrandPage = new PageImpl(Collections.EMPTY_LIST);

       }else{


           fashionBrandPage = fashionBrandService.getFashionBrand(new PageRequest(0, 20));

           for (FashionBrand fashionBrand: fashionBrandPage.getContent()) {

               fashionBrand.setRating(ratingService.getFashionBrandRating(fashionBrand));

               BrandStats brandStats = new BrandStats();

               brandStats.setTotalOrders(ordersService.getTotalOrders(fashionBrand))

                       .setNoEmployees(employeeService.countAllEmployee(fashionBrand));

               fashionBrand.setStats(brandStats);

           }


       }



        model.addAttribute("fashionBrandPage", fashionBrandPage);

        model.addAttribute("fashionbrand", fashionBrandService.getFashionBrand((long) 1));

        model.addAttribute("materialPage", new PageImpl(Collections.EMPTY_LIST));

        return "customization-pages/" + gender.toLowerCase() + "/" + clothing_type.toLowerCase();

    }


    @RequestMapping(value = "/t", method = RequestMethod.GET)
    public String ogin(Model model) throws IOException {


        FashionBrand fashionBrand = fashionBrandService.getFashionBrand((long) 1);

        List<Employee> employeeList = employeeService.getAllEmployeeByFashionBrand(fashionBrand);

        Orders orders = ordersService.getOrderByID((long) 1);

        List<List<Task>> clothModelsTaskList = new ArrayList<>();

        for (ClothModel clothModel: orders.getClothModels() ) {

            List<Task> taskList = taskService.getTasksByClothModel(clothModel);

            clothModelsTaskList.add(taskList);

        }


        model.addAttribute("fashionBrand", fashionBrand);

        model.addAttribute("orders", orders);

        model.addAttribute("employees", employeeList);

        model.addAttribute("clothModelsTaskList", clothModelsTaskList);

        return "viewer/order-viewer";

    }
}
