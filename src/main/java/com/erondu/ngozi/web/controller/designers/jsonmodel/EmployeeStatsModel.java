package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;

public class EmployeeStatsModel {

    private String fullName;

    private double totalTasks;

    private int rating;

    private double completedTask;

    private double pendingTask;

    private ResponseModel responseModel;

    public String getFullName() {
        return fullName;
    }

    public EmployeeStatsModel setFullName(String fullName) {

        this.fullName = fullName;

        return  this;
    }

    public double getTotalTasks() {

        return totalTasks;
    }

    public EmployeeStatsModel setTotalTasks(double totalTasks) {

        this.totalTasks = totalTasks;

        return this;
    }

    public double getCompletedTask() {
        return completedTask;
    }

    public EmployeeStatsModel setCompletedTask(double completedTask) {

        this.completedTask = completedTask;

        return this;
    }

    public double getPendingTask() {
        return pendingTask;
    }

    public EmployeeStatsModel setPendingTask(double pendingTask) {

        this.pendingTask = pendingTask;

        return this;
    }

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public EmployeeStatsModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;
    }

    public int getRating() {
        return rating;
    }

    public EmployeeStatsModel setRating(int rating) {

        this.rating = rating;

        return this;
    }
}
