package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.entity.cloths.ClothingType;

public class RegistrationPricingModel {

    public RegistrationPricingModel(ClothingType clothingType, double price, boolean canDo){

        this.clothingType = clothingType;

        this.price = price;

        this.canDo = canDo;
    }

    private ClothingType clothingType;

    private double price;

    private boolean canDo;

    public ClothingType getClothingType() {
        return clothingType;
    }

    public RegistrationPricingModel setClothingType(ClothingType clothingType) {
        this.clothingType = clothingType;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public RegistrationPricingModel setPrice(double price) {
        this.price = price;
        return this;
    }

    public boolean isCanDo() {
        return canDo;
    }

    public RegistrationPricingModel setCanDo(boolean canDo) {
        this.canDo = canDo;
        return this;
    }
}
