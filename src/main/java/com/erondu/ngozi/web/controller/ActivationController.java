package com.erondu.ngozi.web.controller;


import com.erondu.ngozi.web.entity.Activations;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.User;
import com.erondu.ngozi.web.service.implementation.ActivationService;
import com.erondu.ngozi.web.service.implementation.EmployeeService;
import com.erondu.ngozi.web.service.implementation.UserService;
import com.erondu.ngozi.web.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.rmi.CORBA.Util;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping(value = "/activate")
public class ActivationController {

    @Autowired
    ActivationService activationService;

    @Autowired
    UserService userService;

    @Autowired
    EmployeeService employeeService;


    @RequestMapping(path="/{token}",  method = RequestMethod.GET)
    public String ActivateAccount(Model model,@PathVariable("token") String token) {

        Map<String,String> map = new HashMap<>();

        String fullname = "";

        Activations activations = activationService.GetActivationByToken(token);

        if(Objects.isNull(activations))
        {

            map.put("salutation", "Dear Anonymous");

            map.put("title", "Invalid Activation Code");

            map.put("message", "The activation code supplied is invalid. Please if this URL was sent to your mail, kindly re-register and activate the account with the new URL sent to your mail, within a 1hour duration.");

            model.addAllAttributes(map);

            return "activation-pages/failure";

        }else if(Utility.hoursDifference(activations.getDateCreated(),Utility.getPresentDateInUTC()) > 1){

            if(activations.getRole().equals(Activations.Role.USER)){

                User user = userService.findUserByEmail(activations.getEmail());

                fullname = user.getFirstName() + " " + user.getLastName();

                userService.delete(user);

            }else if(activations.getRole().equals(Activations.Role.DESIGNER)){

                Employee employee = employeeService.findEmployeeByEmail(activations.getEmail());

                fullname = employee.getFirstName() + " " + employee.getLastName();

                employeeService.delete(employee);

            }

            activationService.delete(activations);

            map.put("salutation", String.format("Dear %s", fullname ));

            map.put("title", "Token Has Expired");

            map.put("message", "The activation code supplied has expired. Please if this URL was sent to your mail, kindly re-register and activate the account with the new URL sent to your mail, within a 1hour duration.");

            model.addAllAttributes(map);

            return "activation-pages/failure";


        }else{


            if(activations.getRole().equals(Activations.Role.USER)){

                User user = userService.findUserByEmail(activations.getEmail());

                fullname = user.getFirstName() + " " + user.getLastName();

                user.setActivated(true);

                userService.update(user);


            }else if(activations.getRole().equals(Activations.Role.DESIGNER)){

                Employee employee = employeeService.findEmployeeByEmail(activations.getEmail());

                fullname = employee.getFirstName() + " " + employee.getLastName();

                employee.setActivated(true);

                employeeService.update(employee);

            }

            activationService.delete(activations);

            map.put("salutation", String.format("Dear %s", fullname ));

            map.put("message", String.format("Hello %s, your account for branded has been successfully activated.", fullname ));

            model.addAllAttributes(map);

            return "activation-pages/success";

        }

    }
}
