package com.erondu.ngozi.web.controller.user;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

@Controller
public class UserSwitchController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String ToLogin() throws IOException {

        return "user/login";

    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String ToRegister() throws IOException {

        return "user/register";

    }

}
