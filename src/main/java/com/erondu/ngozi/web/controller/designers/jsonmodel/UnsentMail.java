package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.entity.Employee;

public class UnsentMail {

    private Employee recipient;

    private String errorMessage;

    public Employee getRecipient() {

        return recipient;

    }

    public UnsentMail setRecipient(Employee recipient) {

        this.recipient = recipient;

        return this;
    }

    public String getErrorMessage() {

        return errorMessage;

    }

    public UnsentMail setErrorMessage(String errorMessage) {

        this.errorMessage = errorMessage;

        return this;
    }
}
