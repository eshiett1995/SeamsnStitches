package com.erondu.ngozi.web.controller.jsonmodel.request;

import javax.persistence.Transient;

public class ClothDesignModel {

    private int quantity;

    private double amount;

    private String modelStructure;

    private String savedUrl;


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getModelStructure() {
        return modelStructure;
    }

    public void setModelStructure(String modelStructure) {
        this.modelStructure = modelStructure;
    }

    public String getSavedUrl() {
        return savedUrl;
    }

    public void setSavedUrl(String savedUrl) {
        this.savedUrl = savedUrl;
    }

}
