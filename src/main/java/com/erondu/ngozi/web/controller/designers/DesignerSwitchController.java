package com.erondu.ngozi.web.controller.designers;

import com.erondu.ngozi.web.controller.designers.jsonmodel.*;
import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.service.implementation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(value = "/designer")
public class DesignerSwitchController {

    @Autowired
    ClothingTypeService clothingTypeService;

    @Autowired
    TaskService taskService;

    @Autowired
    RatingService ratingService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    PricingService pricingService;

    @Autowired
    CookieService cookieService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String DesignerRegister(Model model) throws IOException {

        List<RegistrationPricingModel> registrationPricingModelList = new ArrayList<>();

        List<ClothingType> clothingTypeList = new ArrayList<>();

        clothingTypeList = clothingTypeService.getAll();

        for (ClothingType clothingType: clothingTypeList) {

            registrationPricingModelList.add(new RegistrationPricingModel(clothingType, 0, true));

        }

        model.addAttribute("pricingList", registrationPricingModelList);

        return "designers/register";

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String DesignerLogin() throws IOException {

        return "designers/login";

    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String ToShowDashboard(Model model, HttpServletRequest request) throws IOException {

        //Employee employee = (Employee) this.cookieService.getStoredObject("branded", request);

        Employee employee = employeeService.findEmployeeByEmail("eshiett1995@gmail.com");


        ////////////////////////////////////////
        //*********** ORDER MODEL ************//
        ////////////////////////////////////////

        OrderModel orderModel = new OrderModel().setTotalOrders(ordersService.getTotalOrders(employee.getFashionBrand()))
                .setTotalAssigned(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.ASSIGNED))
                .setTotalCompleted(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.COMPLETED))
                .setTotalDelivered(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.DELIVERED))
                .setTotalUnassigned(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.UNASSIGNED));


        ////////////////////////////////////////
        //*********** TASK MODEL ************//
        ////////////////////////////////////////

        TaskModel taskModel = new TaskModel().setTotalCompletedTask(taskService.countByIsCompleted(employee, true))

                .setTotalPendingTask(taskService.countByIsCompleted(employee, false));


        ////////////////////////////////////////
        //*********** RATING MODEL ************//
        ////////////////////////////////////////

        RatingModel ratingModel = new RatingModel().setBrandRating(ratingService.getFashionBrandRating(employee.getFashionBrand()))

                .setDesignerRating(ratingService.getEmployeeRating(employee));

        /**********************************************************************************************/
        /**********************************DASHBOARD MODEL*********************************************/
        /**********************************************************************************************/

        DashboardModel dashboardModel = new DashboardModel().setOrderModel(orderModel)
                .setRatingModel(ratingModel)
                .setTaskModel(taskModel)
                .setEmployee(employee);


        /**********************************************************************************************/
        /********************************** PRICING MODEL *********************************************/
        /**********************************************************************************************/

        List<PricingModel> pricingModelList = new ArrayList<>();

        List<ClothingType> clothingTypeList  = clothingTypeService.getAll();

        for (ClothingType clothingType: clothingTypeList) {

            Pricing foundPricing = pricingService.getPricingByClothingTypeAndFashionBrand(clothingType, employee.getFashionBrand());

            PricingModel pricingModel = new PricingModel();

            pricingModel.setClothingType(clothingType)
                    .setAService(foundPricing == null ? false : true)
                    .setPrice(foundPricing == null ? 0 : foundPricing.getPrice())
                    .setPricing(foundPricing == null ? new Pricing(): foundPricing);

            pricingModelList.add(pricingModel);

        }

        model.addAttribute("dashboardModel", dashboardModel );

        model.addAttribute("pricingModelList", pricingModelList);


        if (employee.getPosition().equals(Employee.POSITION.ADMIN)) {

            return "designers/dashboard";

        }else {return "designers/dashboard2";}

    }

    @RequestMapping(value = "/dashboard/2", method = RequestMethod.GET)
    public String ToShowEmployeeDashboard() throws IOException {

        return "designers/dashboard2";

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String LogoutEmployee(HttpServletRequest request) throws IOException {

        this.cookieService.removesStoredObject("branded", request);

        return "designers/login";

    }


    @RequestMapping(value = "/dashboard/orders", method = RequestMethod.GET)
    public String OrderPage(Model model, HttpServletRequest request) throws IOException {

        //Employee employee = (Employee) this.cookieService.getStoredObject("branded", request);

        Employee employee = employeeService.findEmployeeByEmail("eshiett1995@gmail.com");


        DashboardModel dashboardModel = new DashboardModel()
                .setEmployee(employee);


        /**********************************************************************************************/
        /********************************** PRICING MODEL *********************************************/
        /**********************************************************************************************/

        List<PricingModel> pricingModelList = new ArrayList<>();

        List<ClothingType> clothingTypeList  = clothingTypeService.getAll();

        for (ClothingType clothingType: clothingTypeList) {

            Pricing foundPricing = pricingService.getPricingByClothingTypeAndFashionBrand(clothingType, employee.getFashionBrand());

            PricingModel pricingModel = new PricingModel();

            pricingModel.setClothingType(clothingType)
                    .setAService(foundPricing == null ? false : true)
                    .setPrice(foundPricing == null ? 0 : foundPricing.getPrice())
                    .setPricing(foundPricing == null ? new Pricing(): foundPricing);

            pricingModelList.add(pricingModel);

        }



        model.addAttribute("dashboardModel", dashboardModel );

        model.addAttribute("pricingModelList", pricingModelList);

        model.addAttribute("order", new Orders().setUser(new User()));

        model.addAttribute("employees", employeeService.getAllEmployeeByFashionBrand(employee.getFashionBrand()) );

        model.addAttribute("employee", new Employee());


        if (employee.getPosition().equals(Employee.POSITION.ADMIN)) {

            return "designers/order";

        }else {return  "designers/order";}

    }


    @RequestMapping(value = "/dashboard/employee", method = RequestMethod.GET)
    public String EmployeePage(Model model, HttpServletRequest request) throws IOException {

        //Employee employee = (Employee) this.cookieService.getStoredObject("branded", request);

        Employee employee = employeeService.findEmployeeByEmail("eshiett1995@gmail.com");


        ////////////////////////////////////////
        //*********** ORDER MODEL ************//
        ////////////////////////////////////////

        OrderModel orderModel = new OrderModel().setTotalOrders(ordersService.getTotalOrders(employee.getFashionBrand()))
                .setTotalAssigned(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.ASSIGNED))
                .setTotalCompleted(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.COMPLETED))
                .setTotalDelivered(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.DELIVERED))
                .setTotalUnassigned(ordersService.getTotalOrdersByStatus(employee.getFashionBrand(), Enum.Status.UNASSIGNED));


        ////////////////////////////////////////
        //*********** TASK MODEL ************//
        ////////////////////////////////////////

        TaskModel taskModel = new TaskModel().setTotalCompletedTask(taskService.countByIsCompleted(employee, true))

                .setTotalPendingTask(taskService.countByIsCompleted(employee, false));


        ////////////////////////////////////////
        //*********** RATING MODEL ************//
        ////////////////////////////////////////

        RatingModel ratingModel = new RatingModel().setBrandRating(ratingService.getFashionBrandRating(employee.getFashionBrand()))

                .setDesignerRating(ratingService.getEmployeeRating(employee));

        /**********************************************************************************************/
        /**********************************DASHBOARD MODEL*********************************************/
        /**********************************************************************************************/

        DashboardModel dashboardModel = new DashboardModel().setOrderModel(orderModel)
                .setRatingModel(ratingModel)
                .setTaskModel(taskModel)
                .setEmployee(employee);


        /**********************************************************************************************/
        /********************************** PRICING MODEL *********************************************/
        /**********************************************************************************************/

        List<PricingModel> pricingModelList = new ArrayList<>();

        List<ClothingType> clothingTypeList  = clothingTypeService.getAll();

        for (ClothingType clothingType: clothingTypeList) {

            Pricing foundPricing = pricingService.getPricingByClothingTypeAndFashionBrand(clothingType, employee.getFashionBrand());

            PricingModel pricingModel = new PricingModel();

            pricingModel.setClothingType(clothingType)
                    .setAService(foundPricing == null ? false : true)
                    .setPrice(foundPricing == null ? 0 : foundPricing.getPrice())
                    .setPricing(foundPricing == null ? new Pricing(): foundPricing);

            pricingModelList.add(pricingModel);

        }


        model.addAttribute("dashboardModel", dashboardModel );

        model.addAttribute("pricingModelList", pricingModelList);


        if (employee.getPosition().equals(Employee.POSITION.ADMIN)) {

            return "designers/employee";

        }else {return  "designers/dashboard";}

    }



    @RequestMapping(value = "/dashboard/tasks", method = RequestMethod.GET)
    public String TaskPage(Model model, HttpServletRequest request) throws IOException {

        //Employee employee = (Employee) this.cookieService.getStoredObject("branded", request);

        Employee employee = employeeService.findEmployeeByEmail("eshiett1995@gmail.com");


        DashboardModel dashboardModel = new DashboardModel()
                .setEmployee(employee);


        model.addAttribute("dashboardModel", dashboardModel );

       // model.addAttribute("order", new Orders().setUser(new User()));

      //  model.addAttribute("employees", employeeService.getAllEmployeeByFashionBrand(employee.getFashionBrand()) );

       model.addAttribute("employee", new Employee());


        if (employee.getPosition().equals(Employee.POSITION.ADMIN)) {

            return "designers/tasks";

        }else {return  "designers/order";}

    }


    @RequestMapping(value = "/viewer/{id}", method = RequestMethod.GET)
    public String ThreeDimensionOrderViewer(Model model, @PathVariable long id) throws IOException {


        FashionBrand fashionBrand = fashionBrandService.getFashionBrand((long) 1);

        List<Employee> employeeList = employeeService.getAllEmployeeByFashionBrand(fashionBrand);

        Orders orders = ordersService.getOrderByID(id);

        List<List<Task>> clothModelsTaskList = new ArrayList<>();

        for (ClothModel clothModel: orders.getClothModels() ) {

            List<Task> taskList = taskService.getTasksByClothModel(clothModel);

            clothModelsTaskList.add(taskList);

        }


        model.addAttribute("fashionBrand", fashionBrand);

        model.addAttribute("orders", orders);

        model.addAttribute("employees", employeeList);

        model.addAttribute("clothModelsTaskList", clothModelsTaskList);

        return "viewer/order-viewer";

    }



    }
