package com.erondu.ngozi.web.controller.thirdparty.jsonmodel;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.FashionBrand;

public class GenericPageResponseModel {


    public GenericPageResponseModel(GenericPageModel genericPageModel, ResponseModel responseModel) {

        this.genericPageModel = genericPageModel;

        this.responseModel = responseModel;

    }

    private GenericPageModel genericPageModel;

    private ResponseModel responseModel;


    public GenericPageModel getGenericPageModel() {

        return genericPageModel;

    }

    public void setGenericPageModel(GenericPageModel genericPageModel) {

        this.genericPageModel = genericPageModel;

    }

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public void setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

    }

}
