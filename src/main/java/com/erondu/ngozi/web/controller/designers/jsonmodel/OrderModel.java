package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.Task;

import java.util.ArrayList;
import java.util.List;


public class OrderModel {

    private int totalOrders;

    private int totalDelivered;

    private int totalCompleted;

    private int totalAssigned;

    private int totalUnassigned;

    private Orders order;

    private List<Task> tasks = new ArrayList<>();

    private ResponseModel responseModel;

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public OrderModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;

    }

    public Orders getOrder() {

        return order;

    }

    public OrderModel setOrder(Orders order) {

        this.order = order;

        return this;
    }

    public int getTotalOrders() {
        return totalOrders;
    }

    public OrderModel setTotalOrders(int totalOrders) {

        this.totalOrders = totalOrders;

        return this;
    }

    public int getTotalDelivered() {

        return totalDelivered;
    }

    public OrderModel setTotalDelivered(int totalDelivered) {

        this.totalDelivered = totalDelivered;

        return this;
    }

    public int getTotalCompleted() {

        return totalCompleted;

    }

    public OrderModel setTotalCompleted(int totalCompleted) {

        this.totalCompleted = totalCompleted;

        return this;
    }

    public int getTotalAssigned() {

        return totalAssigned;
    }

    public OrderModel setTotalAssigned(int totalAssigned) {

        this.totalAssigned = totalAssigned;

        return this;
    }

    public int getTotalUnassigned() {

        return totalUnassigned;

    }

    public OrderModel setTotalUnassigned(int totalUnassigned) {

        this.totalUnassigned = totalUnassigned;

        return this;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public OrderModel setTasks(List<Task> tasks) {

        this.tasks = tasks;

        return this;
    }
}
