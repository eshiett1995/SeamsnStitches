package com.erondu.ngozi.web.controller.designers.restcontroller;


import com.erondu.ngozi.web.controller.designers.jsonmodel.EmployeeStatsModel;
import com.erondu.ngozi.web.controller.designers.jsonmodel.OrderModel;
import com.erondu.ngozi.web.controller.designers.jsonmodel.TaskResponseModel;
import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.Task;
import com.erondu.ngozi.web.service.implementation.*;
import com.erondu.ngozi.web.utility.MyExclusionStrategy;
import com.erondu.ngozi.web.utility.datatable.AppUtil;
import com.erondu.ngozi.web.utility.datatable.DataTableRequest;
import com.erondu.ngozi.web.utility.datatable.DataTableResults;
import com.erondu.ngozi.web.utility.datatable.PaginationCriteria;
import com.erondu.ngozi.web.utility.email.EmailHtmlSender;
import com.erondu.ngozi.web.utility.email.EmailStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/designer")
public class DesignerUtilityController {

    @Autowired
    ClothModelService clothModelService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    RatingService ratingService;

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    TaskService taskService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    MailService mailService;

    @Autowired
    ActivationService activationService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    TokenService tokenService;

    @PersistenceContext
    private EntityManager entityManager;

    @RequestMapping(value = "/update/fashionbrand",method = RequestMethod.POST) // //new annotation since 4.3
    public ResponseEntity<ResponseModel> UpdateFashionBrand(@RequestBody FashionBrand fashionBrand){

        FashionBrand brand = fashionBrandService.update(fashionBrand);

        ResponseModel responseModel = new ResponseModel();

        responseModel.setIsSuccessful(true).setResponseMessage("Successful");

        return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/add/employee",method = RequestMethod.POST) // //new annotation since 4.3
    public ResponseEntity<ResponseModel> AddEmployee(@RequestBody Employee employee){

        ResponseModel responseModel = new ResponseModel();

        String token = tokenService.generateToken(10);

        Employee foundEmployee = employeeService.findEmployeeByEmail(employee.getEmail());

        if(Objects.nonNull(foundEmployee)){

            responseModel.setIsSuccessful(false).setResponseMessage("Employee already exist");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);


        }

        while(activationService.TokenExists(token)){

            token = tokenService.generateToken(10);
        }

        EmailStatus emailStatus = emailHtmlSender.send(employee.getEmail(), "Welcome to Branded", "email/token", mailService.ComposeEmployeeActivationMail(employee,token));

        if(emailStatus.isSuccess()){

            employeeService.save(employee);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

        }else {


            responseModel.setIsSuccessful(false).setResponseMessage(emailStatus.getErrorMessage());

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

        }

    }



    @RequestMapping(value = "/query/orders",method = RequestMethod.GET)
    public String QueryOrderTable(HttpServletRequest request){

        DataTableRequest<Orders> dataTableInRQ = new DataTableRequest<Orders>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, amount as amount, status as status, fashionbrand as fashionbrand, user_id as user_id, (SELECT COUNT(1) FROM ORDERS) AS totalrecords  FROM ORDERS";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Orders.class);

        @SuppressWarnings("unchecked")
        List<Orders> ordersList = query.getResultList();



        DataTableResults<Orders> dataTableResult = new DataTableResults<Orders>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(ordersList);
        if (!AppUtil.isObjectEmpty(ordersList)) {
            dataTableResult.setRecordsTotal(String.valueOf(ordersList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(ordersList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(ordersList.size()));
            }
        }

        Gson g = new GsonBuilder()
                .setExclusionStrategies(new MyExclusionStrategy())
                .create();

        return g.toJson(dataTableResult);
    }


    @RequestMapping(value = "/order/{id}",method = RequestMethod.GET)
    ResponseEntity<OrderModel> GetOrder(@PathVariable long id){

        //List<Orders> xx = ordersService.getAll();

        //System.out.print(xx);

        Orders foundOrder = ordersService.getOrderByID(id);

        if(Objects.isNull(foundOrder)){

            return new ResponseEntity<>(new OrderModel().setResponseModel(new ResponseModel(false, "No order found")), HttpStatus.CREATED);
        }


         OrderModel orderModel = new OrderModel();

        //orderModel.setTasks(taskService.getTasksByOrder(foundOrder));

       // if(orderModel.getTasks() == null){

 //           orderModel.setTasks(Collections.emptyList());
   //     }

         orderModel.setOrder(foundOrder).setResponseModel(new ResponseModel(true,"Successful"));

         return new ResponseEntity<>(orderModel, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/query/employees",method = RequestMethod.GET)
    public String QueryEmployeeTable(HttpServletRequest request){

        DataTableRequest<Employee> dataTableInRQ = new DataTableRequest<Employee>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, firstname as firstName, lastname as lastName, email as email, password as password, activated as activated, position as position, fashion_brand_id as fashion_brand_id, (SELECT COUNT(1) FROM EMPLOYEE) AS totalrecords  FROM EMPLOYEE";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Employee.class);

        @SuppressWarnings("unchecked")
        List<Employee> employeeList = query.getResultList();



        DataTableResults<Employee> dataTableResult = new DataTableResults<Employee>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(employeeList);
        if (!AppUtil.isObjectEmpty(employeeList)) {
            dataTableResult.setRecordsTotal(String.valueOf(employeeList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(employeeList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(employeeList.size()));
            }
        }

        Gson g = new GsonBuilder()
                .setExclusionStrategies(new MyExclusionStrategy())
                .create();

        return g.toJson(dataTableResult);
    }

    @RequestMapping(value = "/stats/employee/{id}",method = RequestMethod.POST)
    ResponseEntity<EmployeeStatsModel> GetEmployeeStats(@PathVariable long id){

        Employee foundEmployee = employeeService.getEmployeeByID(id);

        if(Objects.isNull(foundEmployee)){

            return new ResponseEntity<>(new EmployeeStatsModel().setResponseModel(new ResponseModel(false, "No employee found")), HttpStatus.CREATED);
        }


        EmployeeStatsModel employeeStatsModel = new EmployeeStatsModel();

        employeeStatsModel.setFullName(foundEmployee.getFirstName() + " " + foundEmployee.getLastName())
                .setCompletedTask(taskService.countByIsCompleted(foundEmployee, true))
                .setPendingTask(taskService.countByIsCompleted(foundEmployee, false))
                .setTotalTasks(taskService.countTotalTask(foundEmployee))
                .setResponseModel(new ResponseModel(true, "Success"))
                .setRating((int) Math.floor(ratingService.getEmployeeRating(foundEmployee)));

        return new ResponseEntity<>(employeeStatsModel, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/remove/employee",method = RequestMethod.POST)
    ResponseEntity<ResponseModel> RemoveEmployeeFromFashionBrand(@RequestBody Employee employee){

        Employee foundEmployee = employeeService.getEmployeeByID(employee.getId());

        if(Objects.isNull(foundEmployee)){

            return new ResponseEntity<>(new ResponseModel(false, "No employee found"), HttpStatus.CREATED);
        }


        employeeService.delete(foundEmployee);

        return new ResponseEntity<>(new ResponseModel(false, "No employee found"), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/save/tasks/{id}",method = RequestMethod.POST)
    ResponseEntity<ResponseModel> SaveTasks(@RequestBody List<Task> tasks, @PathVariable Long id){

      /**  Orders orders = ordersService.getOrderByID(id);

        List<Task> allPresentTasks = taskService.getTasksByOrder(orders);


        for (Task task: allPresentTasks ) {

            Optional<Task> containedTask = tasks.stream()
                    .filter(o -> o.getId() == task.getId())
                    .findAny();

            if (!containedTask.isPresent()) {

              taskService.delete(task);
            }
        }



        for (Task task: tasks) {

            if(task.getId() == 0){

                task.setCompleted(false);

                task.setOrders(orders);

                taskService.save(task);

            }else{

                Task foundTask = taskService.getTaskByID(task.getId());

                foundTask.setDetails(task.getDetails());

                taskService.save(foundTask);

            }


        }
**/
        return new ResponseEntity<>(new ResponseModel(true, "Task saved successfully"), HttpStatus.CREATED);
    }


    /***************************************************************************/
    /************************TASK FUNCTIONALITY*********************************/
    /***************************************************************************/

    @RequestMapping(value = "/query/task",method = RequestMethod.GET)
    public String QueryTaskTable(HttpServletRequest request){

        DataTableRequest<Task> dataTableInRQ = new DataTableRequest<Task>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, orders_id as orders_id, details as details, completed as completed, employee_id as employee_id, (SELECT COUNT(1) FROM TASK) AS totalrecords  FROM TASK";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Task.class);

        @SuppressWarnings("unchecked")
        List<Task> taskList = query.getResultList();



        DataTableResults<Task> dataTableResult = new DataTableResults<Task>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(taskList);
        if (!AppUtil.isObjectEmpty(taskList)) {
            dataTableResult.setRecordsTotal(String.valueOf(taskList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(taskList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(taskList.size()));
            }
        }

        Gson g = new GsonBuilder()
                .setExclusionStrategies(new MyExclusionStrategy())
                .create();

        return g.toJson(dataTableResult);
    }


    @RequestMapping(value = "/associated/task",method = RequestMethod.POST)
    public ResponseEntity<OrderModel> GetAssociatedTasks(HttpServletRequest request, @RequestBody Orders orders){

        OrderModel orderModel = new OrderModel();

        orderModel.setResponseModel(new ResponseModel(true, "Success"));

        return new ResponseEntity<>(orderModel, HttpStatus.CREATED);

    }

    @RequestMapping(value = "/task/save",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskResponseModel> SaveTask(@RequestBody Task task, HttpServletRequest request){

        try {

            //task.setClothModel(clothModelService.findById(task.getClothModelId()));

            Task savedTask = taskService.save(task);

            return new ResponseEntity<>(new TaskResponseModel(savedTask, new ResponseModel(true, "Successful")), HttpStatus.CREATED);

        }catch (Exception ex){

            return new ResponseEntity<>(new TaskResponseModel(null, new ResponseModel(false, "Ooops!, an error occurred")), HttpStatus.CREATED);

        }
    }


    @RequestMapping(value = "/task/delete",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> DeleteTask(@RequestBody Task task, HttpServletRequest request){

        try {

            taskService.delete(task);

            return new ResponseEntity<>(new ResponseModel(true, "Successful"), HttpStatus.CREATED);

        }catch (Exception ex){

            return new ResponseEntity<>( new ResponseModel(false, "Ooops!, an error occurred"), HttpStatus.CREATED);

        }
    }

}