package com.erondu.ngozi.web.controller.designers.restcontroller;


import com.erondu.ngozi.web.controller.designers.jsonmodel.UnsentMail;
import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.service.implementation.*;
import com.erondu.ngozi.web.utility.Utility;
import com.erondu.ngozi.web.utility.email.EmailHtmlSender;
import com.erondu.ngozi.web.utility.email.EmailStatus;
import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/designer")
public class DesignerWelcomeController {

    @Autowired
    ActivationService activationService;

    @Autowired
    ResponseModel responseModel;

    @Autowired
    CookieService cookieService;

    @Autowired
    TokenService tokenService;

    @Autowired
    MailService mailService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    PricingService pricingService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    FashionBrandService fashionBrandService;

    private static final String UPLOADED_FOLDER = "C:\\Users\\Java\\IdeaProjects\\SeamsnStitches\\src\\main\\resources\\static\\img\\brand-image\\" ;

    @RequestMapping(value = "/register/upload",method = RequestMethod.POST) // //new annotation since 4.3
    public ResponseEntity<ResponseModel> singleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("model") String model,
                                                          RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");

            responseModel.setIsSuccessful(false).setResponseMessage("Please select a file to upload");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED); // return response to client.
        }



        try {

            FashionBrand oldFashionBrand = new Gson().fromJson(model, FashionBrand.class);
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            Path path = Paths.get(UPLOADED_FOLDER + oldFashionBrand.getBrandName() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));

            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

            FashionBrand newFashionBrand = new FashionBrand();

            newFashionBrand.setBrandName(oldFashionBrand.getBrandName())
                    .setImage(oldFashionBrand.getBrandName() + "." + FilenameUtils.getExtension(file.getOriginalFilename()))
                    .setWebsite(oldFashionBrand.getWebsite())
                    .setEmail(oldFashionBrand.getEmail())
                    .setPhoneNumber(oldFashionBrand.getPhoneNumber())
                    .setLocation(oldFashionBrand.getLocation())
                    .setBrandPhrase(oldFashionBrand.getBrandPhrase())
                    .setBankDetail(oldFashionBrand.getBankDetail())
                    .setDateCreated();



            FashionBrand savedFashionBrand = fashionBrandService.save(newFashionBrand);

            for (Pricing pricing : oldFashionBrand.getPricing()) {

                pricing.setFashionBrand(savedFashionBrand);

                pricingService.save(pricing);

            }


            List<UnsentMail> unsentMails = new ArrayList<>();

            for (Employee employee: oldFashionBrand.getEmployees()) {

                String token = tokenService.generateToken(10);

                while(activationService.TokenExists(token)){

                    token = tokenService.generateToken(10);
                }

                EmailStatus emailStatus = emailHtmlSender.send(employee.getEmail(), "Welcome to Branded", "email/token", mailService.ComposeEmployeeActivationMail(employee,token));

                if(emailStatus.isSuccess()){

                employee.setFashionBrand(savedFashionBrand);

                employee.setPosition(Employee.POSITION.ADMIN);

                Employee savedEmployee = employeeService.save(employee);

                savedFashionBrand.setEmployees(savedEmployee);

                Activations activation = new Activations();

                activation.setEmail(employee.getEmail()).setRole(Activations.Role.DESIGNER).setActivationToken(token).setDateCreated();

                activationService.save(activation);

                }else {

                    UnsentMail unsentMail = new UnsentMail();

                    unsentMail.setRecipient(employee).setErrorMessage(emailStatus.getErrorMessage());

                    unsentMails.add(unsentMail);
                }

            }

            if(oldFashionBrand.getEmployees().size() != unsentMails.size()) {

                fashionBrandService.save(savedFashionBrand);


                responseModel.setIsSuccessful(true);

                if (unsentMails.size() == 0) {

                    responseModel.setResponseMessage("The fashion brand and its founder(s)account has been created on branded. The activation link has been sent to each founder's email, as" +

                            "all account's being opened are deactivated until activated. Thank you.");

                } else {

                    responseModel.setResponseMessage(String.format("The fashion brand and its founder(s)account has been created on branded . The activation link has been sent to each founder's email, as" +

                            "all account's being opened are deactivated until activated. There has been some issues with creating accounts for  %s due to an error in sending them the email, founders who have been activated can subsequently add  new founders, Thank you.", Utility.namesOfNonEmailRecipient(unsentMails)));

                }

                return new ResponseEntity<>(responseModel, HttpStatus.CREATED);


            }else{

                responseModel.setIsSuccessful(false)

                        .setResponseMessage("There has been an error in sending the activation mail's to the founders");

                return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

            }


        } catch (IOException e) {

            e.printStackTrace();

            responseModel.setIsSuccessful(false).setResponseMessage("Ooops! connection error");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);
        }

    }


    @RequestMapping(value = "/login",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> EmployeeLogin(@RequestBody Employee employee, HttpServletRequest request, HttpServletResponse response){

        Employee foundEmployee = employeeService.findEmployeeByEmailAndPasswordAndActivated(employee.getEmail(), employee.getPassword(), true);

        if(Objects.nonNull(foundEmployee)){


            // gets the session ID from the request
            HttpSession session = request.getSession(true);

            // Saves the user's userName by sessionID in the Session Database
            session.setAttribute(session.getId(),foundEmployee);

            responseModel.setIsSuccessful(true).setResponseMessage("Successfully Verified");

            // generates the cookie, puts the session id in the cookie and sends it in the response body
            response.addCookie(cookieService.generateCookie(session.getId()));

            return new ResponseEntity<>(responseModel, HttpStatus.OK); // return response to client.

        }else{

            responseModel.setIsSuccessful(false).setResponseMessage("Password/Email is incorrect.");

            return new ResponseEntity<>(responseModel, HttpStatus.OK);
        }
    }


}


