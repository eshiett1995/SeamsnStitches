package com.erondu.ngozi.web.controller.jsonmodel.response;

import com.erondu.ngozi.web.entity.FashionBrand;
import org.springframework.data.domain.Page;

public class BrandSearchModel {

    public BrandSearchModel(Page<FashionBrand> fashionBrandPage, ResponseModel responseModel){

        this.fashionBrandPage = fashionBrandPage;

        this.responseModel = responseModel;

    }

    private Page<FashionBrand> fashionBrandPage;

    private ResponseModel responseModel;

    public Page<FashionBrand> getFashionBrandPage() {

        return fashionBrandPage;

    }

    public BrandSearchModel setFashionBrandPage(Page<FashionBrand> fashionBrandPage) {

        this.fashionBrandPage = fashionBrandPage;

        return this;

    }

    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public BrandSearchModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;
    }
}
