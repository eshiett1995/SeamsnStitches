package com.erondu.ngozi.web.controller.restcontroller;


import com.erondu.ngozi.web.controller.jsonmodel.request.MaterialSearchParamModel;
import com.erondu.ngozi.web.controller.jsonmodel.request.SearchModel;
import com.erondu.ngozi.web.controller.jsonmodel.response.BrandSearchModel;
import com.erondu.ngozi.web.controller.jsonmodel.response.MaterialSearchModel;
import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.BrandStats;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.cloths.Material;
import com.erondu.ngozi.web.repository.FashionBrandRepository;
import com.erondu.ngozi.web.service.implementation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/generic")
public class GenericRestController {

    @Autowired
    FashionBrandRepository fashionBrandRepository;

    @Autowired
    PricingService pricingService;

    @Autowired
    RatingService ratingService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ClothingTypeService clothingTypeService;

    @Autowired
    MaterialService materialService;


    @Autowired
    FashionBrandService fashionBrandService;

    @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/paginated/brand/{page}", method = RequestMethod.POST)
    public ResponseEntity<BrandSearchModel> GetAllFashionBrand(@PathVariable int page , @RequestBody SearchModel searchModel, HttpServletRequest request, HttpServletResponse response)
    {
        Page<FashionBrand> fashionBrandPage;

        if(searchModel.getSearchKey().trim().isEmpty()){


            fashionBrandPage = fashionBrandService.getFashionBrand(new PageRequest(page, 20));


        }else{

            fashionBrandPage = fashionBrandService.getFashionBrand(searchModel.getSearchKey(), new PageRequest(page, 20));

        }

        for (FashionBrand fashionBrand: fashionBrandPage.getContent()) {

            fashionBrand.setRating(ratingService.getFashionBrandRating(fashionBrand));

            fashionBrand.setPricing(pricingService.getAllByFashionBrand(fashionBrand));

            BrandStats brandStats = new BrandStats();

            brandStats.setTotalOrders(ordersService.getTotalOrders(fashionBrand))

                    .setNoEmployees(employeeService.countAllEmployee(fashionBrand));

            fashionBrand.setStats(brandStats);

        }

        if(fashionBrandPage.getContent().isEmpty()){

            return new ResponseEntity<BrandSearchModel>(new BrandSearchModel(fashionBrandPage, new ResponseModel(true, "No fashion brand has been found")), HttpStatus.OK);

        }else{

            return new ResponseEntity<BrandSearchModel>(new BrandSearchModel(fashionBrandPage, new ResponseModel(true, "Success")), HttpStatus.OK);

        }


    }


    @RequestMapping(value = "brand/paginated/{clothTypeId}/{page}/{latitude}/{longitude}/{distance}", method = RequestMethod.GET)
    public ResponseEntity<BrandSearchModel> GetFashionBrandsByDistance(   @PathVariable int clothTypeId,
                                                                          @PathVariable int page,
                                                                          @PathVariable double latitude,
                                                                          @PathVariable double longitude,
                                                                          @PathVariable double distance,
                                                                          HttpServletRequest request,
                                                                          HttpServletResponse response)
    {

        try {

            Page<FashionBrand> fashionBrandPage = fashionBrandService.getFashionBrandByDistance(clothTypeId, longitude, latitude, distance , new PageRequest(page, 20));

            for (FashionBrand fashionBrand: fashionBrandPage.getContent()) {

                fashionBrand.setRating(ratingService.getFashionBrandRating(fashionBrand));

                BrandStats brandStats = new BrandStats();

                brandStats.setTotalOrders(ordersService.getTotalOrders(fashionBrand))

                        .setNoEmployees(employeeService.countAllEmployee(fashionBrand));

                fashionBrand.setStats(brandStats);

            }

            if(fashionBrandPage.getContent().isEmpty() && page == 0){ //means the search result was empty as first page has empty result

                return new ResponseEntity<>(new BrandSearchModel(fashionBrandPage, new ResponseModel(true, "No fashion brand was found that fits the searched parameter")), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new BrandSearchModel(fashionBrandPage, new ResponseModel(true, "Successful")), HttpStatus.OK);

            }


        }catch (Exception exception){

            return new ResponseEntity<>(new BrandSearchModel(null, new ResponseModel(false, "An error occurred")), HttpStatus.OK);

        }

    }

    @RequestMapping(value = "/paginated/material/{clothTypeId}/{fashionBrandId}/{page}", method = RequestMethod.GET)
    public ResponseEntity<MaterialSearchModel> GetAllMaterials(@PathVariable int clothTypeId, @PathVariable int fashionBrandId,  @PathVariable int page, HttpServletRequest request, HttpServletResponse response)
    {

        try {


            Page<Material> materialPage = materialService.getMaterial(fashionBrandService.getFashionBrand((long) fashionBrandId), clothingTypeService.getClothingType((long) clothTypeId), new PageRequest(page, 20));

            if(materialPage.getContent().isEmpty()){

                return new ResponseEntity<>(new MaterialSearchModel(materialPage, new ResponseModel(true, "No material found for Fashion Brand")), HttpStatus.OK);

            }else{

                return new ResponseEntity<>(new MaterialSearchModel(materialPage, new ResponseModel(true, "Material has been updated")), HttpStatus.OK);

            }

        }catch (Exception exception){

            return new ResponseEntity<>(new MaterialSearchModel(null, new ResponseModel(false, "An error occurred")), HttpStatus.OK);

        }

    }


    @RequestMapping(value = "/it", method = RequestMethod.GET)
    public ResponseEntity<FashionBrand> ede(HttpServletRequest httpServletRequest)
    {

        for (Cookie cookie: httpServletRequest.getCookies() ) {

            System.out.println(cookie.getValue());

        }

        return new ResponseEntity<>(fashionBrandService.getFashionBrand((long) 1), HttpStatus.OK);


    }

    @RequestMapping(value = "/eat", method = RequestMethod.GET)
    public ResponseEntity<Page<FashionBrand>> edemm(HttpServletRequest httpServletRequest)
    {

            Page<FashionBrand> fashionBrands = fashionBrandRepository.getFashionMe(new PageRequest(0, 5));

            return new ResponseEntity<>(fashionBrands, HttpStatus.OK);

    }
}
