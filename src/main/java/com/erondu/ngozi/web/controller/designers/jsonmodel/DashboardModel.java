package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Pricing;
import com.erondu.ngozi.web.entity.Services;
import org.hibernate.criterion.Order;

public class DashboardModel {

    private Employee employee;

    private OrderModel orderModel;

    private RatingModel ratingModel;

    private TaskModel taskModel;

    public Employee getEmployee() {

        return employee;

    }

    public DashboardModel setEmployee(Employee employee) {

        this.employee = employee;

        return this;

    }

    public OrderModel getOrderModel() {

        return orderModel;

    }

    public DashboardModel setOrderModel(OrderModel orderModel) {

        this.orderModel = orderModel;

        return this;
    }

    public RatingModel getRatingModel() {

        return ratingModel;

    }

    public DashboardModel setRatingModel(RatingModel ratingModel) {

        this.ratingModel = ratingModel;

        return this;
    }

    public TaskModel getTaskModel() {

        return taskModel;

    }

    public DashboardModel setTaskModel(TaskModel taskModel) {

        this.taskModel = taskModel;

        return this;
    }
}
