package com.erondu.ngozi.web.controller.jsonmodel.request;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.cloths.ClothingType;

import java.util.Objects;



/**
 *  This class accepts a requests of the fashion brand and
 *  cloth types (trouser, shirts), so it can search for
 *  the appropriate material to supply to the front end
 */

public class MaterialSearchParamModel {



    private FashionBrand fashionBrand;

    private ClothingType clothingType;

    public FashionBrand getFashionBrand() {
        return fashionBrand;
    }

    public MaterialSearchParamModel setFashionBrand(FashionBrand fashionBrand) {
        this.fashionBrand = fashionBrand;
        return this;
    }

    public ClothingType getClothingType() {
        return clothingType;
    }

    public MaterialSearchParamModel setClothingType(ClothingType clothingType) {
        this.clothingType = clothingType;
        return this;
    }
}
