package com.erondu.ngozi.web.controller.admin;


import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.service.implementation.FibreService;
import com.erondu.ngozi.web.service.implementation.WeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin")
public class AdminSwitchController {

    @Autowired
    FibreService fibreService;

    @Autowired
    WeaveService weaveService;


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String DesignerLogin() throws IOException {

        return "admin/login";

    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String DesignerRegister() throws IOException {

       return "admin/dashboard";

    }

    @RequestMapping(value = "/dashboard/fibre", method = RequestMethod.GET)
    public String ViewFibre() throws IOException {



        return "admin/fibre";

    }


    @RequestMapping(value = "/dashboard/weave", method = RequestMethod.GET)
    public String ViewWeave() throws IOException {

        return "admin/weave";

    }


    @RequestMapping(value = "/dashboard/material", method = RequestMethod.GET)
    public String ViewMaterial(Model model) throws IOException {

        Map<String,Object> map = new HashMap<String,Object>();

        map.put("fibres", fibreService.getAll());

        map.put("weaves", weaveService.getAll());

        List<String> arrayListOfNames = new ArrayList<>();

        for (Enum.ClothStyle clothStyle : Enum.ClothStyle.values()) {

            arrayListOfNames.add(clothStyle.getValue());
        }

        map.put("clothStyles", arrayListOfNames);

        model.addAllAttributes(map);

        return "admin/material";

    }
}
