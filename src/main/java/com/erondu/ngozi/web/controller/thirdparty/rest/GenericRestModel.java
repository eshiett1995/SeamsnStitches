package com.erondu.ngozi.web.controller.thirdparty.rest;


import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.controller.thirdparty.jsonmodel.*;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Transaction;
import com.erondu.ngozi.web.entity.User;
import com.erondu.ngozi.web.service.implementation.EmployeeService;
import com.erondu.ngozi.web.service.implementation.FashionBrandService;
import com.erondu.ngozi.web.service.implementation.TransactionService;
import com.erondu.ngozi.web.service.implementation.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Objects;

@RestController
@RequestMapping(value = "rest/third-party")
public class GenericRestModel {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    UserService userService;


    @RequestMapping(value = "/login",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeModel> EmployeeLogin(@RequestBody LoginModel employee, HttpServletRequest request, HttpServletResponse response){

        Employee foundEmployee = employeeService.findEmployeeByEmailAndPasswordAndActivated(employee.getEmail(), employee.getPassword(), true);

        if(Objects.nonNull(foundEmployee)){

            return new ResponseEntity<>(new EmployeeModel(foundEmployee, new ResponseModel(true,"Successfully Verified")), HttpStatus.OK); // return response to client.

        }else{

            return new ResponseEntity<>(new EmployeeModel(null, new ResponseModel(true,"Password/Email is incorrect.")), HttpStatus.OK); // return response to client.

        }
    }

    @RequestMapping(value = "pageable/transactions/{brand-id}/{page-number}",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericPageResponseModel> GetPageableTransactions(@PathVariable("brand-id") long fashionBrandId, @PathVariable("page-number") int pageNumber, @RequestBody TransactionsSearchParams transactionsSearchParams, HttpServletResponse response){

        Page<Transaction> transactionPage = new PageImpl<>(Collections.emptyList());

        try {

            if(Objects.isNull(transactionsSearchParams.getEmail()) && Objects.isNull(transactionsSearchParams.getDate())) {

                transactionPage = transactionService.getTransactionByOrdersFashionBrand(fashionBrandService.getFashionBrand(fashionBrandId), new PageRequest(pageNumber, 20));

            }else if(!Objects.isNull(transactionsSearchParams.getEmail()) || Objects.isNull(transactionsSearchParams.getDate())){

                transactionPage = transactionService.getTransactionByOrdersUserEmail(transactionsSearchParams.getEmail(), new PageRequest(pageNumber, 20));

            }else if(Objects.isNull(transactionsSearchParams.getEmail()) || !Objects.isNull(transactionsSearchParams.getDate())){

                transactionPage = transactionService.getTransactionByDate(transactionsSearchParams.getDate(), new PageRequest(pageNumber, 20));

            }

            GenericPageModel<Transaction> genericPageModel = new GenericPageModel();

            genericPageModel.setContent(transactionPage.getContent())
                    .setCurrentPageNumber(transactionPage.getNumber())
                    .setFirst(transactionPage.isFirst())
                    .setGetCurrentPageSize(transactionPage.getSize())
                    .setHasNext(transactionPage.hasNext())
                    .setLast(transactionPage.isLast())
                    .setTotalNumberOfElements(transactionPage.getTotalPages())
                    .setTotalNumberOfPages(transactionPage.getTotalPages());

            return new ResponseEntity<>(new GenericPageResponseModel(genericPageModel, new ResponseModel(true, "Successfully Verified")), HttpStatus.OK); // return response to client.

        }catch (Exception exception){

            return new ResponseEntity<>(new GenericPageResponseModel(new GenericPageModel(), new ResponseModel(false, "Oops! an error occurred, please try again.")), HttpStatus.OK); // return response to client.

        }
    }


    @RequestMapping(value = "/employee/{email}",method = RequestMethod.GET)
    public ResponseEntity<UserModel> GetUser(@PathVariable("email") String email){

        User foundUser = userService.findUserByEmail(email);

        if(Objects.nonNull(foundUser)){

            if(foundUser.isActivated()) {

                return new ResponseEntity<>(new UserModel(foundUser, new ResponseModel(true, "Successfully Verified")), HttpStatus.OK); // return response to client.

            }else{

                return new ResponseEntity<>(new UserModel(foundUser, new ResponseModel(false, "User found, but not account activated")), HttpStatus.OK); // return response to client.

            }

        }else{

            return new ResponseEntity<>(new UserModel(null, new ResponseModel(true,"Password/Email is incorrect.")), HttpStatus.OK); // return response to client.

        }
    }

}
