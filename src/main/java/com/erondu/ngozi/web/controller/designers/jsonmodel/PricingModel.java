package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.entity.ClothModel;
import com.erondu.ngozi.web.entity.Pricing;
import com.erondu.ngozi.web.entity.cloths.ClothingType;

public class PricingModel {

    private ClothingType clothingType;

    private Pricing pricing;

    private boolean isAService;

    private double price;


    public ClothingType getClothingType() {

        return clothingType;

    }

    public PricingModel setClothingType(ClothingType clothingType) {

        this.clothingType = clothingType;

        return this;

    }

    public Pricing getPricing() {

        return pricing;

    }

    public void setPricing(Pricing pricing) {

        this.pricing = pricing;

    }

    public boolean isAService() {

        return isAService;

    }

    public PricingModel setAService(boolean AService) {

        isAService = AService;

        return this;

    }

    public double getPrice() {

        return price;

    }

    public PricingModel setPrice(double price) {

        this.price = price;

        return this;
    }

}
