package com.erondu.ngozi.web.controller.jsonmodel.request;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Transaction;

public class TransactionModel {

    public TransactionModel(Transaction transaction, ResponseModel responseModel){

        this.transaction = transaction;

        this.responseModel = responseModel;

    }

    private Transaction transaction;

    private ResponseModel responseModel;

    public Transaction getTransaction() {
        return transaction;
    }

    public TransactionModel setTransaction(Transaction transaction) {
        this.transaction = transaction;
        return this;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public TransactionModel setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
        return this;
    }
}
