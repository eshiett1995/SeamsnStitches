package com.erondu.ngozi.web.controller.restcontroller;


import com.erondu.ngozi.web.controller.jsonmodel.email.ReceiptModel;
import com.erondu.ngozi.web.controller.jsonmodel.email.SingleProductModel;
import com.erondu.ngozi.web.controller.jsonmodel.request.ClothDesignModel;
import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.controller.jsonmodel.request.TransactionModel;
import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.entity.cloths.Material;
import com.erondu.ngozi.web.service.implementation.*;
import com.erondu.ngozi.web.utility.email.EmailHtmlSender;
import com.erondu.ngozi.web.utility.email.EmailStatus;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping(value = "/transaction")
public class TransactionController {

    @Autowired
    CookieService cookieService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    ClothingTypeService clothingTypeService;

    @Autowired
    UserService userService;

    @Autowired
    MailService mailService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    private static final String UPLOADED_FOLDER = "C:\\Users\\Java\\IdeaProjects\\SeamsnStitches\\src\\main\\resources\\static\\js\\vendor\\branded\\orders\\" ;

    /**@RequestMapping(path="/save",  method = RequestMethod.POST)
    public ResponseEntity<TransactionModel> SaveTransaction(@RequestBody Orders orders, HttpServletRequest request){

        User deSerializedUser = new User();

        FashionBrand fashionBrand = fashionBrandService.getFashionBrand((long) 1);

        try {

            //TODO save json files of model

           /** User serializedUser = (User) this.cookieService.getStoredObject("branded", request);

            if(Objects.isNull(serializedUser)){

                deSerializedUser = userService.findUserByEmail("eshieyy@gmail.com");

            }else{

                deSerializedUser = userService.findUserByEmail(serializedUser.getEmail());

                orders.setUser(deSerializedUser);

            }


            orders.setFashionBrand(fashionBrand);

            orders.setUser(deSerializedUser);

            for(int index = 0 ; index < orders.getClothModels().size(); index ++){


                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

                String name = orders.getUser().getFirstName() + orders.getUser().getFirstName() + "-" + dt.format(new Date()) + "-(order" + (index + 1) +")";

                File targetFile = new File(UPLOADED_FOLDER, name.replace(" " , "") + ".json");

                try (FileWriter file = new FileWriter(targetFile)) {

                    file.write(orders.getClothModels().get(0).getModelStructure());

                    file.flush();

                    orders.getClothModels().get(index).setSavedUrl(name + ".json");

                } catch (IOException e) {

                    e.printStackTrace();
                }


            }

            Orders savedOrder = ordersService.save(orders);

            Transaction transaction = new Transaction();

            transaction.setOrders(savedOrder).setAmount(5000)
                    .setSuccessful(true)
                    .setUser(deSerializedUser)
                    .setDateCreated();

            Transaction savedTransaction = transactionService.save(transaction);

            TransactionModel transactionModel = new TransactionModel(savedTransaction, new ResponseModel(true, "Successful"));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK); **/

           // String name = "badguy";
            /**for(int index = 0 ; index < orders.getClothModels().size(); index ++) {

                File targetFile = new File(UPLOADED_FOLDER, name.replace(" ", "") + ".json");

                try (FileWriter file = new FileWriter(targetFile)) {

                    file.write(orders.getClothModels().get(index).getModelStructure());

                    file.flush();

                    //orders.getClothModels().get(index).setSavedUrl(name + ".json");

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            TransactionModel transactionModel = new TransactionModel(new Transaction(), new ResponseModel(true, "Successful"));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK);


        }catch (Exception ex){

            return new ResponseEntity<>(new TransactionModel(new Transaction(), new ResponseModel(false, "Oops! connection error")), HttpStatus.OK);

        }
    }**/

    @RequestMapping(path="/save",  method = RequestMethod.POST)
    public ResponseEntity<TransactionModel> SaveTransaction(@RequestBody List<ClothDesignModel> clothDesignModels, HttpServletRequest request){

        try {

        User deSerializedUser;

        User serializedUser = (User) this.cookieService.getStoredObject("branded", request);

        if(Objects.isNull(serializedUser)){

            // todo over here send the client a message that they meed to logg in
            //deSerializedUser = userService.findUserByEmail("eshieyy@gmail.com");

            TransactionModel transactionModel = new TransactionModel(new Transaction(null, 0, false), new ResponseModel(true, "Please login"));

            return new ResponseEntity<>(transactionModel, HttpStatus.OK);

        }else {

            deSerializedUser = userService.findUserByEmail(serializedUser.getEmail());

        }

        /*****************************************************************************************/
        /*************************** CREATE A RECEIPT EMAIL **************************************/
        /*****************************************************************************************/

        ReceiptModel receiptModel = new ReceiptModel();

        List<SingleProductModel> products = new ArrayList<>();

        double cumulativeTotal = 0;

        for (ClothDesignModel clothDesignModel: clothDesignModels) {
            JSONObject jsonObject = new JSONObject(clothDesignModel.getModelStructure());

            SingleProductModel singleProductModel = new SingleProductModel();

            Material material =  new Gson().fromJson(jsonObject.getString("Material"), Material.class);

            singleProductModel.setProductName(material.getName() + ", " + material.getColor() + " " + jsonObject.getString("ClothType"))
                    .setCumulativePrice(clothDesignModel.getAmount() * clothDesignModel.getQuantity())
                    .setImageUrl("okkefokefo")
                    .setPrice(clothDesignModel.getAmount())
                    .setQuantity(clothDesignModel.getQuantity());

            products.add(singleProductModel);

            cumulativeTotal = cumulativeTotal + clothDesignModel.getAmount();

        }

        receiptModel.setDelivery(1000)
                .setProducts(products)
                .setTotal(cumulativeTotal);

        EmailStatus emailStatus = emailHtmlSender.send(deSerializedUser.getEmail(), "Welcome to Branded", "email/receipt", mailService.ComposeCustomerReceiptMail(products));

           /** if(emailStatus.isSuccess()){

                System.out.println("it is successful");

            }else {

                System.out.println("it has error");

            } **/


        /*****************************************************************************************/
        /******************************** END OF METHOD ******************************************/
        /*****************************************************************************************/


        List<Orders> ordersList = new ArrayList<>();

        for (ClothDesignModel clothDesignModel: clothDesignModels) {

            if(ordersList.isEmpty()){

                try {

                  JSONObject jsonObject = new JSONObject(clothDesignModel.getModelStructure());

                  Orders order = new Orders();

                  FashionBrand fashionBrand =  new Gson().fromJson(jsonObject.getString("FashionBrand"), FashionBrand.class);


                   ClothModel clothModel =  new ClothModel().setAmount(clothDesignModel.getAmount())
                            .setClothingType(clothingTypeService.getClothingTypeByNameAndGender(jsonObject.getString("ClothType"), ClothingType.Gender.valueOf(jsonObject.getString("Gender"))))
                            .setModelStructure(clothDesignModel.getModelStructure())
                            .setQuantity(clothDesignModel.getQuantity());

                   clothModel.setDateCreated();


                  order.setFashionBrand(fashionBrand)
                          .setClothModels(clothModel);

                  ordersList.add(order);

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }else{

                try {
                    JSONObject jsonObject = new JSONObject(clothDesignModel.getModelStructure());

                    FashionBrand fashionBrand =  new Gson().fromJson(jsonObject.getString("FashionBrand"), FashionBrand.class);

                    Orders foundOrder = ordersList.stream()
                            .filter(order -> fashionBrand.getBrandName().equals(order.getFashionBrand().getBrandName()))
                            .findFirst()
                            .orElse(null);

                    if(!Objects.isNull(foundOrder)){

                       ClothModel clothModel = new ClothModel().setAmount(clothDesignModel.getAmount())
                                .setClothingType(clothingTypeService.getClothingTypeByNameAndGender(jsonObject.getString("ClothType"), ClothingType.Gender.valueOf(jsonObject.getString("Gender"))))
                                .setModelStructure(clothDesignModel.getModelStructure())
                                .setQuantity(clothDesignModel.getQuantity());

                       clothModel.setDateCreated();


                        int indexOfFoundOrder = ordersList.indexOf(foundOrder);

                        foundOrder.setClothModels(clothModel);

                        ordersList.add(indexOfFoundOrder, foundOrder);


                    }else{

                        ClothModel clothModel = new ClothModel().setAmount(clothDesignModel.getAmount())
                                .setClothingType(clothingTypeService.getClothingTypeByNameAndGender(jsonObject.getString("ClothType"), ClothingType.Gender.valueOf(jsonObject.getString("Gender"))))
                                .setModelStructure(clothDesignModel.getModelStructure())
                                .setQuantity(clothDesignModel.getQuantity());

                        clothModel.setDateCreated();

                        foundOrder.setFashionBrand(fashionBrand)
                                .setClothModels(clothModel);

                        ordersList.add(foundOrder);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

        double totalAmountForTransaction = 0;

        for (Orders order: ordersList) {

            double totalAmountForFashionBrand = 0;

            order.setUser(deSerializedUser);

            for(int index = 0 ; index < order.getClothModels().size(); index ++){

                totalAmountForFashionBrand =  totalAmountForFashionBrand + order.getClothModels().get(index).getAmount();

                totalAmountForTransaction = totalAmountForTransaction + order.getClothModels().get(index).getAmount();

                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

                String name = order.getUser().getFirstName() + order.getUser().getLastName() + "-" + dt.format(new Date()) + "-(order" + (index + 1) +")";

                File targetFile = new File(UPLOADED_FOLDER, name.replace(" " , "") + ".json");

                try (FileWriter file = new FileWriter(targetFile)) {

                    file.write(order.getClothModels().get(0).getModelStructure());

                    file.flush();

                    order.getClothModels().get(index).setSavedUrl(name + ".json");

                } catch (IOException e) {

                    e.printStackTrace();
                }


            }

            Orders savedOrder = ordersService.save(order);

            Transaction transaction = new Transaction();

            transaction.setOrders(savedOrder).setAmount(totalAmountForFashionBrand)
                    .setSuccessful(true)
                    .setUser(deSerializedUser)
                    .setDateCreated();

            transactionService.save(transaction);

        }

        TransactionModel transactionModel = new TransactionModel(new Transaction(deSerializedUser, totalAmountForTransaction, true), new ResponseModel(true, "Successful"));

        return new ResponseEntity<>(transactionModel, HttpStatus.OK);





    }catch (Exception ex){

          return new ResponseEntity<>(new TransactionModel(new Transaction(), new ResponseModel(false, "Oops! connection error")), HttpStatus.OK);

    }

}


}
