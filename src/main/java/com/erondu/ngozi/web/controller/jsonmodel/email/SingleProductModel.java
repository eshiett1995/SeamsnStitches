package com.erondu.ngozi.web.controller.jsonmodel.email;

public class SingleProductModel {


    private String productName;

    private String imageUrl;

    private double price;

    private int quantity;

    private double cumulativePrice;

    public String getProductName() {

        return productName;

    }

    public SingleProductModel setProductName(String productName) {

        this.productName = productName;

        return this;
    }

    public String getImageUrl() {

        return imageUrl;

    }

    public SingleProductModel setImageUrl(String imageUrl) {

        this.imageUrl = imageUrl;

        return this;
    }

    public double getPrice() {
        return price;
    }

    public SingleProductModel setPrice(double price) {

        this.price = price;

        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public SingleProductModel setQuantity(int quantity) {

        this.quantity = quantity;

        return this;
    }

    public double getCumulativePrice() {
        return cumulativePrice;
    }

    public SingleProductModel setCumulativePrice(double cumulativePrice) {

        this.cumulativePrice = cumulativePrice;

        return this;
    }
}
