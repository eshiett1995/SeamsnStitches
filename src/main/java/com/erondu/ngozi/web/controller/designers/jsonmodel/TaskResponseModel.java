package com.erondu.ngozi.web.controller.designers.jsonmodel;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Task;

public class TaskResponseModel
{


    public TaskResponseModel(Task task, ResponseModel responseModel) {

        this.task = task;

        this.responseModel = responseModel;

    }

    private Task task;

    private ResponseModel responseModel;

    public Task getTask() {

        return task;

    }

    public TaskResponseModel setTask(Task task) {

        this.task = task;

        return this;

    }


    public ResponseModel getResponseModel() {

        return responseModel;

    }

    public TaskResponseModel setResponseModel(ResponseModel responseModel) {

        this.responseModel = responseModel;

        return this;
    }
}
