package com.erondu.ngozi.web.controller.admin.restcontroller;


import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Admin;
import com.erondu.ngozi.web.service.implementation.AdminService;
import com.erondu.ngozi.web.service.implementation.CookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;


@RestController
@RequestMapping(value = "/admin")
public class AdminWelcomeController {

    @Autowired
    AdminService adminService;

    @Autowired
    CookieService cookieService;


    @RequestMapping(value = "/login",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseModel> EmployeeLogin(@RequestBody Admin admin, HttpServletRequest request, HttpServletResponse response){

        Admin foundAdmin = adminService.findAdminByEmailAndPassword(admin.getEmail(), admin.getPassword());

        if(Objects.nonNull(foundAdmin)){

            // gets the session ID from the request
            HttpSession session = request.getSession(true);

            // Saves the user's userName by sessionID in the Session Database
            session.setAttribute(session.getId(),foundAdmin);

            // generates the cookie, puts the session id in the cookie and sends it in the response body
            response.addCookie(cookieService.generateCookie(session.getId()));

            return new ResponseEntity<>(new ResponseModel(true,"Successfully Verified"), HttpStatus.OK); // return response to client.

        }else{

            return new ResponseEntity<>(new ResponseModel(false,"Password/Email is incorrect."), HttpStatus.OK);
        }
    }


}
