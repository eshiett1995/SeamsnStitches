package com.erondu.ngozi.web.controller.designers.jsonmodel;

public class RatingModel {

    private double brandRating;

    private double designerRating;

    public double getBrandRating() {
        return brandRating;
    }

    public RatingModel setBrandRating(double brandRating) {
        this.brandRating = brandRating;
        return this;
    }

    public double getDesignerRating() {
        return designerRating;
    }

    public RatingModel setDesignerRating(double designerRating) {
        this.designerRating = designerRating;
        return this;
    }
}
