package com.erondu.ngozi.web.controller.jsonmodel.email;

import java.util.List;

public class ReceiptModel {

    private List<SingleProductModel> products;

    private double delivery;

    private double total;

    public List<SingleProductModel> getProducts() {

        return products;

    }

    public ReceiptModel setProducts(List<SingleProductModel> products) {

        this.products = products;

        return this;

    }

    public double getDelivery() {

        return delivery;

    }

    public ReceiptModel setDelivery(double delivery) {

        this.delivery = delivery;

        return this;

    }

    public double getTotal() {

        return total;

    }

    public ReceiptModel setTotal(double total) {

        this.total = total;

        return this;
    }
}
