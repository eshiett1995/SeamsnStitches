package com.erondu.ngozi.web.controller.user.restcontroller;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Activations;
import com.erondu.ngozi.web.entity.User;
import com.erondu.ngozi.web.service.implementation.*;
import com.erondu.ngozi.web.utility.email.EmailHtmlSender;
import com.erondu.ngozi.web.utility.email.EmailStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping(value = "/welcome")
public class WelcomeController {

    @Autowired
    CookieService cookieService;

    @Autowired
    TokenService tokenService;

    @Autowired
    EmailHtmlSender emailHtmlSender;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);


    @Autowired
    ActivationService activationService;

    @Autowired
    MailService mailService;

    @Autowired
    UserService userService;

    @Autowired
    ResponseModel responseModel;




    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> SignIn(@Valid @RequestBody User user, HttpServletRequest request, HttpServletResponse response, BindingResult result)
    {

    try{

        User foundUser = userService.findUserByEmailAndPassword(user.getEmail(), user.getPassword());

        if(Objects.nonNull(foundUser)) {

            // gets the session ID from the request
            HttpSession session = request.getSession(true);

            // Saves the user's userName by sessionID in the Session Database
            session.setAttribute(session.getId(),foundUser);

            // generates the cookie, puts the session id in the cookie and sends it in the response body
            response.addCookie(cookieService.generateCookie(session.getId()));

            responseModel.setIsSuccessful(true)

                    .setResponseMessage("Successful");

        }else{

            responseModel.setIsSuccessful(false)
                    .setResponseMessage("No user found");

        }

        return new ResponseEntity<>(responseModel, HttpStatus.OK);

    }catch (Exception ex)
    {

        LOGGER.error(ex.getMessage()); // log the exception message;

        responseModel.setIsSuccessful(false);

        responseModel.setResponseMessage("OOPs!...connection error, please retry");

        return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK);

    }

    };



    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<ResponseModel> SignUp(@Valid @RequestBody User user, BindingResult result){


        user.setDateCreated();

        //checks Database to know if a user already exists with the same username or email
        String existsResponse = userService.exists(user);

        // if non exists then persists user to the Database
        if(existsResponse.equalsIgnoreCase("No records found")){

            if(result.hasErrors()){ // checks if the data-binding (jpa validation errors) has errors

                responseModel.setIsSuccessful(false);

                responseModel.setResponseMessage(result.getFieldError().getDefaultMessage());

                return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK);


            }else {

                try {

                    String token = tokenService.generateToken(10);

                    while(activationService.TokenExists(token)){

                        token = tokenService.generateToken(10);
                    }

                    user.setDateCreated();

                    user.setActivated(false);

                    User savedUser = this.userService.save(user);

                    EmailStatus emailStatus = emailHtmlSender.send(user.getEmail(), "Welcome to Branded", "email/token", mailService.ComposeUserActivationMail(user,token));

                    if(emailStatus.isSuccess()) {

                        Activations activation = new Activations();

                        activation.setEmail(user.getEmail()).setRole(Activations.Role.USER)

                                .setActivationToken(token)

                                .setDateCreated();

                        activationService.save(activation);

                        responseModel.setIsSuccessful(true);

                        responseModel.setResponseMessage("An activation link has been sent to your mail");

                        return new ResponseEntity<>(responseModel, HttpStatus.CREATED); // return response to client.

                    }else{

                        userService.delete(savedUser);

                        responseModel.setIsSuccessful(false);

                        responseModel.setResponseMessage(emailStatus.getErrorMessage().isEmpty() ? "An error occurred sending you an activation mail, please try again!" : emailStatus.getErrorMessage());

                        return new ResponseEntity<>(responseModel, HttpStatus.OK);

                    }


                } catch (Exception ex) {

                    LOGGER.error(ex.getMessage()); // log the exception message;

                    responseModel.setIsSuccessful(false);

                    responseModel.setResponseMessage("OOPs!...connection error, please retry");

                    return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK);

                }
            }


            // if record exists on the Database send a response to the user that his username and email has been used already.
        }else{

            responseModel.setIsSuccessful(false);

            responseModel.setResponseMessage(existsResponse);

            return new ResponseEntity<ResponseModel>(responseModel, HttpStatus.OK); // return response to the client/
        }


    }

}
