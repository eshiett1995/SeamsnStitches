package com.erondu.ngozi.web.controller.thirdparty.jsonmodel;

import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.User;

public class UserModel {

    public UserModel(User user, ResponseModel responseModel) {

        this.user = user;
        this.responseModel = responseModel;

    }

    private User user;

    private ResponseModel responseModel;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ResponseModel getResponseModel() {
        return responseModel;
    }

    public void setResponseModel(ResponseModel responseModel) {
        this.responseModel = responseModel;
    }

}
