package com.erondu.ngozi.web.controller.jsonmodel.request;

public class SearchModel {

    private String searchKey;

    public String getSearchKey() {

        return searchKey;

    }

    public void setSearchKey(String searchKey) {

        this.searchKey = searchKey;

    }
}
