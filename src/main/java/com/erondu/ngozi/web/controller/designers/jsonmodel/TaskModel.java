package com.erondu.ngozi.web.controller.designers.jsonmodel;

public class TaskModel {

    private int totalPendingTask;

    private int totalCompletedTask;

    public int getTotalPendingTask() {

        return totalPendingTask;

    }

    public TaskModel setTotalPendingTask(int totalPendingTask) {

        this.totalPendingTask = totalPendingTask;

        return this;

    }

    public int getTotalCompletedTask() {

        return totalCompletedTask;

    }

    public TaskModel setTotalCompletedTask(int totalCompletedTask) {

        this.totalCompletedTask = totalCompletedTask;

        return this;

    }
}
