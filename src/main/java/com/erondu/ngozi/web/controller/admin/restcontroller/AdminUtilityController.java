package com.erondu.ngozi.web.controller.admin.restcontroller;


import com.erondu.ngozi.web.controller.jsonmodel.response.ResponseModel;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.cloths.Fibre;
import com.erondu.ngozi.web.entity.cloths.Material;
import com.erondu.ngozi.web.entity.cloths.Weave;
import com.erondu.ngozi.web.service.implementation.FibreService;
import com.erondu.ngozi.web.service.implementation.MaterialService;
import com.erondu.ngozi.web.service.implementation.WeaveService;
import com.erondu.ngozi.web.utility.datatable.AppUtil;
import com.erondu.ngozi.web.utility.datatable.DataTableRequest;
import com.erondu.ngozi.web.utility.datatable.DataTableResults;
import com.erondu.ngozi.web.utility.datatable.PaginationCriteria;
import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/admin")
public class AdminUtilityController {


    @Autowired
    FibreService fibreService;

    @Autowired
    WeaveService weaveService;

    @Autowired
    MaterialService materialService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String SHIRT_MATERIAL_FOLDER = "C:\\Users\\Java\\IdeaProjects\\SeamsnStitches\\src\\main\\resources\\static\\models\\Material\\shirt\\DiffuseMap\\" ;

    private static final String TROUSER_MATERIAL_FOLDER = "C:\\Users\\Java\\IdeaProjects\\SeamsnStitches\\src\\main\\resources\\static\\models\\Material\\trouser\\DiffuseMap\\" ;

    private static final String SUIT_MATERIAL_FOLDER = "C:\\Users\\Java\\IdeaProjects\\SeamsnStitches\\src\\main\\resources\\static\\models\\Material\\suit\\DiffuseMap\\" ;


    @RequestMapping(value = "/save/fibre",method = RequestMethod.POST)

    public ResponseEntity<ResponseModel> SaveFibre(@RequestBody Fibre fibre){

        try {

            fibreService.save(fibre.setName(fibre.getName().toUpperCase()));

            ResponseModel responseModel = new ResponseModel();

            responseModel.setIsSuccessful(true).setResponseMessage("Successful");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "unable to save the fibre"), HttpStatus.CREATED);

        }

    }


    @RequestMapping(value = "/query/fibre",method = RequestMethod.GET)

    public String QueryFibreTable(HttpServletRequest request){

        DataTableRequest<Fibre> dataTableInRQ = new DataTableRequest<Fibre>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, name as name, (SELECT COUNT(1) FROM FIBRE) AS totalrecords  FROM FIBRE";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Fibre.class);

        @SuppressWarnings("unchecked")
        List<Fibre> fibreList = query.getResultList();

        DataTableResults<Fibre> dataTableResult = new DataTableResults<Fibre>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(fibreList);
        if (!AppUtil.isObjectEmpty(fibreList)) {
            dataTableResult.setRecordsTotal(String.valueOf(fibreList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(fibreList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(fibreList.size()));
            }
        }
        return new Gson().toJson(dataTableResult);
    }


    @RequestMapping(value = "/delete/fibre",method = RequestMethod.POST)

    public ResponseEntity<ResponseModel> DeleteFibre(@RequestBody Fibre fibre, HttpServletRequest request){

        ResponseModel responseModel = new ResponseModel();

        try {

            Fibre foundFibre = fibreService.getFibre(fibre.getId());

            if (Objects.nonNull(foundFibre)) {

                fibreService.delete(foundFibre);

                responseModel.setIsSuccessful(true).setResponseMessage("Successfully deleted");

            } else {

                responseModel.setIsSuccessful(true).setResponseMessage("Successfully deleted");

            }

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }catch (Exception ex){return new ResponseEntity<>(new ResponseModel(true,"OOps!, Connection error"), HttpStatus.OK);}


    }


    @RequestMapping(value = "/save/weave",method = RequestMethod.POST)

    public ResponseEntity<ResponseModel> SaveWeave(@RequestBody Weave weave){

        try {

            weaveService.save(weave.setName(weave.getName().toUpperCase()));

            ResponseModel responseModel = new ResponseModel();

            responseModel.setIsSuccessful(true).setResponseMessage("Successful");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

        }catch (Exception ex){

            return new ResponseEntity<>(new ResponseModel(false, "unable to save the fibre"), HttpStatus.CREATED);

        }

    }


    @RequestMapping(value = "/query/weave",method = RequestMethod.GET)

    public String QueryWeaveTable(HttpServletRequest request){

        DataTableRequest<Weave> dataTableInRQ = new DataTableRequest<Weave>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, name as name, (SELECT COUNT(1) FROM WEAVE) AS totalrecords  FROM WEAVE";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Fibre.class);

        @SuppressWarnings("unchecked")
        List<Fibre> weaveList = query.getResultList();

        DataTableResults<Fibre> dataTableResult = new DataTableResults<Fibre>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(weaveList);
        if (!AppUtil.isObjectEmpty(weaveList)) {
            dataTableResult.setRecordsTotal(String.valueOf(weaveList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(weaveList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(weaveList.size()));
            }
        }
        return new Gson().toJson(dataTableResult);
    }



    @RequestMapping(value = "/delete/weave",method = RequestMethod.POST)

    public ResponseEntity<ResponseModel> DeleteWeave(@RequestBody Weave weave, HttpServletRequest request){

        ResponseModel responseModel = new ResponseModel();

        try {

            Weave foundWeave = weaveService.getWeave(weave.getId());

            if (Objects.nonNull(foundWeave)) {

                weaveService.delete(foundWeave);

                responseModel.setIsSuccessful(true).setResponseMessage("Successfully deleted");

            } else {

                responseModel.setIsSuccessful(true).setResponseMessage("Successfully deleted");

            }

            return new ResponseEntity<>(responseModel, HttpStatus.OK);

        }catch (Exception ex){return new ResponseEntity<>(new ResponseModel(true,"OOps!, Connection error"), HttpStatus.OK);}


    }



    @RequestMapping(value = "/save/material",method = RequestMethod.POST)

    public ResponseEntity<ResponseModel> SaveMaterial(@RequestParam("file") MultipartFile file, @RequestParam("model") String model,
                                                      RedirectAttributes redirectAttributes){


        ResponseModel responseModel = new ResponseModel();

        try {

            if (file.isEmpty()) {
                redirectAttributes.addFlashAttribute("message", "Please select a file to upload");

                responseModel.setIsSuccessful(false).setResponseMessage("Please select a file to upload");

                return new ResponseEntity<>(responseModel, HttpStatus.CREATED); // return response to client.
            }


            JSONObject jsonObject = new JSONObject(model);

            Material material = new Gson().fromJson(model, Material.class);

            for (Enum.ClothStyle clothStyle : Enum.ClothStyle.values()) {

                if (clothStyle.getValue().equalsIgnoreCase(jsonObject.getString("clothStyle"))) {

                    material.setClothStyle(clothStyle);
                }
            }

            Material savedMaterial = materialService.save(material);

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            Path path;

            if(material.getClothStyle() == Enum.ClothStyle.COOPERATE_SHIRT) {

                path = Paths.get(SHIRT_MATERIAL_FOLDER + material.getName() + savedMaterial.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));

            }else if(material.getClothStyle() == Enum.ClothStyle.COOPERATE_TROUSER){

                path = Paths.get(TROUSER_MATERIAL_FOLDER + material.getName() + savedMaterial.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));


            }else{

                path = Paths.get(SUIT_MATERIAL_FOLDER + material.getName() + savedMaterial.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));


            }


            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");


            savedMaterial.setImageURL(material.getName() + savedMaterial.getId());

            materialService.save(savedMaterial);

            responseModel.setIsSuccessful(true).setResponseMessage("Successful");

            return new ResponseEntity<>(responseModel, HttpStatus.CREATED);

        }catch (Exception ex){

            System.out.print(ex);

            return new ResponseEntity<>(new ResponseModel(false, "unable to save the Material"), HttpStatus.CREATED);

        }

    }


    @RequestMapping(value = "/query/material",method = RequestMethod.GET)

    public String QueryMaterialTable(HttpServletRequest request){

        DataTableRequest<Material> dataTableInRQ = new DataTableRequest<Material>(request);
        PaginationCriteria pagination = dataTableInRQ.getPaginationRequest();

        String baseQuery = "SELECT id as id, time as time, name as name, color as color, price as price, (SELECT COUNT(1) FROM MATERIAL) AS totalrecords  FROM MATERIAL";
        String paginatedQuery = AppUtil.buildPaginatedQuery(baseQuery, pagination);

        System.out.println(paginatedQuery);

        Query query = entityManager.createNativeQuery(paginatedQuery, Material.class);

        @SuppressWarnings("unchecked")
        List<Material> materialList = query.getResultList();

        DataTableResults<Material> dataTableResult = new DataTableResults<Material>();
        dataTableResult.setDraw(dataTableInRQ.getDraw());
        dataTableResult.setListOfDataObjects(materialList);
        if (!AppUtil.isObjectEmpty(materialList)) {
            dataTableResult.setRecordsTotal(String.valueOf(materialList.size())
            );
            if (dataTableInRQ.getPaginationRequest().isFilterByEmpty()) {
                dataTableResult.setRecordsFiltered(String.valueOf(materialList.size()));
            } else {
                dataTableResult.setRecordsFiltered(String.valueOf(materialList.size()));
            }
        }
        return new Gson().toJson(dataTableResult);
    }
}
