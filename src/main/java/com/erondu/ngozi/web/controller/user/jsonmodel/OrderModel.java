package com.erondu.ngozi.web.controller.user.jsonmodel;

import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.utility.annotations.GsonRepellent;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class OrderModel {

    private double amount;


    @Enumerated(EnumType.STRING)
    @Column(name = "style")
    private Enum.ClothStyle clothStyle;


    @GsonRepellent
    @OneToMany(fetch = FetchType.EAGER)
    private List<ClothModel> clothModels = new ArrayList<>();


    @Enumerated(EnumType.STRING)
    private Enum.Status status;



    @GsonRepellent
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    @Transient
    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {

        return tasks;
    }

    public OrderModel setTasks(List<Task> tasks) {

        this.tasks = tasks;

        return this;
    }

    public List<ClothModel> getClothModels() {
        return clothModels;
    }

    public OrderModel setClothModels(List<ClothModel> clothModels) {

        this.clothModels = clothModels;

        return this;
    }


    public Enum.Status getStatus() {

        return status;

    }

    public void setStatus(Enum.Status status) {
        this.status = status;
    }

    public double getAmount() {

        return amount;

    }

    public OrderModel setAmount(double amount) {

        this.amount = amount;

        return this;

    }


    public Enum.ClothStyle getClothStyle() {

        return clothStyle;
    }

    public OrderModel setClothStyle(Enum.ClothStyle clothStyle) {

        this.clothStyle = clothStyle;

        return this;
    }

    public User getUser() {
        return user;
    }

    public OrderModel setUser(User user) {

        this.user = user;

        return this;
    }
}


