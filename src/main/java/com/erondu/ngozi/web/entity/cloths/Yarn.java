package com.erondu.ngozi.web.entity.cloths;


import com.erondu.ngozi.web.entity.DefaultEntity;

import javax.persistence.Entity;

@Entity
public class Yarn extends DefaultEntity {

    public Yarn(double warp, double weft){

        this.warp = warp;

        this.weft = weft;
    }

    public Yarn(){

    }

    private double warp;

    private double weft;

    public double getWarp() {

        return warp;

    }

    public Yarn setWarp(double warp) {

        this.warp = warp;

        return this;
    }

    public double getWeft() {

        return weft;

    }

    public Yarn setWeft(double weft)
    {
        this.weft = weft;

        return this;
    }
}
