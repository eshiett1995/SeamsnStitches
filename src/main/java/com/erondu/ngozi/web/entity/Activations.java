package com.erondu.ngozi.web.entity;


import javax.persistence.Entity;

@Entity
public class Activations extends DefaultEntity {

    public enum Role{DESIGNER,USER,BRANDEDTEAM}

    public String email;

    public Role role;

    public String activationToken;

    public String getEmail() {
        return email;
    }

    public Activations setEmail(String email) {
        this.email = email;

        return this;
    }

    public Role getRole() {
        return role;
    }

    public Activations setRole(Role role) {
        this.role = role;
        return this;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public Activations setActivationToken(String activationToken) {

        this.activationToken = activationToken;
        return this;

    }
}
