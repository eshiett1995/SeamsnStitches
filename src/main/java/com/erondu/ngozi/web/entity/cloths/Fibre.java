package com.erondu.ngozi.web.entity.cloths;


import com.erondu.ngozi.web.entity.DefaultEntity;

import javax.persistence.Entity;

@Entity
public class Fibre extends DefaultEntity {

    public Fibre(String name){

        this.name = name;

    }

    public Fibre(){

    }

    private String name;

    public String getName() {
        return name;
    }

    public Fibre setName(String name) {

        this.name = name;

        return this;
    }
}
