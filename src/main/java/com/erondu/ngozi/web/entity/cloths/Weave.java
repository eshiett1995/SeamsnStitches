package com.erondu.ngozi.web.entity.cloths;


import com.erondu.ngozi.web.entity.DefaultEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Weave extends DefaultEntity {

    public Weave(String name){

        this.name = name;

    }

    public Weave(){

    }

    @Column(unique = true)
    private String name;

    public String getName() {
        return name;
    }

    public Weave setName(String name) {

        this.name = name;

        return this;
    }
}
