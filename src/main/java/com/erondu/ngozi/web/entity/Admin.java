package com.erondu.ngozi.web.entity;

import com.erondu.ngozi.web.entity.DefaultEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Entity
public class Admin extends DefaultEntity {


    private String username;

    private  String email;

    private String password;


    @Enumerated(EnumType.STRING)
    private Enum.AdminType  adminType;


    public String getUsername() {

        return username;
    }

    public Admin setUsername(String username) {

        this.username = username;

        return this;

    }

    public String getEmail() {

        return email;

    }

    public Admin setEmail(String email) {

        this.email = email;

        return this;

    }

    public String getPassword() {

        return password;

    }

    public Admin setPassword(String password) {

        this.password = password;

        return this;

    }

    public Enum.AdminType getAdminType() {

        return adminType;

    }

    public Admin setAdminType(Enum.AdminType adminType) {

        this.adminType = adminType;

        return this;

    }
}
