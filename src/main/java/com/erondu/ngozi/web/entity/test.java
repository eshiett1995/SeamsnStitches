package com.erondu.ngozi.web.entity;

import com.google.gson.annotations.SerializedName;
import flexjson.JSONSerializer;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class test extends DefaultEntity {


    /** The draw. */
    private String draw;

    /** The records filtered. */
    private String recordsFiltered;

    /** The records total. */

    private String recordsTotal;

    /** The list of data objects. */
    @SerializedName("data")
    List<Orders> listOfDataObjects;


    public String getJson() {


        JSONSerializer serializer = new JSONSerializer();

        return serializer.serialize(this);


    }


    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public String getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(String recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public String getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(String recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public List<Orders> getListOfDataObjects() {
        return listOfDataObjects;
    }

    public void setListOfDataObjects(List<Orders> listOfDataObjects) {
        this.listOfDataObjects = listOfDataObjects;
    }
}
