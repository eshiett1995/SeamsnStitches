package com.erondu.ngozi.web.entity;


import javax.persistence.*;

@Entity
@Table(name = "users")
public class User extends DefaultEntity {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String password;

    private String email;

    private String address;

    private boolean isActivated = false;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "coordinate")
    private UserGpsCoordinate userGpsCoordinate;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "trouserMeasurement")
    private TrouserMeasurement trouserMeasurement;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "shirtMeasurement")
    private ShirtMeasurement shirtMeasurement;



    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {

        this.firstName = firstName;

        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {

        this.lastName = lastName;

        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public User setPhoneNumber(String phoneNumber) {

        this.phoneNumber = phoneNumber;

        return this;
    }


    public String getPassword() {

        return password;

    }

    public User setPassword(String password) {

        this.password = password;

        return this;

    }

    public String getEmail() {

        return email;

    }

    public User setEmail(String email) {

        this.email = email;

        return this;
    }

    public String getAddress() {

        return address;

    }

    public User setAddress(String address) {
        this.address = address;
        return this;
    }

    public ShirtMeasurement getShirtMeasurement() {
        return shirtMeasurement;
    }

    public User setShirtMeasurement(ShirtMeasurement shirtMeasurement) {

        this.shirtMeasurement = shirtMeasurement;

        return this;
    }

    public TrouserMeasurement getTrouserMeasurement() {
        return trouserMeasurement;
    }

    public User setTrouserMeasurement(TrouserMeasurement trouserMeasurement) {

        this.trouserMeasurement = trouserMeasurement;

        return this;
    }

    public boolean isActivated() {

        return isActivated;

    }

    public User setActivated(boolean activated) {

        isActivated = activated;

        return this;

    }

    public UserGpsCoordinate getUserGpsCoordinate() {
        return userGpsCoordinate;
    }

    public User setUserGpsCoordinate(UserGpsCoordinate userGpsCoordinate) {
        this.userGpsCoordinate = userGpsCoordinate;
        return this;
    }
}
