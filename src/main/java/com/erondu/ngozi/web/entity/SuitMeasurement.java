package com.erondu.ngozi.web.entity;


import javax.persistence.Entity;


public class SuitMeasurement {


    private User customer;

    private double top_length;

    private double chest;

    private double shoulder;

    private double sleeve_lenght;

    private double neck;

    private double round_wrist;

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public double getTop_length() {
        return top_length;
    }

    public void setTop_length(double top_length) {
        this.top_length = top_length;
    }

    public double getChest() {
        return chest;
    }

    public void setChest(double chest) {
        this.chest = chest;
    }

    public double getShoulder() {
        return shoulder;
    }

    public void setShoulder(double shoulder) {
        this.shoulder = shoulder;
    }

    public double getSleeve_lenght() {
        return sleeve_lenght;
    }

    public void setSleeve_lenght(double sleeve_lenght) {
        this.sleeve_lenght = sleeve_lenght;
    }

    public double getNeck() {
        return neck;
    }

    public void setNeck(double neck) {

        this.neck = neck;

    }

    public double getRound_wrist() {

        return round_wrist;

    }

    public void setRound_wrist(double round_wrist) {

        this.round_wrist = round_wrist;

    }

}
