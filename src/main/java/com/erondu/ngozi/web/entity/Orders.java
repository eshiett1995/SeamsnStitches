package com.erondu.ngozi.web.entity;

import com.erondu.ngozi.web.utility.annotations.GsonRepellent;
import com.google.gson.annotations.Expose;
import org.hibernate.criterion.Order;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Orders extends DefaultEntity {


    @GsonRepellent
    @ManyToOne
    @JoinColumn(name = "fashionbrand")
    private FashionBrand fashionBrand;

    private double amount;


    @GsonRepellent
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ClothModel> clothModels = new ArrayList<>();


    @Enumerated(EnumType.STRING)
    private Enum.Status status;



    @GsonRepellent
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    public List<ClothModel> getClothModels() {
        return clothModels;
    }

    public Orders setClothModels(List<ClothModel> clothModels) {

        this.clothModels = clothModels;

        return this;
    }

    public Orders setClothModels(ClothModel clothModel) {

        this.clothModels.add(clothModel);

        return this;
    }


    public FashionBrand getFashionBrand() {
        return fashionBrand;
    }

    public Orders setFashionBrand(FashionBrand fashionBrand) {
        this.fashionBrand = fashionBrand;
        return this;
    }

    public Enum.Status getStatus() {

        return status;

    }

    public Orders setStatus(Enum.Status status) {

        this.status = status;

        return this;
    }

    public double getAmount() {

        return amount;

    }

    public Orders setAmount(double amount) {

        this.amount = amount;

        return this;

    }


    public User getUser() {
        return user;
    }

    public Orders setUser(User user) {

        this.user = user;

        return this;
    }
}
