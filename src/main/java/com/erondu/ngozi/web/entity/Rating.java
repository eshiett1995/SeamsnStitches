package com.erondu.ngozi.web.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Rating extends  DefaultEntity {

    @ManyToOne
    @JoinColumn(name = "fashion_brand_id")
    private FashionBrand fashionBrand;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private double score;

    public FashionBrand getFashionBrand() {

        return fashionBrand;

    }

    public void setFashionBrand(FashionBrand fashionBrand) {

        this.fashionBrand = fashionBrand;

    }

    public double getScore() {

        return score;

    }

    public void setScore(double score) {

        this.score = score;

    }

    public Employee getEmployee() {

        return employee;

    }

    public void setEmployee(Employee employee) {

        this.employee = employee;

    }
}
