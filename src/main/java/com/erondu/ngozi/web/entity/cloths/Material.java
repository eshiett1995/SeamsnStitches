package com.erondu.ngozi.web.entity.cloths;


import com.erondu.ngozi.web.entity.DefaultEntity;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.FashionBrand;

import javax.persistence.*;

@Entity
public class Material extends DefaultEntity {

    private String name;

    private String detail;

    private String color;

    private double weight;

    @Enumerated(EnumType.STRING )
    private Enum.ClothStyle clothStyle;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "yarn")
    private Yarn yarn;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "weave")
    private Weave weave;

    private double price;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fibre")
    private Fibre fibre;

    private int fiberPercentage;

    private String imageURL;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "fashionbrand")
    private FashionBrand fashionBrand;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "clothing_type")
    private ClothingType clothingType;

    public FashionBrand getFashionBrand() {
        return fashionBrand;
    }

    public Material setFashionBrand(FashionBrand fashionBrand) {

        this.fashionBrand = fashionBrand;

        return this;
    }

    public ClothingType getClothingType() {

        return clothingType;
    }

    public Material setClothingType(ClothingType clothingType) {
        this.clothingType = clothingType;
        return this;
    }

    public String getName() {
        return name;
    }

    public Material setName(String name) {

        this.name = name;

        return this;
    }

    public String getImageURL() {
        return imageURL;
    }

    public Material setImageURL(String imageURL) {
        this.imageURL = imageURL;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public Material setDetail(String detail) {

        this.detail = detail;

        return this;
    }

    public String getColor() {

        return color;

    }

    public Material setColor(String color) {

        this.color = color;

        return this;
    }

    public double getWeight() {

        return weight;

    }

    public Material setWeight(double weight) {

        this.weight = weight;

        return this;

    }

    public Yarn getYarn() {

        return yarn;

    }

    public Material setYarn(Yarn yarn) {

        this.yarn = yarn;

        return this;
    }

    public Weave getWeave() {

        return weave;

    }

    public Material setWeave(Weave weave) {

        this.weave = weave;

        return this;

    }

    public double getPrice() {

        return price;

    }

    public Material setPrice(double price) {

        this.price = price;

        return this;

    }

    public Fibre getFibre() {

        return fibre;

    }

    public Material setFibre(Fibre fibre) {

        this.fibre = fibre;

        return this;

    }

    public int getFiberPercentage() {

        return fiberPercentage;

    }

    public Material setFiberPercentage(int fiberPercentage) {

        this.fiberPercentage = fiberPercentage;

        return this;

    }

    public Enum.ClothStyle getClothStyle() {
        return clothStyle;
    }

    public Material setClothStyle(Enum.ClothStyle clothStyle) {

        this.clothStyle = clothStyle;

        return this;
    }
}
