package com.erondu.ngozi.web.entity;

import java.util.ArrayList;
import java.util.List;

public class Enum {

    public enum ClothStyle {

        COOPERATE_SHIRT("SHIRT"), COOPERATE_TROUSER("TROUSER") , NATIVE ("ANKARA");

        private String value;

        ClothStyle(String name) {

            this.value = name;

        }

        public String getValue() {

            return value;
        }

    }

    public enum AdminType {

        SUPER, NORMAL;

    }

    public enum Status { PENDING, COMPLETED, DELIVERED, ASSIGNED, UNASSIGNED}

}
