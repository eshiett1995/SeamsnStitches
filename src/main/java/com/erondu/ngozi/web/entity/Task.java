package com.erondu.ngozi.web.entity;


import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;

@Entity
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
public class Task extends DefaultEntity{


    @ManyToOne
    @JoinColumn(name = "cloth_model_id")
    private ClothModel clothModel;

    @Transient
    private long clothModelId;

    private String details;

    private boolean completed;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;


    public long getClothModelId() {
        return clothModelId;
    }

    public Task setClothModelId(long clothModelId) {
        this.clothModelId = clothModelId;
        return this;
    }

    public ClothModel getClothModel() {

        return clothModel;
    }

    public void setClothModel(ClothModel clothModel) {
        this.clothModel = clothModel;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
