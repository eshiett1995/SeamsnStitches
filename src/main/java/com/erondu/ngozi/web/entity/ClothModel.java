package com.erondu.ngozi.web.entity;


import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
public class ClothModel extends DefaultEntity {

    private ClothingType clothingType;

    private int quantity;

    private double amount;

    @Transient
    private String modelStructure;

    private String savedUrl;


    @Transient
    @JsonIgnore
    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {

        return tasks;
    }

    public ClothModel setTasks(List<Task> tasks) {

        this.tasks = tasks;

        return this;
    }


    public ClothModel setTask(Task tasks) {

        this.tasks.add(tasks);

        return this;
    }


    public ClothingType getClothingType() {

        return clothingType;

    }

    public ClothModel setClothingType(ClothingType clothingType) {

        this.clothingType = clothingType;

        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public ClothModel setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public double getAmount() {
        return amount;
    }

    public ClothModel setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public String getModelStructure() {
        return modelStructure;
    }

    public ClothModel setModelStructure(String modelStructure) {

        this.modelStructure = modelStructure;

        return this;
    }

    public String getSavedUrl() {
        return savedUrl;
    }

    public void setSavedUrl(String savedUrl) {
        this.savedUrl = savedUrl;
    }
}
