package com.erondu.ngozi.web.entity;


import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class Pricing extends  DefaultEntity{

     @ManyToOne
     @JoinColumn(name = "clothingtype_id")
     private ClothingType clothingType;

     private  double price;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fashionBrand_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FashionBrand fashionBrand;

    public ClothingType getClothingType() {
        return clothingType;
    }

    public Pricing setClothingType(ClothingType clothingType) {
        this.clothingType = clothingType;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Pricing setPrice(double price) {
        this.price = price;
        return this;
    }

    public FashionBrand getFashionBrand() {
        return fashionBrand;
    }

    public Pricing setFashionBrand(FashionBrand fashionBrand) {
        this.fashionBrand = fashionBrand;
        return this;
    }
}
