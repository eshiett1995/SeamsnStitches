package com.erondu.ngozi.web.entity;


import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Services extends DefaultEntity {

    //@OneToOne(mappedBy = "services")
    //private FashionBrand fashionBrand;

    private boolean traditional;

    private boolean shirt;

    private boolean trouser;

    private boolean suit;


    public boolean isTraditional() {

        return traditional;
    }


    public Services setTraditional(boolean traditional) {
        this.traditional = traditional;
        return this;
    }

    public boolean isShirt() {
        return shirt;
    }

    public Services setShirt(boolean shirt) {
        this.shirt = shirt;
        return this;
    }

    public boolean isTrouser() {
        return trouser;
    }

    public Services setTrouser(boolean trouser) {
        this.trouser = trouser;
        return this;
    }

    public boolean isSuit() {
        return suit;
    }

    public Services setSuit(boolean suit) {
        this.suit = suit;
        return this;
    }

   // public FashionBrand getFashionBrand() {
    //    return fashionBrand;
    //}

    //public Services setFashionBrand(FashionBrand fashionBrand) {
    //    this.fashionBrand = fashionBrand;
    //    return this;
    //}
}
