package com.erondu.ngozi.web.configuration;

import com.erondu.ngozi.web.interceptors.AdminPageInterceptor;
import com.erondu.ngozi.web.interceptors.DesignerPageInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Oto-obong on 31/10/2017.
 */

@Configuration
public class WebMVCConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    AdminPageInterceptor adminPageInterceptor;

    @Autowired
    DesignerPageInterceptor designerPageInterceptor;


    @Override
    public void configurePathMatch(PathMatchConfigurer matcher) {
        matcher.setUseRegisteredSuffixPatternMatch(true);
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //registry.addInterceptor(adminPageInterceptor).addPathPatterns("/admin/**").excludePathPatterns("/admin/login");

        //registry.addInterceptor(designerPageInterceptor).addPathPatterns("/designer/**");


    }


   //@Bean
   /** public BCryptPasswordEncoder passwordEncoder() {

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        return bCryptPasswordEncoder;
    }**/
}
