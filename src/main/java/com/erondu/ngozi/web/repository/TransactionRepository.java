package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Transaction;
import org.hibernate.cache.spi.TransactionalDataRegion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;


@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {

    Page<Transaction> getByOrdersFashionBrand(FashionBrand fashionBrand, Pageable pageable);

    Page<Transaction> getByOrdersUserEmail(String email, Pageable pageable);

    Page<Transaction> getByDateCreatedLessThan(Date date, Pageable pageable);

}
