package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findUserByEmailOrPhoneNumber(String email, String phoneNumber);

    User findUsersByEmailAndPassword(String email, String password);

    User findUserByEmail(String email);

    @Query("select count(user) from User user where user.isActivated = ?1")
    int countAll(boolean activated);


}
