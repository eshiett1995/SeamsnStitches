package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Pricing;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PricingRepository extends JpaRepository<Pricing,Long> {

    List<Pricing> getAllByFashionBrand(FashionBrand fashionBrand);

    Pricing getByClothingTypeAndAndFashionBrand(ClothingType clothingType, FashionBrand fashionBrand);


}
