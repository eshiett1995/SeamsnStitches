package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.cloths.ClothingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface ClothingTypeRepository extends JpaRepository<ClothingType,Long> {

    List<ClothingType> getByGenderIsNot(ClothingType.Gender gender);

    ClothingType getClothingTypeByNameAndGender(String name, ClothingType.Gender gender);

}
