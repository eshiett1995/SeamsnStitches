package com.erondu.ngozi.web.repository;


import com.erondu.ngozi.web.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin,Long> {

    Admin getAdminByEmailAndPassword(String email, String password);


}
