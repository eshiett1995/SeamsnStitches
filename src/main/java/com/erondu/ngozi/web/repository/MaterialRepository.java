package com.erondu.ngozi.web.repository;


import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.entity.cloths.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialRepository extends JpaRepository<Material,Long> {

    Page<Material> getMaterialsByFashionBrandAndClothingTypeOrderByNameAsc(FashionBrand fashionBrand, ClothingType clothingType, Pageable pageable);

}
