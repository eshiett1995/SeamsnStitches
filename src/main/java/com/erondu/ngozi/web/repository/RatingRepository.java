package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating,Long> {

    @Query("select rating.score from Rating rating where rating.fashionBrand = ?1")
    List<Double> getAllFashionBrandRating(FashionBrand fashionBrand);

    @Query("select rating.score from Rating rating where rating.employee = ?1")
    List<Double> getAllEmployeeRating(Employee employee);

}
