package com.erondu.ngozi.web.repository;


import com.erondu.ngozi.web.entity.FashionBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FashionBrandRepository extends JpaRepository<FashionBrand,Long> {

    @Query("select COUNT(fashionBrand) from FashionBrand  fashionBrand")
    int countAllFashionBrand();

    @Query("select fashionbrand from FashionBrand fashionbrand order by fashionbrand.brandName asc")
    Page<FashionBrand> getAllFashionBrandAndOrderByBrandNameAsc(Pageable pageable);

    Page<FashionBrand> getFashionBrandByBrandNameIsContaining(String brandName, Pageable pageable);


    @Query(value = "SELECT id, time, brandname, brandphrase, email, image, location, password, phonenumber, website, bankdetail, coordinate, distance  FROM " +

            "( SELECT fs.id, fs.time, fs.brandname, fs.brandphrase, fs.email, fs.image, fs.location, fs.password, fs.phonenumber, fs.website, fs.bankdetail, fs.coordinate," +

            "3956 * 2 * ASIN (SQRT(POWER (SIN((gc.latitude - abs(:lat)) * pi()/180/2), 2) + COS(gc.latitude * pi()/180)* COS(abs(:lat) * pi()/180) * POWER(SIN((gc.longitude - :long)*pi()/180/2),2))) AS distance FROM fashionbrand fs inner join gpscoordinate gc on gc.id = fs.coordinate inner join pricing p on p.fashionbrand_id = fs.id and p.clothingtype_id = :clothType )as a GROUP BY a.id, a.distance, a.time, a.brandname, a.brandphrase, a.email, a.image, a.location, a.password, a.phonenumber, a.website, a.bankdetail, a.coordinate HAVING distance  <= :dist ORDER BY distance \n-- #pageable\n",  nativeQuery = true)
    Page<FashionBrand> getNearestFashionBrand(@Param("clothType")int clothType,@Param("long") double longitude, @Param("lat")double latitude, @Param("dist")double distance, Pageable p);


    @Query(value = "SELECT id, time, brandname, brandphrase, email, image, location, password, phonenumber, website, bankdetail, coordinate, distance  FROM " +

     "( SELECT fs.id, fs.time, fs.brandname, fs.brandphrase, fs.email, fs.image, fs.location, fs.password, fs.phonenumber, fs.website, fs.bankdetail, fs.coordinate," +

     "3956 * 2 * ASIN (SQRT(POWER (SIN((gc.latitude - 2) * pi()/180/2), 2) + COS(gc.latitude * pi()/180)* COS(2 * pi()/180) * POWER(SIN((gc.longitude - 3)*pi()/180/2),2))) AS distance FROM fashionbrand fs inner join gpscoordinate gc on gc.id = fs.coordinate )as a GROUP BY a.id, a.distance, a.time, a.brandname, a.brandphrase, a.email, a.image, a.location, a.password, a.phonenumber, a.website, a.bankdetail, a.coordinate HAVING distance  > 0 ORDER BY distance \n-- #pageable\n",  nativeQuery = true)
    Page<FashionBrand> getFashionMe(Pageable p);
}
