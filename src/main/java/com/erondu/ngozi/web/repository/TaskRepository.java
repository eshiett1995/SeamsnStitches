package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.ClothModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Long> {

    int countAllByEmployeeAndCompleted(Employee employee, boolean completed);

    int countAllByEmployee(Employee employee);

    void deleteTasksByEmployee(Employee employee);

    List<Task> getAllByClothModel(ClothModel clothModel);

}
