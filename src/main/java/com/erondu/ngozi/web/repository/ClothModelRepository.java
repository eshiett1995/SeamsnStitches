package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.ClothModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClothModelRepository extends JpaRepository<ClothModel,Long> {
}
