package com.erondu.ngozi.web.repository;


import com.erondu.ngozi.web.entity.Transaction;
import com.erondu.ngozi.web.entity.cloths.Weave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface WeaveRepository extends JpaRepository<Weave,Long> {


}
