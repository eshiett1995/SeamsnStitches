package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepository extends JpaRepository<Orders,Long> {


    Orders getById(Long id);

    int countByFashionBrand(FashionBrand fashionBrand);

    int countByFashionBrandAndStatus(FashionBrand fashionBrand, Enum.Status status);

    int countAllByStatus(Enum.Status status);

}
