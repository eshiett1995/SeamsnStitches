package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.Activations;
import com.erondu.ngozi.web.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ActivationsRepository extends JpaRepository<Activations,Long> {

    List<Activations> getAllByActivationTokenEquals(String token);

    Activations getFirstByActivationToken(String token);

}
