package com.erondu.ngozi.web.repository;

import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    Employee findEmployeeByEmail(String email);

    Employee findEmployeeByEmailAndPassword(String email, String password);

    @Query("select count(employee) from Employee employee where employee.Activated = ?1")
    int countAll(boolean activated);

    int countAllByFashionBrand(FashionBrand fashionBrand);

    List<Employee> getByFashionBrand(FashionBrand fashionBrand);


}
