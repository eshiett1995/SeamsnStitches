package com.erondu.ngozi.web.repository;


import com.erondu.ngozi.web.entity.cloths.Fibre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FibreRepository extends JpaRepository<Fibre,Long> {

}
