package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.User;
import com.erondu.ngozi.web.repository.UserRepository;
import com.erondu.ngozi.web.service.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserServiceInterface {


    @Autowired
    UserRepository userRepository;

    @Override
    public String exists(User user) {

       User expectedUser =  userRepository.findUserByEmailOrPhoneNumber(user.getEmail(), user.getPassword());

       if(expectedUser == null || expectedUser.getEmail() == null){

           return "No records found";

       }else{

           return "User exists";

       }

    }

    @Override
    public User save(User user) {

        return userRepository.save(user);

    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {

        userRepository.delete(user);

    }

    @Override
    public User findUserByEmailAndPassword(String email, String password) {

        User foundUser = userRepository.findUsersByEmailAndPassword(email, password);

        return foundUser;

    }

    @Override
    public User findUserByEmail(String Email) {

        return userRepository.findUserByEmail(Email);
    }

    @Override
    public int countAll(boolean isActivated) {

        return userRepository.countAll(isActivated);
    }
}
