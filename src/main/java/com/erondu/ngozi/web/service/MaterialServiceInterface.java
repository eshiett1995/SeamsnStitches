package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.entity.cloths.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MaterialServiceInterface {

    Material save(Material material);

    Page<Material> getPaginatedList(Pageable pageable);

    Page<Material> getMaterial(FashionBrand fashionBrand, ClothingType clothingType, Pageable pageable);
}
