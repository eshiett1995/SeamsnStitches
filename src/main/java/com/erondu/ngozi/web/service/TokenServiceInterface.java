package com.erondu.ngozi.web.service;

/**
 * Created by Oto-obong on 29/10/2017.
 */
public interface TokenServiceInterface {

    String generateToken(int length);
}
