package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.Activations;
import com.erondu.ngozi.web.repository.ActivationsRepository;
import com.erondu.ngozi.web.service.ActivationServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivationService implements ActivationServiceInterface {


    @Autowired
    ActivationsRepository activationsRepository;

    @Override
    public Activations save(Activations activations) {

        return activationsRepository.save(activations);

    }

    @Override
    public void delete(Activations activations) {

        activationsRepository.delete(activations);
    }

    @Override
    public boolean TokenExists(String token) {

        List availableTokens = new ArrayList();

        availableTokens = activationsRepository.getAllByActivationTokenEquals(token);

        if(availableTokens.size() > 0){

            return true;

        }else{

            return false;

        }

    }

    @Override
    public Activations GetActivationByToken(String string) {

        return activationsRepository.getFirstByActivationToken(string);

    }


}
