package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.User;

import java.util.List;

public interface EmployeeServiceInterface {

    Employee getEmployeeByID(Long id);

    Employee save(Employee employee);

    Employee findEmployeeByEmail(String email);

    Employee findEmployeeByEmailAndPasswordAndActivated(String email, String password, boolean isActivated);

    void update(Employee employee);

    void delete(Employee employee);

    int countAllEmployee(boolean activated);

    int countAllEmployee(FashionBrand fashionBrand);

    List<Employee> getAllEmployeeByFashionBrand(FashionBrand fashionBrand);

}
