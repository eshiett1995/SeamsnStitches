package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.service.CookieServiceInterface;
import com.erondu.ngozi.web.utility.AppConstants;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * Created by Oto-obong on 18/10/2017.
 */

@Service
public class CookieService implements CookieServiceInterface {

    @Override
    public boolean hasValidCookie(HttpServletRequest request) {


        Cookie[] cookieArray = request.getCookies();

        boolean hasValidCookie = false;

        if(cookieArray != null) {   //checks if the cookie array is null

            if (cookieArray.length >= 2) {  // checks if there are two cookies, SESSION and gambeat else return false (there most be only two cookies)

                for (Cookie cookie : cookieArray) { // checks if gambeat cookie is amongst the cookies

                     if(cookie.getName().equalsIgnoreCase("branded")){

                         HttpSession session = request.getSession(true);

                         Object object = (String) session.getAttribute(cookie.getValue());

                         return object != null;


                     }
                }

            } else {

                return false;
            }

        }else{

            return false;
        }

        return hasValidCookie;
    }

    @Override
    public Cookie generateCookie(String sessionId) {

        // generates cookie for gambeat.

        Cookie cookie = new Cookie(AppConstants.CookieName, sessionId);
        cookie.setSecure(false);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(900000000);
        cookie.setDomain("localhost");
        cookie.setPath("/");

        return cookie;

    }

    @Override
    public Object getStoredObject(String cookieName, HttpServletRequest request) {



        Cookie[] cookies = request.getCookies();

        if(cookies == null){

            return null;

        }

        for (Cookie cookie: cookies) {

            if(cookie.getName().equalsIgnoreCase(cookieName)){

                // Gets the session ID from the request
                HttpSession session = request.getSession(true);

                System.out.println("cookie name: " + cookie.getValue());

                return session.getAttribute(cookie.getValue());

            }

        }

        return null;


    }

    @Override
    public void removesStoredObject(String cookieName, HttpServletRequest request) {

        Cookie[] cookies = request.getCookies();

        if(Objects.isNull(cookies))
            return;

        for (Cookie cookie: cookies) {

            if(cookie.getName().equalsIgnoreCase(cookieName)){

                // Gets the session ID from the request
                HttpSession session = request.getSession(true);

                System.out.println("cookie name: " + cookie.getValue());

                 session.removeAttribute(cookie.getValue());

            }

        }

    }


}
