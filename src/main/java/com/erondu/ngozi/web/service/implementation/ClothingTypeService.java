package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.repository.ClothingTypeRepository;
import com.erondu.ngozi.web.service.ClothingTypeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClothingTypeService implements ClothingTypeServiceInterface {

    @Autowired
    ClothingTypeRepository clothingTypeRepository;

    @Override
    public ClothingType getClothingType(Long id) {

        return clothingTypeRepository.findOne(id);

    }

    @Override
    public ClothingType getClothingTypeByNameAndGender(String name, ClothingType.Gender gender) {

        return clothingTypeRepository.getClothingTypeByNameAndGender(name, gender);

    }

    @Override
    public List<ClothingType> getAll() {

        return clothingTypeRepository.findAll();

    }

    @Override
    public List<ClothingType> getAllByGenderIsNot(ClothingType.Gender gender) {

        return clothingTypeRepository.getByGenderIsNot(ClothingType.Gender.Unisex);
    }
}
