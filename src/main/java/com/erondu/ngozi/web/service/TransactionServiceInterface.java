package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface TransactionServiceInterface {

    Transaction save(Transaction transaction);

    Page<Transaction> getTransactionByOrdersFashionBrand(FashionBrand fashionBrand, Pageable pageable);

    Page<Transaction> getTransactionByOrdersUserEmail(String email, Pageable pageable);

    Page<Transaction> getTransactionByDate(Date date, Pageable pageable);

}
