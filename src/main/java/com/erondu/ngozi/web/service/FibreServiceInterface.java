package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.cloths.Fibre;

import java.util.List;

public interface FibreServiceInterface {

    Fibre save(Fibre fibre);

    List<Fibre> getAll();

    Fibre getFibre(Long id);

    void delete(Fibre fibre);
}
