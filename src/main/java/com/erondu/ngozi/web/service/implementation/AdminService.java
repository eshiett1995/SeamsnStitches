package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.Admin;
import com.erondu.ngozi.web.repository.AdminRepository;
import com.erondu.ngozi.web.service.AdminServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AdminService implements AdminServiceInterface {


    @Autowired
    AdminRepository adminRepository;

    @Override
    public Admin findAdminByEmailAndPassword(String email, String password) {

        return adminRepository.getAdminByEmailAndPassword(email,password);

    }
}
