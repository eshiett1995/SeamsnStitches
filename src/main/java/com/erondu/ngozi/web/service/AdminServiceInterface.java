package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.Admin;

public interface AdminServiceInterface {

    Admin findAdminByEmailAndPassword(String email, String password);
}
