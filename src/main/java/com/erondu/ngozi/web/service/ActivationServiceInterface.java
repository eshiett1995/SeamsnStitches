package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.Activations;
import org.thymeleaf.context.Context;

public interface ActivationServiceInterface {

    Activations save(Activations activations);

    void delete(Activations activations);

    boolean TokenExists(String token);

    Activations GetActivationByToken(String string);


}
