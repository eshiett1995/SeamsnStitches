package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.entity.cloths.Material;
import com.erondu.ngozi.web.repository.MaterialRepository;
import com.erondu.ngozi.web.service.MaterialServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MaterialService implements MaterialServiceInterface {

    @Autowired
    MaterialRepository materialRepository;


    @Override
    public Material save(Material material) {
        Page<Material> users = materialRepository.findAll(new PageRequest(0, 20));


        return materialRepository.save(material);

    }

    @Override
    public Page<Material> getPaginatedList(Pageable pageable) {

        return materialRepository.findAll(pageable);

    }

    @Override
    public Page<Material> getMaterial(FashionBrand fashionBrand, ClothingType clothingType, Pageable pageable) {

        return materialRepository.getMaterialsByFashionBrandAndClothingTypeOrderByNameAsc(fashionBrand, clothingType, pageable);

    }


}
