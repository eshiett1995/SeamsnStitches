package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.controller.jsonmodel.email.SingleProductModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.User;
import com.erondu.ngozi.web.service.MailServiceInterface;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.util.List;


@Service
public class MailService implements MailServiceInterface {


    @Override
    public Context ComposeUserActivationMail(User user,String token) {

        Context context = new Context();

        String msg = String.format("Hello %s %s welcome to Branded, to activate your account please kindly click the button below. We trust you would enjoy your stay, for request, inquiry please contact us on 08098516969. ", user.getFirstName(),user.getLastName(),token);

        context.setVariable("message", msg);

        context.setVariable("url","http://localhost:8080/activate/" + token );

        return  context;
    }


    @Override
    public Context ComposeEmployeeActivationMail(Employee employee, String token) {

        Context context = new Context();

        String msg = String.format("Hello %s %s welcome to Branded, to activate your account please kindly click the button below. We trust you would enjoy your stay, for request, inquiry please contact us on 08098516969. ", employee.getFirstName(),employee.getLastName(),token);

        context.setVariable("message", msg);

        context.setVariable("url","http://localhost:8080/activate/" + token );

        return  context;
    }

    @Override
    public Context ComposeCustomerReceiptMail(List<SingleProductModel> products) {

        Context context = new Context();

        context.setVariable("products", products);

        return  context;

    }
}
