package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Pricing;
import com.erondu.ngozi.web.entity.cloths.ClothingType;
import com.erondu.ngozi.web.repository.PricingRepository;
import com.erondu.ngozi.web.service.PricingServiceInterface;
import com.erondu.ngozi.web.service.RatingServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PricingService implements PricingServiceInterface {

    @Autowired
    PricingRepository pricingRepository;

    @Override
    public Pricing save(Pricing pricing) {

        return pricingRepository.save(pricing);

    }

    @Override
    public List<Pricing> getAllByFashionBrand(FashionBrand fashionBrand) {

        return pricingRepository.getAllByFashionBrand(fashionBrand);

    }

    @Override
    public Pricing getPricingByClothingTypeAndFashionBrand(ClothingType clothingType, FashionBrand fashionBrand) {

        return pricingRepository.getByClothingTypeAndAndFashionBrand(clothingType, fashionBrand);

    }
}
