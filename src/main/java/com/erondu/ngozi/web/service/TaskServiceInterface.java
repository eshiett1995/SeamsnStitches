package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.ClothModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.Task;

import java.util.List;

public interface TaskServiceInterface {

    Task getTaskByID(Long id);

    Task save(Task task);

    int countByIsCompleted(Employee employee, boolean isCompleted);

    int countTotalTask(Employee employee);

    List<Task> getTasksByClothModel(ClothModel clothModel);

    void delete(Task task);


}
