package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.controller.jsonmodel.email.SingleProductModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.User;
import org.thymeleaf.context.Context;

import java.util.List;

public interface MailServiceInterface {

    Context ComposeUserActivationMail(User user, String token);

    Context ComposeEmployeeActivationMail(Employee employee, String token);

    Context ComposeCustomerReceiptMail(List<SingleProductModel> products);
}
