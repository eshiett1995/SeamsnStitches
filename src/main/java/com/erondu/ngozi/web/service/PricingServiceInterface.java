package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Pricing;
import com.erondu.ngozi.web.entity.cloths.ClothingType;

import java.util.List;

public interface PricingServiceInterface {

    Pricing save(Pricing pricing);

    List<Pricing> getAllByFashionBrand(FashionBrand fashionBrand);

    Pricing getPricingByClothingTypeAndFashionBrand(ClothingType clothingType, FashionBrand fashionBrand);

}
