package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.ClothModel;
import com.erondu.ngozi.web.repository.ClothModelRepository;
import com.erondu.ngozi.web.service.ClothModelServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ClothModelService implements ClothModelServiceInterface {

    @Autowired
    ClothModelRepository clothModelRepository;

    @Override
    public ClothModel findById(Long id) {

        return clothModelRepository.findOne(id);

    }
}
