package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.cloths.Weave;
import com.erondu.ngozi.web.repository.WeaveRepository;
import com.erondu.ngozi.web.service.WeaveServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Service
public class WeaveService implements WeaveServiceInterface{


    @Autowired
    WeaveRepository weaveRepository;

    @Override
    public Weave save(Weave weave) {
        return weaveRepository.save(weave);
    }

    @Override
    public List<Weave> getAll() {
        List listOfWeaves = new ArrayList();

        listOfWeaves = weaveRepository.findAll();

        if(Objects.isNull(listOfWeaves) || listOfWeaves.isEmpty()){

            return Collections.EMPTY_LIST;


        }else{

            return listOfWeaves;
        }
    }

    @Override
    public Weave getWeave(Long id) {

        return weaveRepository.getOne(id);

    }

    @Override
    public void delete(Weave weave) {

        weaveRepository.delete(weave);

    }
}
