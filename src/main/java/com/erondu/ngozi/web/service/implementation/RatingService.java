package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.repository.RatingRepository;
import com.erondu.ngozi.web.service.RatingServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RatingService implements RatingServiceInterface {


    @Autowired
    RatingRepository ratingRepository;

    @Override
    public double getFashionBrandRating(FashionBrand fashionBrand) {

        double cumulativeScore = 0;

        List<Double> listOfScores = new ArrayList<Double>();

        listOfScores = ratingRepository.getAllFashionBrandRating(fashionBrand);

        if(listOfScores.isEmpty())

            return cumulativeScore;

        for (Double score : listOfScores){

            cumulativeScore = cumulativeScore + score;

        }

        return cumulativeScore/listOfScores.size();

    }

    @Override
    public double getEmployeeRating(Employee employee) {

        double cumulativeScore = 0;

        List<Double> listOfScores = new ArrayList<>();

        listOfScores = ratingRepository.getAllEmployeeRating(employee);

        if(listOfScores.isEmpty())

            return cumulativeScore;

        for (Double score : listOfScores){

            cumulativeScore = cumulativeScore + score;

        }

        return cumulativeScore/listOfScores.size();

    }
}
