package com.erondu.ngozi.web.service;



import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Oto-obong on 18/10/2017.
 */


public interface CookieServiceInterface {

    boolean hasValidCookie(HttpServletRequest request);

    Cookie generateCookie(String sessionID);

    Object getStoredObject(String cookieName, HttpServletRequest request);

    void removesStoredObject(String cookieName, HttpServletRequest request);

}
