package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.cloths.Fibre;
import com.erondu.ngozi.web.repository.FibreRepository;
import com.erondu.ngozi.web.service.FibreServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


@Service
public class FibreService implements FibreServiceInterface {

    @Autowired
    FibreRepository fibreRepository;


    @Override
    public Fibre save(Fibre fibre) {

        return fibreRepository.save(fibre);
    }

    @Override
    public List<Fibre> getAll() {

        List listOfFibres = new ArrayList();

        listOfFibres = fibreRepository.findAll();

        if(Objects.isNull(listOfFibres) || listOfFibres.isEmpty()){

            return Collections.EMPTY_LIST;


        }else{

            return listOfFibres;
        }

    }

    @Override
    public Fibre getFibre(Long id) {

        return fibreRepository.getOne(id);

    }

    @Override
    public void delete(Fibre fibre) {

        fibreRepository.delete(fibre);
    }
}
