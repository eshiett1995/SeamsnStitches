package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.ClothModel;

public interface ClothModelServiceInterface {

    ClothModel findById(Long id);

}
