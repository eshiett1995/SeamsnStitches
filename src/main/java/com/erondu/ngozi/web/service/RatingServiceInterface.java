package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;

public interface RatingServiceInterface {

    double getFashionBrandRating(FashionBrand fashionBrand);

    double getEmployeeRating(Employee employee);
}
