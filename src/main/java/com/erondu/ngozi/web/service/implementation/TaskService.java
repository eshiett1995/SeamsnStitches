package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.ClothModel;
import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.entity.Task;
import com.erondu.ngozi.web.repository.TaskRepository;
import com.erondu.ngozi.web.service.TaskServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService implements TaskServiceInterface {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public Task getTaskByID(Long id) {

        return taskRepository.getOne(id);

    }

    @Override
    public Task save(Task task) {

        return taskRepository.save(task);

    }

    @Override
    public int countByIsCompleted(Employee employee, boolean isCompleted) {

        return taskRepository.countAllByEmployeeAndCompleted(employee, isCompleted);
    }

    @Override
    public int countTotalTask(Employee employee) {

        return taskRepository.countAllByEmployee(employee);

    }

    @Override
    public List<Task> getTasksByClothModel(ClothModel clothModel) {

        return taskRepository.getAllByClothModel(clothModel);

    }

    @Override
    public void delete(Task task) {

        taskRepository.delete(task);
    }

}
