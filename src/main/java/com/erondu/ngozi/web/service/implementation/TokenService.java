package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.service.TokenServiceInterface;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by Oto-obong on 29/10/2017.
 */

@Service
public class TokenService implements TokenServiceInterface {

    String numbers = "1234567890";

    String alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


    @Override
    public String generateToken(int length) {

        String otp;

        Random randomGenerator = new Random();

        Integer indexOne = randomGenerator.nextInt(length);

        Integer indexTwo = randomGenerator.nextInt(length);

        if(indexOne.equals(indexTwo)){

            while(indexOne.equals(indexTwo)){

                indexTwo = randomGenerator.nextInt(length);

                if(!indexOne.equals(indexTwo))

                 break;
            }
        }

        char[] temporaryOTP = new char[length];

        for(Integer indexNumber = 0; indexNumber < length; indexNumber++) {

            if (indexNumber.equals(indexOne) || indexNumber.equals(indexTwo)) {

                temporaryOTP[indexNumber] = alphabets.charAt(randomGenerator.nextInt(alphabets.length()));

            } else {

                temporaryOTP[indexNumber] = numbers.charAt(randomGenerator.nextInt(numbers.length()));

            }
        }

        otp =  String.valueOf(temporaryOTP);

        return otp ;
    }
}
