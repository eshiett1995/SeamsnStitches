package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Orders;
import org.hibernate.criterion.Order;

import java.util.List;

public interface OrdersServiceInterface {

    Orders save(Orders orders);

    Orders getOrderByID(Long id);

    int getTotalOrders(FashionBrand fashionBrand);

    int getTotalOrdersByStatus(FashionBrand fashionBrand, Enum.Status status);

    int countAllByStatus(Enum.Status status);

    List<Orders> getAll();
}
