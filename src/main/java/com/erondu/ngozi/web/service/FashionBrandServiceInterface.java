package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.FashionBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FashionBrandServiceInterface {

    FashionBrand save(FashionBrand fashionBrand);

    FashionBrand update(FashionBrand fashionBrand);

    int countAllFashionBrand();

    FashionBrand getFashionBrand(Long id);

    Page<FashionBrand> getFashionBrand(Pageable pageable);

    Page<FashionBrand> getFashionBrandByDistance(int clothType, double longitude, double latitude, double distance, Pageable pageable);

    Page<FashionBrand> getFashionBrand(String containing,Pageable pageable);

}
