package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Transaction;
import com.erondu.ngozi.web.repository.TransactionRepository;
import com.erondu.ngozi.web.service.TransactionServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TransactionService implements TransactionServiceInterface{

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Transaction save(Transaction transaction) {

        return transactionRepository.save(transaction);

    }

    @Override
    public Page<Transaction> getTransactionByOrdersFashionBrand(FashionBrand fashionBrand, Pageable pageable) {

        return transactionRepository.getByOrdersFashionBrand(fashionBrand, pageable);

    }

    @Override
    public Page<Transaction> getTransactionByOrdersUserEmail(String email, Pageable pageable) {

        return transactionRepository.getByOrdersUserEmail(email, pageable);

    }

    @Override
    public Page<Transaction> getTransactionByDate(Date date, Pageable pageable) {

        return transactionRepository.getByDateCreatedLessThan(date, pageable);

    }
}
