package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.Employee;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.repository.EmployeeRepository;
import com.erondu.ngozi.web.service.EmployeeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements EmployeeServiceInterface{

    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public Employee getEmployeeByID(Long id) {

        return employeeRepository.getOne(id);

    }

    @Override
    public Employee save(Employee employee) {

        return employeeRepository.save(employee);

    }

    @Override
    public Employee findEmployeeByEmail(String email) {

        return employeeRepository.findEmployeeByEmail(email);
    }

    @Override
    public Employee findEmployeeByEmailAndPasswordAndActivated(String email, String password, boolean isActivated) {

        return employeeRepository.findEmployeeByEmailAndPassword(email,password);

    }

    @Override
    public void update(Employee employee) {

        employeeRepository.save(employee);
    }

    @Override
    public void delete(Employee employee) {

        employeeRepository.delete(employee);

    }

    @Override
    public int countAllEmployee(boolean activated) {

        return employeeRepository.countAll(activated);

    }

    @Override
    public int countAllEmployee(FashionBrand fashionBrand) {

        return employeeRepository.countAllByFashionBrand(fashionBrand);

    }

    @Override
    public List<Employee> getAllEmployeeByFashionBrand(FashionBrand fashionBrand) {

        return employeeRepository.getByFashionBrand(fashionBrand);
    }

}
