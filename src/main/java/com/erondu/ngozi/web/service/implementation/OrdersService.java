package com.erondu.ngozi.web.service.implementation;

import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.entity.Orders;
import com.erondu.ngozi.web.repository.OrdersRepository;
import com.erondu.ngozi.web.service.OrdersServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrdersService implements OrdersServiceInterface {

    @Autowired
    OrdersRepository ordersRepository;

    @Override
    public Orders save(Orders orders) {

        return ordersRepository.save(orders);

    }

    @Override
    public Orders getOrderByID(Long id) {

        return ordersRepository.getById(id);

    }

    @Override
    public int getTotalOrders(FashionBrand fashionBrand) {

        return ordersRepository.countByFashionBrand(fashionBrand);

    }

    @Override
    public int getTotalOrdersByStatus(FashionBrand fashionBrand, Enum.Status status) {

        return ordersRepository.countByFashionBrandAndStatus(fashionBrand, status);

    }

    @Override
    public int countAllByStatus(Enum.Status status) {

        return ordersRepository.countAllByStatus(status);

    }

    @Override
    public List<Orders> getAll() {

        return ordersRepository.findAll();
    }
}
