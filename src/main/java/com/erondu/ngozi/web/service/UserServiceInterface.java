package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.User;

public interface UserServiceInterface {

    String exists(User user);

    User save(User user);

    void update(User user);

    void delete(User user);

    User findUserByEmailAndPassword(String email, String password);

    User findUserByEmail(String Email);

    int countAll(boolean isActivated);

}
