package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.cloths.Weave;

import java.util.List;

public interface WeaveServiceInterface {

    Weave save(Weave weave);

    List<Weave> getAll();

    Weave getWeave(Long id);

    void delete(Weave weave);

}
