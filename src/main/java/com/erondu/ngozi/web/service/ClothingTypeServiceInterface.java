package com.erondu.ngozi.web.service;

import com.erondu.ngozi.web.entity.cloths.ClothingType;

import java.util.List;

public interface ClothingTypeServiceInterface {

    ClothingType getClothingType(Long id);

    ClothingType getClothingTypeByNameAndGender(String name, ClothingType.Gender gender);

    List<ClothingType> getAll();

    List<ClothingType> getAllByGenderIsNot(ClothingType.Gender gender);
}
