package com.erondu.ngozi.web.service.implementation;


import com.erondu.ngozi.web.entity.FashionBrand;
import com.erondu.ngozi.web.repository.FashionBrandRepository;
import com.erondu.ngozi.web.service.FashionBrandServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FashionBrandService implements FashionBrandServiceInterface {

    @Autowired
    FashionBrandRepository fashionBrandRepository;

    @Override
    public FashionBrand save(FashionBrand fashionBrand) {

        return fashionBrandRepository.save(fashionBrand);

    }

    @Override
    public FashionBrand update(FashionBrand fashionBrand) {

        return fashionBrandRepository.save(fashionBrand);
    }

    @Override
    public int countAllFashionBrand() {

        return fashionBrandRepository.countAllFashionBrand();

    }

    @Override
    public FashionBrand getFashionBrand(Long id) {

        return fashionBrandRepository.findOne(id);

    }

    @Override
    public Page<FashionBrand> getFashionBrand(Pageable pageable) {

        return fashionBrandRepository.getAllFashionBrandAndOrderByBrandNameAsc(pageable);
    }

    @Override
    public Page<FashionBrand> getFashionBrandByDistance(int clothType, double longitude, double latitude, double distance, Pageable pageable) {

        return fashionBrandRepository.getNearestFashionBrand(clothType, longitude, latitude, distance, pageable);

    }

    @Override
    public Page<FashionBrand> getFashionBrand(String containing, Pageable pageable) {

        return fashionBrandRepository.getFashionBrandByBrandNameIsContaining(containing, pageable);

    }
}
