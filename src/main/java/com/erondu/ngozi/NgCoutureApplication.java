package com.erondu.ngozi;

import com.erondu.ngozi.web.entity.*;
import com.erondu.ngozi.web.entity.Enum;
import com.erondu.ngozi.web.entity.cloths.Fibre;
import com.erondu.ngozi.web.entity.cloths.Material;
import com.erondu.ngozi.web.entity.cloths.Weave;
import com.erondu.ngozi.web.entity.cloths.Yarn;
import com.erondu.ngozi.web.service.implementation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NgCoutureApplication {

    @Autowired
    FashionBrandService fashionBrandService;

    @Autowired
    ClothingTypeService clothingTypeService;

    @Autowired
    MaterialService materialService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    PricingService pricingService;

    @Autowired
    UserService userService;

    @Autowired
    TransactionService transactionService;

    public static void main(String[] args) {

		SpringApplication.run(NgCoutureApplication.class, args);



	}

    @Bean
    public CommandLineRunner loadData() {
        return (args) -> {

            FashionBrand fashionBrand = new FashionBrand();

            fashionBrand.setBankDetail(
                    new FashionBrandBankDetail()
                            .setAccountName("Eshiett Oto-obong")
                            .setAccountNumber("210000469").setBankName("Zenith")//.setFashionBrand(fashionBrand)
            ).setBrandPhrase("Here to concqour")
                    .setPhoneNumber("08098516969")
                    .setLocation("VGC, Nigeria")
                    .setEmail("Brand@brand.com")
                    .setWebsite("www.brand.com")
                    .setImage("Brand")
                    .setBrandName("Brand")
                    .setGpsCoordinate(
                            new GpsCoordinate().setLatitude(6.444)
                            .setLongitude(6.444)

                    );

            FashionBrand saveFashionBrand = fashionBrandService.save(fashionBrand);


            Employee employee = new Employee();

            employee.setFashionBrand(saveFashionBrand);

            employee.setPosition(Employee.POSITION.ADMIN);

            employee.setActivated(true);

            employee.setEmail("eshiett1995@gmail.com");

            employee.setPassword("iam2fresh");

            employee.setFirstName("Oto-obong");

            employee.setLastName("Bassey");

            employee.setDateCreated();

            employeeService.save(employee);

            Material material = new Material();

            material.setImageURL("blue")
                    .setColor("blue")
                    .setDetail("for everyone")
                    .setName("Sexy xy")
                    .setPrice(5000)
                    .setYarn(new Yarn(33.3, 34.3))
                    .setWeight(34.3)
                    .setClothingType(clothingTypeService.getClothingType((long) 1))
                    .setFashionBrand(saveFashionBrand)
                    .setFiberPercentage(4)
                    .setFibre(new Fibre("Cotton"))
                    .setWeave(new Weave("polyester"));

            materialService.save(material);

            Pricing pricing = new Pricing();

            pricing.setFashionBrand(saveFashionBrand)
                    .setClothingType(clothingTypeService.getClothingType((long) 1))
                    .setPrice(700)
                    .setDateCreated();

            pricingService.save(pricing);


            User user = new User();

            user.setActivated(true)
                    .setAddress("VGC")
                    .setEmail("eshiett1995@gmail.com")
                    .setFirstName("Eshiett")
                    .setLastName("Oto-obong")
                    .setPassword("iam2fresh")
                    .setPhoneNumber("08098516969")
                    .setShirtMeasurement(new ShirtMeasurement())
                    .setTrouserMeasurement(new TrouserMeasurement())
                    .setUserGpsCoordinate(new UserGpsCoordinate());

            User savedUser = userService.save(user);

            ClothModel clothModel = new ClothModel();

            clothModel.setQuantity(1)
                  .setModelStructure("efefef")
                  .setClothingType(clothingTypeService.getClothingType((long) 1))
                    .setAmount(700)
                  .setSavedUrl("EshiettOto-obong-2018-09-30-(order1).json");

            ClothModel clothModel2 = new ClothModel();

            clothModel2.setQuantity(1)
                    .setModelStructure("efefef")
                    .setClothingType(clothingTypeService.getClothingType((long) 1))
                    .setAmount(10700)
                    .setSavedUrl("EshiettOto-obong-2018-09-31-(order1).json");


            Orders orders = new Orders();

            orders.setUser(savedUser)
                    .setFashionBrand(saveFashionBrand)
                    .setUser(savedUser)
                    .setAmount(10700)
                    .setStatus(Enum.Status.PENDING)
                    .setClothModels(clothModel);

            orders.setClothModels(clothModel2);

            Transaction transaction = new Transaction();

            transaction.setOrders(orders)
                    .setUser(savedUser)
                    .setSuccessful(false)
                    .setAmount(10700)
                    .setDateCreated();

            transactionService.save(transaction);
        };
    }
}
