

function getGpsCoordinate () {



    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(showPosition,

            function error(msg){alert('Please enable your GPS position future.');

        });

    } else {
        //x.innerHTML = "Geolocation is not supported by this browser.";

        toast("error","Geolocation is not supported by this browser, please update your browser")

    }
}


function doit() {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            var accuracy = position.coords.accuracy;
            var coords = new google.maps.LatLng(latitude, longitude);
            var mapOptions = {
                zoom: 15,
                center: coords,
                mapTypeControl: true,
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.SMALL
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var capa = document.getElementById("capa");
            capa.innerHTML = "latitud: " + latitude + " longitud: " + "   aquesta es la precisio en metres  :  " + accuracy;

            map = new google.maps.Map(
                document.getElementById("mapContainer"), mapOptions
            );
            var marker = new google.maps.Marker({
                position: coords,
                map: map,
                title: "ok"
            });


        },function error(msg){alert('Please enable your GPS position future.');

        }, {maximumAge:600000, timeout:5000, enableHighAccuracy: true});

    }else {
        alert("Geolocation API is not supported in your browser.");
    }
}


function showPosition(position) {

    var coordinate = position.coords.latitude + "&" + position.coords.longitude;

    console.log(position);

    createCookie(coordinate_cookie_name, coordinate);

    alert(coordinate);

};

function createCookie(name, value) {

    var cookieName = name;

    var cookieValue = value;

    var myDate = new Date();

    myDate.setMonth(myDate.getMonth() + 12);

    document.cookie = cookieName +"=" + cookieValue + ";expires=" + myDate + ";domain=" + cookie_domain + ";path=/";

}


function shake(element) {

    $(element).shake({
        count: 4,
        distance: 15,
        duration: 100
    });

}

function toast(type, title ) {

    const toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    toast({
        type: type,
        title: title
    })

}

function  post(url,data,callback) {

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: data,

        success: function(response) {

            callback(response);

        }
    });

}


function get(url,callback) {

    $.get( url , function( data ) {

        callback(data);

    });

}

function initToolTip() {

    $(function () {

        $('[data-toggle="tooltip"]').tooltip()

    })

}

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180;
    var radlat2 = Math.PI * lat2/180;
    var theta = lon1-lon2;
    var radtheta = Math.PI * theta/180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
}


if (typeof JSON.decycle !== 'function') {
    JSON.decycle = function decycle(object) {
        'use strict';

// Make a deep copy of an object or array, assuring that there is at most
// one instance of each object or array in the resulting structure. The
// duplicate references (which might be forming cycles) are replaced with
// an object of the form
//      {$ref: PATH}
// where the PATH is a JSONPath string that locates the first occurance.
// So,
//      var a = [];
//      a[0] = a;
//      return JSON.stringify(JSON.decycle(a));
// produces the string '[{"$ref":"$"}]'.

// JSONPath is used to locate the unique object. $ indicates the top level of
// the object or array. [NUMBER] or [STRING] indicates a child member or
// property.

        var objects = [],   // Keep a reference to each unique object or array
            paths = [];     // Keep the path to each unique object or array

        return (function derez(value, path) {

// The derez recurses through the object, producing the deep copy.

            var i,          // The loop counter
                name,       // Property name
                nu;         // The new object or array

// typeof null === 'object', so go on if this value is really an object but not
// one of the weird builtin objects.

            if (typeof value === 'object' && value !== null &&
                !(value instanceof Boolean) &&
                !(value instanceof Date)    &&
                !(value instanceof Number)  &&
                !(value instanceof RegExp)  &&
                !(value instanceof String)) {

// If the value is an object or array, look to see if we have already
// encountered it. If so, return a $ref/path object. This is a hard way,
// linear search that will get slower as the number of unique objects grows.

                for (i = 0; i < objects.length; i += 1) {
                    if (objects[i] === value) {
                        return {$ref: paths[i]};
                    }
                }

// Otherwise, accumulate the unique value and its path.

                objects.push(value);
                paths.push(path);

// If it is an array, replicate the array.

                if (Object.prototype.toString.apply(value) === '[object Array]') {
                    nu = [];
                    for (i = 0; i < value.length; i += 1) {
                        nu[i] = derez(value[i], path + '[' + i + ']');
                    }
                } else {

// If it is an object, replicate the object.

                    nu = {};
                    for (name in value) {
                        if (Object.prototype.hasOwnProperty.call(value, name)) {
                            nu[name] = derez(value[name],
                                path + '[' + JSON.stringify(name) + ']');
                        }
                    }
                }
                return nu;
            }
            return value;
        }(object, '$'));
    };
}