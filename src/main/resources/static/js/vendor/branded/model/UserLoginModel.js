
var loginModel = function(){

    var model = {

        email : ko.observable(''),

        password : ko.observable(''),

    };

    model.validate = function (callback) {

        if(model.email() === ""){

            return callback(false,"Enter an email");

        }

        if(model.password() === ""){

            return callback(false,"Enter a password");

        }

        return callback(true,"validated!");

    };

    model.login = function(){

        model.validate(function (isvalid, msg) {

            if(!isvalid){

                toast("error",msg);

                console.log('got there');

                return;


            }else{

                console.log('didnt get there');

                post(base_url +"welcome/login",ko.toJSON(model),function (data) {

                    if(data.isSuccessful){

                        toast("success","success");

                        window.location.replace(base_url + "/home");

                    }else{

                        toast("error",data.responseMessage);

                    }

                })

            }

        });




    };

    return model;

};