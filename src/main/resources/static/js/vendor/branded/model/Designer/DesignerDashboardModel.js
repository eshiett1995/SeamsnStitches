
var designerDashboardModel = function(data){

    var model = ko.mapping.fromJS(data);

    console.log(model);

    model.dashboardModel.employee.repassword = ko.observable("");

    model.isNotAdmin = ko.computed(function () {

        return model.dashboardModel.employee.position.$name() != "ADMIN";

    });

    model.logout = function () {

        window.location.href = base_url + "designer/logout"

    };


    model.newEmployee = {

        firstName : ko.observable(""),

        lastName : ko.observable(""),

        email : ko.observable(""),

        fashionBrand: ko.observable(model.dashboardModel.employee.fashionBrand),

        position: ko.observable(""),

        password: ko.observable(""),

        repassword : ko.observable(""),



    };

    model.noOfServices = ko.computed(function() {

        var count = 0;

        ko.utils.objectForEach(model.dashboardModel.employee.fashionBrand.services, function (key, value) {

            if(key != "id"){

                count = value() == true ? count + 1 : count + 0;
            }


        });

        return count;

    }, this);


    var tempModel = ko.mapping.fromJS(data);

    tempModel.dashboardModel.employee.repassword = ko.observable("");

    model.changePricing = function () {

        $('#pricingModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.pricing) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.pricing).replace("/","")){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "pricing has been successfully update");

                    //tempModel.employee.fashionBrand = ko.mapping.fromJS(data.fashionBrand);


                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeServices = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.services) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.services)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "services has been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeDetails = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "Fashion Brand detail have been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.addNewEmployee = function () {

        if(!model.validatePassword()){

            return;

        }

        toast("info", "Adding new Employee.");

         post(base_url + "designer/add/employee", ko.toJSON(model.newEmployee), function (data) {

             if(data.isSuccessful){

                 toast("success", "Employee successfully added");

                 model.newEmployee.firstName("");

                 model.newEmployee.lastName("");

                 model.newEmployee.email("");

                 model.newEmployee.password("");

                 model.newEmployee.repassword("");

             }else{

                 toast("error", data.responseMessage)

             }

         });

    };

    model.updateFashionBrand = function (employee, callback) {

        console.log(ko.toJSON(employee));

        post(base_url + "designer/update/fashionbrand",ko.toJSON(employee), function (data) {

            callback(data);

        } );

    };

    model.validatePassword = function () {

        if(String(model.newEmployee.password().trim()) === String("")){

            toast("error", "Please enter a password");

            return false;

        }

        if(String(model.newEmployee.repassword().trim()) === String("")){

            toast("error", "Please retype the password");

            return false;

        }

        if(String(model.newEmployee.password()) != String(model.newEmployee.repassword())){

            toast("error", "inconsistent password");

            return false;

        }

        return true;
    };

    console.log(model);

    return model;

};