
var materialDashBoardModel = function(data){

    var model = ko.mapping.fromJS(data);



    model.name = ko.observable("");

    model.color = ko.observable("");

    model.detail = ko.observable("");

    model.fiberPercentage = ko.observable("");

    model.price = ko.observable("");

    model.weight = ko.observable("");

    model.image = ko.observable("");

    model.fibre = ko.observable("");

    model.weave = ko.observable("");

    model.yarn = {

        warp: ko.observable(""),

        weft: ko.observable(""),

    };

    model.clothStyle = ko.observable("");


    console.log(model);

    model.fileUpload = function(data, e)
    {
        var _URL = window.URL || window.webkitURL;
        var file    = e.target.files[0];
        var reader  = new FileReader();
        img = new Image();

        img.onload = function () {

            if(Number(this.width) >= Number(900) && Number(this.height) >= Number(900)){

            }else{

                model.image('');

                toast("error", "image dimension must be 900x900");

            }

        };

        reader.onloadend = function (onloadend_e)
        {
            var result = reader.result; // Here is your base 64 encoded file. Do with it what you want.

            //model.image(result);

        };

        if(file)
        {
            reader.readAsDataURL(file);

            img.src = _URL.createObjectURL(file);

            model.image(file);
        }
    };



    model.addNewMaterial = function () {


        var material = {

            name : model.name(),

            color : model.color(),

            detail : model.detail(),

            weight : model.weight(),

            price : model.price(),

            fibre : model.fibre(),

            weave : model.weave(),

            yarn : model.yarn,

            fiberPercentage : model.fiberPercentage(),

            clothStyle : model.clothStyle()

        };

        var data = new FormData();

        data.append( 'file', model.image());

        data.append('model',JSON.stringify(ko.toJS(material)));

        $.ajax({

            url: base_url +"admin/save/material",
            type: 'POST',
            contentType: false,
            processData: false,
            data: data,

            success: function(response) {

                console.log(response);

                if(response.isSuccessful) {

                   toast('success', "Material has been saved")

                }else {

                    toast("error",response.responseMessage);

                }

            }
        });

    };

    model.logout = function () {


    };


    model.initDataTable = function () {

        $('#datatables').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "admin/query/material",
                "data": function ( data ) {
                    //process data before sent to server.
                }},
            "columns": [
                { "data": "id", "name" : "ID", "title" : "ID"  },
                { "data": "name", "name" : "Name" , "title" : "Name"},
                { "data": "color", "name" : "Color" , "title" : "Color"},
                { "data": "price", "name" : "Price" , "title" : "price"},
                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="viewBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'deleteData('+JSON.stringify(full)+', this )\' >' + 'Delete' + '</button>';
                        return actionhtml;

                    }},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="viewBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'deleteData('+JSON.stringify(full)+', this )\' >' + 'Delete' + '</button>';
                        return actionhtml;

                    }},
            ]

        } );

    };



    return model;

};