
var weaveDashBoardModel = function(data){

    var model = {};

    model.name = ko.observable("");


    model.addNewWeave = function () {

        var fibreModel = {};

        fibreModel.name = model.name();

        post(base_url + "admin/save/weave", ko.toJSON(fibreModel), function (data) {

            if(data.isSuccessful){

                toast("success","Weave successfully saved");

            }else{

                toast("error","Oops!! an error has occurred");
            }

        });

    };

    model.logout = function () {


    };


    model.initDataTable = function () {

        $('#datatables').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "admin/query/weave",
                "data": function ( data ) {

                   console.log(data);

                }},
            "columns": [
                { "data": "id", "name" : "ID", "title" : "ID"  },
                { "data": "name", "name" : "Name" , "title" : "Name"},
                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {



                        var actionhtml='<button id="viewBtn" type="button" class="btn purple-gradient btn-sm" onclick=\'viewData('+JSON.stringify(full)+')\'>' + 'View' + '</button>';
                        return actionhtml;
                    }},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="viewBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'deleteData('+JSON.stringify(full)+', this )\' >' + 'Delete' + '</button>';
                        return actionhtml;

                    }},
            ]

        } );

    };

    return model;

};