
function orderViewerModel(data) {



    var model = {};


    model.order = ko.observable(ko.mapping.fromJS(data.orders));

    model.employees = ko.observableArray(data.employees);

    model.fashionBrand = ko.observable(ko.mapping.fromJS(data.fashionbrand));

    model.clothModelsTaskList = ko.observable(data.clothModelsTaskList);

    model.clothDetailsArray = ko.observableArray([]);


    /************************************************************************************************************/
    /*********************************** TASKS FUNCTIONS AND VARIABLES ******************************************/
    /************************************************************************************************************/


    model.updatingTask = ko.observable(false);

    //generic tasks model
    model.tasks = ko.observableArray([]);

    //model class for a single task model
    model.task = {

     id : ko.observable(0),

     clothModelId : ko.observable(0),

     clothModel : ko.observable(),

     details : ko.observable(""),

     completed : ko.observable(false),

     employee : ko.observable()

    };

    model.saveTask = function () {

        if(model.task.employee() === undefined){

            toast("error", "Please select an employee");

            return;
        }

        if(model.task.details().trim() === ""){

            toast("error", "Task description cannot be empty");

            return
        }


        var foundTask = ko.utils.arrayFirst(model.tasks(), function(task) {

            return task.employee.firstName === model.task.employee().firstName;

        });


        if(foundTask != undefined){

            model.task.details("");

            toast("error", "Employee has already been given a task");

            return;

        }

        // sets a cloth-model to the present tasks own cloth model
        model.task.clothModel(model.order().clothModels()[model.presentClothModelNumber()]);

        model.task.clothModelId(model.order().clothModels()[model.presentClothModelNumber()].id());


        // this is for the enum in cloth-model

        if(model.task.clothModel().clothingType.gender != "Male" || model.task.clothModel().clothingType.gender != "Female") {

            model.task.clothModel().clothingType.gender = model.task.clothModel().clothingType.gender.$name;

        }

        //this is for the enum in employee-model
        model.task.employee().position = model.task.employee().position.$name;


        post(base_url + "designer/task/save", ko.toJSON(model.task), function (response) {


            if(response.responseModel.isSuccessful){

                model.order().clothModels()[model.presentClothModelNumber()].tasks.push(response.task);

                model.tasks(model.order().clothModels()[model.presentClothModelNumber()].tasks());

                model.task.details(undefined);

                toast("success", "Task successfully updated");


            }else{

                toast("error", response.responseModel.responseMessage);

            }

        });



    };

    model.deleteTask = function (task) {

        post(base_url + "designer/task/delete", JSON.stringify(task), function (response) {



           if(response.isSuccessful){


               model.tasks.remove(task);

                toast("success", "Task successfully updated");


            }else{

                toast("error", response.responseModel.responseMessage);

            }

        });


    };

    model.startTaskUpdate = function (task) {

        model.task.id(task.id);

        model.task.clothModel(task.clothModel);

        model.task.details(task.details);

        model.task.completed(task.completed);

        model.task.employee(task.employee);


        model.updatingTask(true);

        $("#employeeNameField").val(task.employee.firstName + ' ' + task.employee.lastName);

    };

    model.updateTask = function () {

        if(model.task.details().trim() === ""){

            toast("error", "Task description cannot be empty");

            return
        }

        var foundTask = ko.utils.arrayFirst(model.tasks(), function(task) {

            return task.employee.firstName === model.task.employee().firstName &&

            task.employee.lastName === model.task.employee().lastName &&

            task.details === model.task.details();

        });


        if(foundTask != undefined){

            model.task.details("");

            toast("error", "Employee has already been given a task");

            return;

        }else {

            // sets a cloth-model to the present tasks own cloth model
            model.task.clothModel(model.order().clothModels()[model.presentClothModelNumber()]);

            // this is for the enum in cloth-model

            if (model.task.clothModel().clothingType.gender != "Male" || model.task.clothModel().clothingType.gender != "Female") {

                model.task.clothModel().clothingType.gender = model.task.clothModel().clothingType.gender.$name;

            }

            //this is for the enum in employee-model
            model.task.employee().position = model.task.employee().position.$name;


            // console.log(ko.toJSON(model.task));

            post(base_url + "designer/task/save", ko.toJSON(model.task), function (response) {


                if (response.responseModel.isSuccessful) {

                    var foundTask = ko.utils.arrayFirst(model.tasks(), function(task) {

                        return task.employee.firstName === model.task.employee().firstName;

                    });

                    model.tasks.remove(foundTask);

                    model.order().clothModels()[model.presentClothModelNumber()].tasks.push(response.task);

                    model.tasks(model.order().clothModels()[model.presentClothModelNumber()].tasks());

                    model.task.details(undefined);

                    model.cancelTaskUpdate();

                    toast("success", "Task successfully updated");


                } else {

                    toast("error", response.responseModel.responseMessage);

                }

            });

        }

    };

    model.cancelTaskUpdate = function () {

        model.task.id(undefined);

        model.task.clothModel(undefined);

        model.task.details(undefined);

        model.task.completed(undefined);


        model.updatingTask(false);

        $('.mdb-select').material_select('destroy');


        $('.mdb-select').material_select();

    };



    /************************************************************************************************************/
    /****************************** CLOTH MODEL CHANGE FUNCTIONS AND VARIABLES **********************************/
    /************************************************************************************************************/

    model.presentClothModelNumber = ko.observable(0);

    model.clothModelCalculatedDisplay = ko.computed(function() {

        return (model.presentClothModelNumber() + 1) + "/" + model.order().clothModels().length;

    },this);

    model.initClothModel = function () {

        model.getJSON(model.order().clothModels()[model.presentClothModelNumber()], true);

        //sets the present clothModels task to the clothModelsTasklist synonymous index number;
        model.order().clothModels()[model.presentClothModelNumber()].tasks(model.clothModelsTaskList()[model.presentClothModelNumber()]);

        // add the cloth-model tasks to the main tasks model, so you can see the tasks meant for this model
        model.tasks(model.order().clothModels()[model.presentClothModelNumber()].tasks());


    };

    model.nextClothModel = function () {

        if(model.order().clothModels().length <= 1) return;

        if((model.presentClothModelNumber() + 1) === model.order().clothModels().length) return;

        model.resetGroupModel();

        model.presentClothModelNumber(model.presentClothModelNumber() + 1);

        //sets the present clothModels task to the clothModelsTasklist synonymous index number;
        model.order().clothModels()[model.presentClothModelNumber()].tasks(model.clothModelsTaskList()[model.presentClothModelNumber()]);


        // add the cloth-model tasks to the main tasks model, so you can see the tasks meant for this model
        model.tasks(model.order().clothModels()[model.presentClothModelNumber()].tasks());

        model.getJSON(model.order().clothModels()[model.presentClothModelNumber()], false);
    };

    model.previousClothModel = function () {

        if((model.presentClothModelNumber() - 1) < 0) return;

        model.resetGroupModel();

        model.presentClothModelNumber(model.presentClothModelNumber() - 1);

        // add the cloth-model tasks to the main tasks model, so you can see the tasks meant for this model
        model.tasks(model.order().clothModels()[model.presentClothModelNumber()].tasks());

        model.getJSON(model.order().clothModels()[model.presentClothModelNumber()], false);


    };

    model.resetGroupModel = function () {

        for (var i = group.children.length - 1; i >= 0; i--) {

            group.remove(group.children[i]);

        }

    };

    model.getJSON =  function (clothModel, isFromInit) {

        $.getJSON("../../js/vendor/branded/orders/" + clothModel.savedUrl(), function(json) {

            if(isFromInit) {

                model.loadModel(json);

            }else{

                model.addMesh(json);

            }

            model.createClothDetailsList(json);

        });

    };


    //this function is responsible for loading the first cloth model in the array
    model.initClothModel();





    /************************************************************************************************************/
    /***************************** THREE.JS 3D CLOTH MODEL METHODS AND VARIABLES ********************************/
    /************************************************************************************************************/



    var userRotation = new THREE.Vector3();

    var userZoom = new THREE.Vector3();

    var lastMousePosition;

    var group = new THREE.Group();

    var lerpyness = 0.1;

    var renderer; var scene; var camera;

    var maxTrans  = {x:0, y:0, z:0}; var minTrans  = {x:0, y:0, z:0}; var translate2Center = {x:0, y:0, z:0};


    model.clamp =function (val,min, max) {

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.scrollWheelClamp =function (val,min, max) {

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.lerp = function(va,vb,vv){return va+((vb-va)*vv);};

    model.loadModel = function (object){

        for (var property in object.Structure) {

            if (parts.Structure.hasOwnProperty(property)) {


                var option = object.Structure[property].selected;


                if(option != undefined) {

                    object.Structure[property].group_type[option].forEach(function (item) {


                        var temp = {};

                        temp.x = item.coordinate.x;
                        temp.y = item.coordinate.y;
                        temp.z = item.coordinate.z;

                        if (temp.x > maxTrans.x) maxTrans.x = temp.x;
                        else if (temp.x < minTrans.x) minTrans.x = temp.x;
                        if (temp.y > maxTrans.y) maxTrans.y = temp.y;
                        else if (temp.y < minTrans.y) minTrans.y = temp.y;
                        if (temp.z > maxTrans.z) maxTrans.z = temp.z;
                        else if (temp.z < minTrans.z) minTrans.z = temp.z;

                    });

                }
            }
        }

        translate2Center.x = minTrans.x + (maxTrans.x-minTrans.x)/2;
        translate2Center.y = minTrans.y + (maxTrans.y-minTrans.y)/2;
        translate2Center.z = minTrans.z + (maxTrans.z-minTrans.z)/2;



        for (var property in object.Structure) {

            if (parts.Structure.hasOwnProperty(property)) {


                if(object.Structure[property].selected != undefined) {

                    var option = object.Structure[property].selected;

                    object.Structure[property].group_type[option].forEach(function (item) {

                        var loader = new THREE.BufferGeometryLoader();

                        loader.load(
                            '../../'+item.model,

                            function (geometry) {


                                var uv = new THREE.TextureLoader().load("../../models/Material/shirt/DiffuseMap/" + "blue" + ".jpg");

                                uv.anisotropy  = renderer.capabilities.getMaxAnisotropy();


                                if(item.part === "thread"){

                                    var material = new THREE.MeshLambertMaterial({map: uv, color:0x000000});

                                }else{

                                    var material = new THREE.MeshLambertMaterial({map: uv});

                                }



                                material.side = THREE.DoubleSide;

                                var object = new THREE.Mesh(geometry, material);


                                object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                                object.position.y = item.coordinate.y - translate2Center.y;
                                object.position.z = item.coordinate.z - translate2Center.z;
                                object.part = item.part;
                                object.type = item.type;
                                object.group = item.group;
                                object.group_role = item.group_role;


                                //object.position.x = item.coordinate.x;
                                //object.position.y = item.coordinate.y;
                                //object.position.z = item.coordinate.z;

                                group.add(object);
                            }
                        );

                    });
                }else{

                    console.log("not here")

                }

            }

        }

        group.applyMatrix( new THREE.Matrix4().makeTranslation(
            translate2Center.x , translate2Center.y, translate2Center.z)
        );

        group.position.x = 0;
        group.position.y = 0.2;

// var box = new THREE.Box3().setFromObject( object );
        //box.center( object.position ); // this re-sets the mesh position
        //object.position.multiplyScalar( - 1 );



        scene = new THREE.Scene();

        var light = new THREE.AmbientLight( 0x404040,0.9 ); // soft white light
        scene.add( light );

        var spotLight = new THREE.SpotLight( 0xffffff );
        spotLight.position.set( 0.92, 0, -100 );


        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;


        var spotLight2 = new THREE.SpotLight( 0xffffff );
        spotLight2.position.set( 1, 20, 20.14);


        spotLight2.castShadow = true;

        spotLight2.intensity =0.95;

        spotLight2.shadow.mapSize.width = 1024;
        spotLight2.shadow.mapSize.height = 1024;

        spotLight2.shadow.camera.near = 500;
        spotLight2.shadow.camera.far = 4000;
        spotLight2.shadow.camera.fov = 30;



        scene.add( spotLight );

        scene.add( spotLight2);

        scene.add(group);

        console.log(group);



        camera = new THREE.PerspectiveCamera(30, window.innerWidth/window.innerHeight, 0.1,1000);




        renderer = new THREE.WebGLRenderer({ antialias: true });

        renderer.setPixelRatio( window.devicePixelRatio );

        renderer.shadowMapEnabled = true;

        renderer.shadowMapSoft = true;

        // var controls = new THREE.OrbitControls( camera,renderer.domElement );

        // controls.update();

        renderer.setClearColor(0xffffff);

        renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(renderer.domElement);

        window.addEventListener('resize', function () {

            var width = window.innerWidth;
            var height = window.innerHeight;
            renderer.setSize(width, height);
            camera.aspect = width/height;
            camera.updateProjectionMatrix();

        });




        //camera.position.x = -0.43;

        //camera.position.z = 1.78;

        //camera.position.y = 0.51;

        camera.lookAt(group.position);

        userZoom.z = 1.8;
        camera.position.lerp(userZoom,lerpyness);

        //$('#x').val(camera.position.x);
        //$('#y').val(camera.position.y);
        //$('#z').val(camera.position.z);

        function addEventListeners(){

            var mouseDown = false;
            renderer.domElement.addEventListener('mousedown',function(evt){mouseDown=true; });

            renderer.domElement.addEventListener('mouseup',function(evt){mouseDown=false;});

            renderer.domElement.addEventListener('mousemove',function(evt){


                if(!lastMousePosition)lastMousePosition = new THREE.Vector2(evt.clientX,evt.clientY);
                if(mouseDown){

                    userRotation.y = model.clamp(userRotation.y+(evt.clientX-lastMousePosition.x)*0.01,-Math.PI*5,Math.PI*5);
                    userRotation.x= model.clamp(userRotation.x+(evt.clientY-lastMousePosition.y)*0.01,-Math.PI*0.5,Math.PI*0.5);
                }
                lastMousePosition.set(evt.clientX,evt.clientY);
            });

            window.addEventListener('mousewheel',function(evt){

                userZoom.z= model.scrollWheelClamp(userZoom.z+evt.wheelDelta*-0.01,1.8,15);
            });


            var obj = renderer.domElement;//document.getElementById('id');
            var lastTouch;
            obj.addEventListener('touchmove', function(event) {
                // If there's exactly one finger inside this element
                if (event.targetTouches.length == 1) {
                    var touch = event.targetTouches[0];
                    // Place element where the finger is
                    //obj.style.left = touch.pageX + 'px';
                    //obj.style.top = touch.pageY + 'px';
                    if(!lastTouch)lastTouch = new THREE.Vector2(touch.pageX,touch.pageY);

                    userRotation.y = model.clamp(userRotation.y+(touch.pageX-lastTouch.x)*0.01,-Math.PI*5,Math.PI*5);

                    userRotation.x = model.clamp(userRotation.x+(touch.pageY-lastTouch.y)*0.01,-Math.PI*0.5,Math.PI*0.5);

                    lastTouch.set(touch.pageX,touch.pageY);
                    event.preventDefault();
                }
            }, false);



        }

        var update = function () {

            // group.rotation.y += 0.001;

        };

        var render = function () {

            renderer.render(scene,camera);
        };

        var GameLoop = function () {

            camera.position.lerp(userZoom,lerpyness);

            requestAnimationFrame(GameLoop);

            update();

            if(group){
                group.rotation.x = model.lerp(group.rotation.x,userRotation.x,lerpyness);
                group.rotation.y = model.lerp(group.rotation.y,userRotation.y,lerpyness);
            }

            render();

        };


        GameLoop();

        addEventListeners();

        $(".fakeloader").fakeLoader({
            timeToHide:3000,
            bgColor:"#e74c3c",
            //spinner:"spinner2",

        });


    };


    /*********************************************************************/
    /***************** UTILITY RENDER 3D MODEL METHOD ********************/
    /*********************************************************************/

    model.addMesh = function (object) {



        for (var property in object.Structure) {

            var groupType = object.Structure[property].selected;

            console.log(object.Structure[property]);

            if(groupType != undefined) {

                parts.Structure[property].group_type[groupType].forEach(function (item) {


                    var loader = new THREE.BufferGeometryLoader();

                    loader.load(
                        "../../" + item.model,

                        function (geometry) {

                            var uv = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + "blue" + '.jpg');

                            var material = new THREE.MeshLambertMaterial({map: uv});

                            material.side = THREE.DoubleSide;

                            var object = new THREE.Mesh(geometry, material);


                            object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                            object.position.y = item.coordinate.y - translate2Center.y;
                            object.position.z = item.coordinate.z - translate2Center.z;
                            object.part = item.part;
                            object.type = item.type;
                            object.group = item.group;
                            object.group_role = item.group_role;


                            group.add(object);
                        }
                    );

                });
            }
        }

    };



    /*********************************************************************/
    /**************** UTILITY CLOTH DESCRIPTION METHODS *******************/
    /*********************************************************************/


    model.createClothDetailsList = function (object) {

        var clothDetailsArray = [];

        for (var property in object.Structure) {

            if (parts.Structure.hasOwnProperty(property)) {

                var clothDetailsObject = {};

                var nameOfSelectedPart = object.Structure[property].selected;

                clothDetailsObject.part = property;

                clothDetailsObject.selected = model.formatSelectedPartName(nameOfSelectedPart);

                clothDetailsObject.materialsArray = model.getMaterialsUsed(property, nameOfSelectedPart, object);

                clothDetailsArray.push(ko.mapping.fromJS(clothDetailsObject));


            };
        }

        model.clothDetailsArray(clothDetailsArray);


    };

    model.formatSelectedPartName = function (name) {

        if(name === undefined){

            return "None"

        }else{

            var count = (name.match(/_/g) || []).length;

            for(var i = 0; i <= count; i++){

                name = name.replace("_" , " ");

            }

            count = (name.match(/-/g) || []).length;

            for(var i = 0; i <= count; i++){

                name = name.replace("-" , " ");

            }

            return name;

        }

    };

    model.getMaterialsUsed = function (property,selected, object) {


        var materialsUsed = [];


        var selectedOption = object.Structure[property].selected;


        if(selectedOption != undefined) {

            object.Structure[property].group_type[selectedOption].forEach(function (newItem) {

                var foundMatch = false;

                materialsUsed.forEach(function (item) {

                    if(materialsUsed.length == 0){

                        foundMatch =  false;

                        return

                    }

                    if(newItem.diffuse.has_diffuse) {


                        if (item.object.name === object.Material.name.trim()) {

                            foundMatch = true;


                            item.clothPartsUsedOn.push(newItem.group_role);

                            return;

                        } else {

                            foundMatch = false;

                        }

                    }else{

                        if (item.object.name === newItem.contrast.contrast_diffuse.name.trim()) {

                            foundMatch = true;

                            item.clothPartsUsedOn.push(newItem.group_role);


                            return;

                        } else {

                            foundMatch = false;

                        }

                    }

                });

                if(!foundMatch){

                    var material = {};

                    if(newItem.diffuse.has_diffuse){

                        material.isContrast = false;

                        material.clothPartsUsedOn = [newItem.group_role];

                        material.object = object.Material;

                    }else{

                        material.isContrast = false;

                        material.clothPartsUsedOn = [newItem.group_role];

                        material.clothpart = newItem.group_role;

                        material.object = object.Material;


                    }

                    materialsUsed.push(material);
                }


            });
        }




        return materialsUsed;

    };

    /*********************************************************************/
    /*********************SHOW MODALS METHODS*****************************/
    /*********************************************************************/


    model.showClothDetailsModal = function (tin) {

        $('#clothDetailsModal').modal('show')

    };

    model.showTasksDetailsModal = function (tin) {

        $('#tasksModal').modal('show')

    };

    model.showUserDetailsModal = function (tin) {

        $('#userDetailModal').modal('show')

    };


    model.showUserMeasurementModal = function (tin) {

        $('#usersMeasurementModal').modal('show')

    };

    model.showMaterialModal = function (material) {



        $("#material-name").text(material.object.name() + ', ' +  material.object.color());

        $("#material-property").text(material.object.weave.name() + ', ' + material.object.fiberPercentage() + "%" + " " + material.object.fibre.name());

        $("#material-description").text(material.object.detail());

        var partsUsedOnString = "";

        material.clothPartsUsedOn().forEach(function (clothPart) {

            var formattedClothPartString = clothPart.replace(/-/g, ' ');

            partsUsedOnString = partsUsedOnString + formattedClothPartString + ",  ";

        });

        $("#used-on").text(partsUsedOnString);


        $('#materialDetailModal').modal('show')

    };

    return model;

};