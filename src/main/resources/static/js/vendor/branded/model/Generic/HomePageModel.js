

var homePageModel = function(data){

    var model = ko.mapping.fromJS(data);

    model.searchInputs = {

        searchText : ko.observable(""),

        checkboxMale : ko.observable(false),

        checkboxFemale : ko.observable(false),

    };

    model.showClothingTypeSearchModal = function () {

        $('#clothingTypeSearchModal').modal('show');

    };

    getGpsCoordinate();

    /********************************************************************/
    /*********************SUBSCRIBE MODELS ******************************/
    /********************************************************************/

    model.searchInputs.searchText.subscribe(function(text) {

        model.queryClothingTypeArray();

    });

    model.searchInputs.checkboxMale.subscribe(function(value) {

        model.queryClothingTypeArray();

    });

    model.searchInputs.checkboxFemale.subscribe(function(value) {

        model.queryClothingTypeArray();

    });
    
    model.queryClothingTypeArray = function () {

        var jsonFilteredArray = ko.toJS(model.clothingTypes());

        var newObservableClothingTypeArrayToBeQueried = ko.mapping.fromJS(jsonFilteredArray);


        if(model.searchInputs.searchText().trim() != "") {

            var filteredClothingTypeArrayByName = ko.utils.arrayFilter(newObservableClothingTypeArrayToBeQueried(), function (clothype) {

                return clothype.name().match(new RegExp(model.searchInputs.searchText().trim(),"gi"));

            });

            newObservableClothingTypeArrayToBeQueried(filteredClothingTypeArrayByName);

        }

        if(model.searchInputs.checkboxMale() === true) {

            var filteredClothingTypeArrayByMaleGender = ko.utils.arrayFilter(newObservableClothingTypeArrayToBeQueried(), function (clothype) {

                if(model.searchInputs.checkboxFemale() == false) {

                    return clothype.gender.$name() == "Male";

                }else{

                    return clothype.gender.$name() == "Male" ||  clothype.gender.$name() == "Female";

                }

            });

            newObservableClothingTypeArrayToBeQueried(filteredClothingTypeArrayByMaleGender);

        }


        if(model.searchInputs.checkboxFemale() === true) {

            var filteredClothingTypeArrayByFemaleGender = ko.utils.arrayFilter(newObservableClothingTypeArrayToBeQueried(), function (clothype) {

                if(model.searchInputs.checkboxMale() == false) {

                    return clothype.gender.$name() == "Female";

                }else{

                    return clothype.gender.$name() == "Male" ||  clothype.gender.$name() == "Female";

                }

            });

            newObservableClothingTypeArrayToBeQueried(filteredClothingTypeArrayByFemaleGender);

        }


        model.filteredClothingTypes(newObservableClothingTypeArrayToBeQueried());





        //model.filteredClothingTypes(ko.mapping.toJS(jsonFilteredArray));


        
    };







    /********************************************************************/
    /*********************MENU BUTTON ACTIONS****************************/
    /********************************************************************/



     model.goToRegistrationPage = function () {

         window.location.href = base_url + "signup";

     };

      model.goToLoginPage = function () {

          window.location.href = base_url + "login";

      };

      model.goToBrandsPage = function () {

          window.location.href = base_url + "designers";

      };




    /********************************************************************/
    /******************GENERIC CUSTOMIZATION PAGE REDIRECT***************/
    /********************************************************************/


    model.goToCustomizationPage = function (clothingStyle) {

        var formattedURL = base_url + "design" + "/" + clothingStyle.gender.$name().toLowerCase() + "/" + clothingStyle.name().replace(" ", "-").toLowerCase();

        window.location.href = formattedURL;

    };



    return model;

};