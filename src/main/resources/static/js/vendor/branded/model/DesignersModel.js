
var designersModel = function(data){


    var model = {};

    model.fashionbrandsPage = ko.observable(ko.mapping.fromJS(data.fashionbrandsPage));

    model.fashionbrand = ko.observable(ko.mapping.fromJS(data.fashionbrand));

    model.fashionbrand().website('');

    model.fashionbrand().brandName('');

    model.fashionbrand().location('');

    model.fashionbrand().rating(0);

    model.isFetchingFashionBrand = ko.observable(false);



    console.log(model);

    model.searchText = ko.observable("");

    model.searchParams = {

        searchKey : ko.observable("")

    };

    model.searchText.subscribe(function(newValue) {


        if(newValue === "") {  // i had to increase the white space over here cause JQuery post wouldn't send a single empty param to the server (Throws and error);

            newValue = "  ";

        }

        if(model.searchParams.searchKey().trim() === newValue.trim()){

            //todo x.replace(/^\s+|\s+$/gm,'');  use incase browser doesnt support trim;

            console.log("it was another space");

           return;

        }

        model.searchParams.searchKey(newValue);

        post(Base_URL + '/generic/paginated/brand/' +  0, newValue, function (response) {

                model.fashionbrandsPage(ko.mapping.fromJS(response.fashionBrandPage));

                console.log(response);

          });

    });

    model.scrollingDesignerList = function (data,event) {

        var element = event.target;

        // added the -100 so that the api can get loaded before the user reaches the end

        if ((element.scrollHeight - $(element).scrollTop()) - 100 <= $(element).height()){

            var nextPageNumber = model.fashionbrandsPage().number() + 1;

            if(nextPageNumber ===  model.fashionbrandsPage().totalPages()){

                return;

            }else{

                model.isFetchingFashionBrand(true);
            }

            post(base_url + '/generic/paginated/brand/' +  nextPageNumber, {searchKey : model.searchParams.searchKey()}, function (response) {



            })

        }


    };


    model.showBrandDetail = function (fashionbrand) {

        console.log(fashionbrand);

        model.fashionbrand(fashionbrand);

       // model.fashionbrand().brandName(fashionbrand.brandName());
       // model.fashionbrand().website(fashionbrand.website());
       // model.fashionbrand().rating(fashionbrand.rating());
       // model.fashionbrand().location(fashionbrand.location());
        //model.fashionbrand().location(fashionbrand.location());


        $('#brandDetailModal').modal('show');
    };


    model.groupedArray = ko.computed(function() {
        var rows = [];

        model.fashionbrandsPage().content().forEach(function(fashionbrand, i) {
            // whenever i = 0, 3, 6 etc, we need to initialize the inner array

            if (i % 3 == 0)

                rows[i/3] = [];

            rows[Math.floor(i/3)][i % 3] = fashionbrand;
        });

        return rows;
    });

    model.shouldBeHeader = function (fashionbrand) {


        var currentFashionBrandIndex = model.fashionbrandsPage().content().indexOf(fashionbrand);

        console.log(currentFashionBrandIndex);

         if(currentFashionBrandIndex == 0 ){

             return true;

         }else{

             var previousFashionBrand  =  model.fashionbrandsPage().content()[currentFashionBrandIndex - 1];

             if(previousFashionBrand.brandName().charAt(0).toLowerCase() != fashionbrand.brandName().charAt(0).toLowerCase()){

                return true;

             }

         }

         return false;

    };




    return model;

};