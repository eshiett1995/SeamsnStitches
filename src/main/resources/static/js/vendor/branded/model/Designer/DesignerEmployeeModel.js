
var designerEmployeeModel = function(data){

    var model = ko.mapping.fromJS(data);

    model.dashboardModel.employee.repassword = ko.observable("");


    model.isNotAdmin = ko.computed(function () {

        return model.dashboardModel.employee.position.$name() != "ADMIN";

    });

    model.logout = function () {

        window.location.href = base_url + "designer/logout"

    };


    model.newEmployee = {

        firstName : ko.observable(""),

        lastName : ko.observable(""),

        email : ko.observable(""),

        fashionBrand: ko.observable(model.dashboardModel.employee.fashionBrand),

        position: ko.observable(""),

        password: ko.observable(""),

        repassword : ko.observable(""),



    };




    model.noOfServices = ko.computed(function() {

        var count = 0;

        ko.utils.objectForEach(model.dashboardModel.employee.fashionBrand.services, function (key, value) {

            if(key != "id"){

                count = value() == true ? count + 1 : count + 0;
            }


        });

        return count;

    }, this);


    var tempModel = ko.mapping.fromJS(data);

    tempModel.dashboardModel.employee.repassword = ko.observable("");

    model.changePricing = function () {

        $('#pricingModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.pricing) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.pricing).replace("/","")){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "pricing has been successfully update");

                    //tempModel.employee.fashionBrand = ko.mapping.fromJS(data.fashionBrand);


                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeServices = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.services) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.services)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "services has been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeDetails = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "Fashion Brand detail have been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.addNewEmployee = function () {

        if(!model.validatePassword()){

            return;

        }

        toast("info", "Adding new Employee.");

        post(base_url + "designer/add/employee", ko.toJSON(model.newEmployee), function (data) {

            if(data.isSuccessful){

                toast("success", "Employee successfully added");

                model.newEmployee.firstName("");

                model.newEmployee.lastName("");

                model.newEmployee.email("");

                model.newEmployee.password("");

                model.newEmployee.repassword("");

            }else{

                toast("error", data.responseMessage)

            }

        });

    };

    model.updateFashionBrand = function (employee, callback) {

        console.log(ko.toJSON(employee));

        post(base_url + "designer/update/fashionbrand",ko.toJSON(employee), function (data) {

            callback(data);

        } );

    };

    model.validatePassword = function () {

        if(String(model.newEmployee.password().trim()) === String("")){

            toast("error", "Please enter a password");

            return false;

        }

        if(String(model.newEmployee.repassword().trim()) === String("")){

            toast("error", "Please retype the password");

            return false;

        }

        if(String(model.newEmployee.password()) != String(model.newEmployee.repassword())){

            toast("error", "inconsistent password");

            return false;

        }

        return true;
    };


    model.initDataTable = function () {

        $('#datatables').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "/designer/query/employees",
                "data": function ( data ) {
                    //process data before sent to server.
                }},
            "columns": [
                { "data": "id", "name" : "ID", "title" : "ID"  },
                { "data": "firstName", "name" : "First Name" , "title" : "First Name"},
                { "data": "lastName", "name" : "Last Name" , "title" : "Last Name"},
                { "data": "email", "name" : "Email" , "title" : "Email"},
                { "data": "position", "name" : "Position" , "title" : "Position"},


                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="detailBtn"  type="button" class="btn purple-gradient btn-sm" >' + 'Details' + '</button>';
                        return actionhtml;

                    }},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="deleteBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'view('+JSON.stringify(full)+', this )\' >' + 'Delete' + '</button>';
                        return actionhtml;

                    }},
            ]

        } );




        $('#datatables tbody').on('click', '#detailBtn', function (){
            var $row = $(this).closest('tr');
            var data =  $('#datatables').DataTable().row($row).data();

            console.log('data', JSON.stringify(data));
            //console.log('Record ID is', data['id']);

            post(base_url + "/designer/stats/employee/" + data['id'], 1, function (response) {

                model.employeeStats = ko.mapping.fromJS(response);

                if(model.employeeStats.responseModel.isSuccessful()){

                    $("#viewEmployeeDetailModalTitleHeader").text("Stats" + " " + "(" + response.fullName + ")" );

                    $("#playstats-fullname").text(response.fullName);

                    $("#playstats-totaltask").text(response.totalTasks);

                    $("#playstats-completedtask").text(response.completedTask);

                    $("#playstats-pendingtask").text(response.pendingTask);

                    $("#playstats-rating").empty();

                    switch (response.rating) {
                        case 0:
                            $("#playstats-rating").append("<span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span>");
                            break;
                        case 1:
                            $("#playstats-rating").append("<span class=\"fa fa-star checked\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span>");

                            break;
                        case 2:

                            $("#playstats-rating").append("<span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span>");

                            break;
                        case 3:

                            $("#playstats-rating").append("<span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span>");

                            break;
                        case 4:

                            $("#playstats-rating").append("<span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star\"></span>");

                            break;
                        case 5:

                            $("#playstats-rating").append("<span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span><span class=\"fa fa-star checked\"></span>");

                            break;

                    }


                    $('#viewEmployeeDetailModal').modal('show');

                }else{

                    toast("error", model.employeeStats.responseModel.responseMessage());

                }

                console.log(model.employeeStats);

                console.log(response);
            })

        });


        $('#datatables tbody').on('click', '#deleteBtn', function (){

            var $row = $(this).closest('tr');

            var data =  $('#datatables').DataTable().row($row).data();

            console.log('data', data);
            console.log('Record ID is', data['id']);

            // Add your code here

            $.confirm({
                type: 'purple',
                theme: 'material',
                backgroundDismiss: false,
                title: 'Delete!',
                content: 'Are you sure you want to delete' + " " + "(" + data['firstName'] + " " + data['lastName'] + ")",
                buttons: {
                    Yes: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function () {

                            toast("info", "Deleting.....");

                            post(base_url + "/admin/delete/weave", JSON.stringify(fibre), function (data) {

                                if(data.isSuccessful){

                                    var tr = $(element).parent().parent();

                                    $(tr[0]).remove();

                                    toast("success", fibre.name + " " + "has been deleted successfully");

                                }else{

                                    toast("error", "An error occured deleting" + " " + fibre.name);

                                }

                            })

                        },
                    },
                    No: {
                        text: 'No',
                        btnClass: 'btn-blue',
                        action: function(){

                        }
                    }
                }
            });

        });

    };


    console.log(model);

    return model;

};