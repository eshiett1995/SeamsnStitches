
function designerShirtShowcaseModel() {

    var model = {};

    model.shirtMeasurement = {

        topLength : ko.observable(0),

        chest : ko.observable(0),

        neck : ko.observable(0),

        shoulder : ko.observable(0),

        sleevelength : ko.observable(0),

        roundWrist : ko.observable(0),

        collar : ko.observable(0),

        shortSleeveLength : ko.observable(0),


    };


    var userRotation = new THREE.Vector3();
    var userZoom = new THREE.Vector3();
    var lastMousePosition;

    var group = new THREE.Group();

    var lerpyness = 0.1;

    var renderer; var scene; var camera;

    var maxTrans  = {x:0, y:0, z:0}; var minTrans  = {x:0, y:0, z:0}; var translate2Center = {x:0, y:0, z:0};


    model.init = function () {

        var s = JSON.stringify(parts);

        console.log(JSON.parse(s));

        if (typeof(Storage) !== "undefined") {

            if(localStorage.Back === undefined){localStorage.setItem("Back", "normal");}

            if(localStorage.BackPad === undefined){localStorage.setItem("BackPad", "normal");}

            if(localStorage.Collar === undefined){localStorage.setItem("Collar", "normal");}

            if(localStorage.Sleeve === undefined){localStorage.setItem("Sleeve", "long");}

            if(localStorage.Placket === undefined){localStorage.setItem("Placket", "normal");}

            if(localStorage.Cuff === undefined){localStorage.setItem("Cuff", "normal");}


            if(localStorage.shirtModelStructure === undefined ){

                localStorage.setItem("shirtModelStructure", JSON.stringify(parts));

                console.log(localStorage.getItem("shirtModelStructure"));

            }


        } else {

            alert("doesnt work");

        }

    };


    model.Orders = {

        fashionBrand : ko.observable(""),

        amount : ko.observable(0),

        clothType : ko.observable("COOPERATE_SHIRT"),

        status: ko.observable("PENDING"),

        clothModels : ko.observableArray([]),

    };

    model.increaseQuantity = function (clothModel) {

        clothModel.quantity(clothModel.quantity() + 1);


    },

        model.reduceQuantity = function (clothModel) {

            if(clothModel.quantity > 0) {

                clothModel.quantity(clothModel.quantity() - 1);

            }


        },

        model.ClothModel = {

            quantity : ko.observable(1),

            image : ko.observable(""),

            price : ko.observable(100),

            modelStructure : ko.observable(""),
        };

    model.clamp =function (val,min, max) {

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.scrollWheelClamp =function (val,min, max) {

        console.log(val);

        console.log(Math.max(val, min?min:0));

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.lerp = function(va,vb,vv){return va+((vb-va)*vv);};

    model.loadModel = function (){


        for (var property in JSON.parse(localStorage.shirtModelStructure)) {
            if (parts.hasOwnProperty(property)) {

                var option = JSON.parse(localStorage.shirtModelStructure)[property].selected;


                if(option != undefined) {

                    parts[property].group_type[option].forEach(function (item) {


                        var temp = {};

                        temp.x = item.coordinate.x;
                        temp.y = item.coordinate.y;
                        temp.z = item.coordinate.z;

                        if (temp.x > maxTrans.x) maxTrans.x = temp.x;
                        else if (temp.x < minTrans.x) minTrans.x = temp.x;
                        if (temp.y > maxTrans.y) maxTrans.y = temp.y;
                        else if (temp.y < minTrans.y) minTrans.y = temp.y;
                        if (temp.z > maxTrans.z) maxTrans.z = temp.z;
                        else if (temp.z < minTrans.z) minTrans.z = temp.z;

                    });

                }
            }

        }

        translate2Center.x = minTrans.x + (maxTrans.x-minTrans.x)/2;
        translate2Center.y = minTrans.y + (maxTrans.y-minTrans.y)/2;
        translate2Center.z = minTrans.z + (maxTrans.z-minTrans.z)/2;



        for (var property in JSON.parse(localStorage.shirtModelStructure)) {
            if (parts.hasOwnProperty(property)) {

                if(JSON.parse(localStorage.shirtModelStructure)[property].selected != undefined) {

                    var option = JSON.parse(localStorage.shirtModelStructure)[property].selected;

                    parts[property].group_type[option].forEach(function (item) {

                        var loader = new THREE.BufferGeometryLoader();

                        loader.load(
                            "../../" + item.model,

                            function (geometry) {

                                var uv = new THREE.TextureLoader().load('../../models/rainbow.jpg');

                                var material = new THREE.MeshLambertMaterial({map: uv});

                                material.side = THREE.DoubleSide;

                                var object = new THREE.Mesh(geometry, material);


                                object.position.x = item.coordinate.x - translate2Center.x + 0.2;
                                object.position.y = item.coordinate.y - translate2Center.y;
                                object.position.z = item.coordinate.z - translate2Center.z;
                                object.part = item.part;
                                object.type = item.type;
                                object.group = item.group;
                                object.group_role = item.group_role;


                                //object.position.x = item.coordinate.x;
                                //object.position.y = item.coordinate.y;
                                //object.position.z = item.coordinate.z;

                                group.add(object);
                            }
                        );

                    });
                }

            }


        }

        group.applyMatrix( new THREE.Matrix4().makeTranslation(
            translate2Center.x , translate2Center.y, translate2Center.z)
        );

        group.position.x = 0;
        group.position.y = 0.2;

// var box = new THREE.Box3().setFromObject( object );
        //box.center( object.position ); // this re-sets the mesh position
        //object.position.multiplyScalar( - 1 );



        scene = new THREE.Scene();

        var light = new THREE.AmbientLight( 0x404040,0.9 ); // soft white light
        scene.add( light );

        var spotLight = new THREE.SpotLight( 0xffffff );
        spotLight.position.set( 0.92, 0, -100 );


        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;


        var spotLight2 = new THREE.SpotLight( 0xffffff );
        spotLight2.position.set( 1, 20, 20.14);


        spotLight2.castShadow = true;

        spotLight2.intensity =0.95;

        spotLight2.shadow.mapSize.width = 1024;
        spotLight2.shadow.mapSize.height = 1024;

        spotLight2.shadow.camera.near = 500;
        spotLight2.shadow.camera.far = 4000;
        spotLight2.shadow.camera.fov = 30;



        scene.add( spotLight );

        scene.add( spotLight2);

        scene.add(group);



        camera = new THREE.PerspectiveCamera(30, window.innerWidth/window.innerHeight, 0.1,1000);




        renderer = new THREE.WebGLRenderer({ antialias: true });

        renderer.shadowMapEnabled = true;

        renderer.shadowMapSoft = true;

        // var controls = new THREE.OrbitControls( camera,renderer.domElement );

        // controls.update();

        renderer.setClearColor(0xffffff);

        renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(renderer.domElement);

        window.addEventListener('resize', function () {

            var width = window.innerWidth;
            var height = window.innerHeight;
            renderer.setSize(width, height);
            camera.aspect = width/height;
            camera.updateProjectionMatrix();

        });




        //camera.position.x = -0.43;

        //camera.position.z = 1.78;

        //camera.position.y = 0.51;

        camera.lookAt(group.position);

        userZoom.z = 1.8;
        camera.position.lerp(userZoom,lerpyness);

        //$('#x').val(camera.position.x);
        //$('#y').val(camera.position.y);
        //$('#z').val(camera.position.z);

        function addEventListeners(){

            var mouseDown = false;
            renderer.domElement.addEventListener('mousedown',function(evt){mouseDown=true; });

            renderer.domElement.addEventListener('mouseup',function(evt){mouseDown=false;});

            renderer.domElement.addEventListener('mousemove',function(evt){


                if(!lastMousePosition)lastMousePosition = new THREE.Vector2(evt.clientX,evt.clientY);
                if(mouseDown){
                    userRotation.y = model.clamp(userRotation.y+(evt.clientX-lastMousePosition.x)*0.01,-Math.PI*5,Math.PI*5);
                    userRotation.x= model.clamp(userRotation.x+(evt.clientY-lastMousePosition.y)*0.01,-Math.PI*0.5,Math.PI*0.5);
                }
                lastMousePosition.set(evt.clientX,evt.clientY);
            });

            window.addEventListener('mousewheel',function(evt){

                userZoom.z= model.scrollWheelClamp(userZoom.z+evt.wheelDelta*-0.01,1.8,15);
            });


            var obj = renderer.domElement;//document.getElementById('id');
            var lastTouch;
            obj.addEventListener('touchmove', function(event) {
                // If there's exactly one finger inside this element
                if (event.targetTouches.length == 1) {
                    var touch = event.targetTouches[0];
                    // Place element where the finger is
                    //obj.style.left = touch.pageX + 'px';
                    //obj.style.top = touch.pageY + 'px';
                    if(!lastTouch)lastTouch = new THREE.Vector2(touch.pageX,touch.pageY);

                    userRotation.y = model.clamp(userRotation.y+(touch.pageX-lastTouch.x)*0.01,-Math.PI*5,Math.PI*5);

                    userRotation.x = model.clamp(userRotation.x+(touch.pageY-lastTouch.y)*0.01,-Math.PI*0.5,Math.PI*0.5);

                    lastTouch.set(touch.pageX,touch.pageY);
                    event.preventDefault();
                }
            }, false);



        }

        var update = function () {

            // group.rotation.y += 0.001;

        };

        var render = function () {

            renderer.render(scene,camera);
        };

        var GameLoop = function () {

            camera.position.lerp(userZoom,lerpyness);

            requestAnimationFrame(GameLoop);

            update();

            if(group){
                group.rotation.x = model.lerp(group.rotation.x,userRotation.x,lerpyness);
                group.rotation.y = model.lerp(group.rotation.y,userRotation.y,lerpyness);
            }

            render();

        };


        GameLoop();

        addEventListeners();

        $(".fakeloader").fakeLoader({
            timeToHide:3000,
            bgColor:"#e74c3c",
            //spinner:"spinner2",

        });


    };

    model.resetRender = function () {

        renderer.setSize(window.innerWidth, window.innerHeight);

        camera.aspect = window.innerWidth/window.innerHeight;

        camera.updateProjectionMatrix();

        renderer.render( scene, camera);

    };

    model.takeScreenshot = function () {

        camera.aspect = 100 / 100;
        camera.updateProjectionMatrix();
        renderer.setSize(  100, 100 );

        renderer.render( scene, camera, null, false );

        var img = new Image();

        img.src = renderer.domElement.toDataURL();

        console.log(renderer.domElement.toDataURL());

        model.resetRender();

        return renderer.domElement.toDataURL();

    };


    model.addToCart = function () {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to add to cart ?',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-blue',
                    action: function(){

                        model.ClothModel.image(model.takeScreenshot());

                        model.ClothModel.modelStructure(JSON.parse(localStorage.shirtModelStructure));


                        if(localStorage.Cart === undefined){

                            var clothModelArray = [];

                            clothModelArray.push(ko.toJS(model.ClothModel));

                            localStorage.setItem("Cart", JSON.stringify(clothModelArray));

                        }else{

                            var clothModelArray = [];

                            clothModelArray = JSON.parse(localStorage.getItem("Cart"));

                            clothModelArray.push(ko.toJS(model.ClothModel));

                            localStorage.setItem("Cart", JSON.stringify(clothModelArray));
                        }

                        toast("success", "Shirt has been added to cart");

                        $.confirm({

                            title: 'Confirm',
                            type: 'purple',
                            //theme: 'black',
                            backgroundDismiss: false,
                            content: 'Whats the next step?',
                            buttons: {
                                makeNewOrder:  {
                                    text: 'Make new order',
                                    btnClass: 'btn-blue',
                                    keys: ['enter', 'shift'],


                                    action: function(){

                                        $.confirm({

                                            title: 'Previous design ?',
                                            type: 'purple',
                                            backgroundDismiss: false,
                                            content: 'Should we reset your last design to the default or do you want to edit it for a new design?',

                                            buttons: {

                                                reset: {
                                                    text: 'Reset',
                                                    btnClass: 'btn-blue',
                                                    keys: ['enter', 'shift'],
                                                    action: function(){

                                                    }
                                                },

                                                leaveIt: {
                                                    text: 'Leave it',
                                                    btnClass: 'btn-blue',
                                                    keys: ['enter', 'shift'],
                                                    action: function(){

                                                    }
                                                }
                                            },
                                            confirmButton: 'Reset to default',
                                            cancelButton: 'Leave it',

                                            confirm: function(){

                                                //todo do the reseting here
                                            },
                                            cancel: function(){


                                            }
                                        });
                                    }
                                },

                                checkout: {
                                    text: 'checkout',
                                    btnClass: 'btn-blue',
                                    keys: ['enter', 'shift'],
                                    action: function(){

                                        model.viewCart();

                                    }
                                }
                            }


                        });

                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-blue',
                    action: function(){

                    }
                }
            }
        });



    };

    model.resetClothModel = function () {



    };

    model.viewCart = function () {

        model.Order.clothModels([]);

        var clothModelArray = JSON.parse(localStorage.getItem("Cart"));

        clothModelArray.forEach(function (clothModel) {

            model.Order.clothModels.push(ko.mapping.fromJS(clothModel));

        });

        console.log(model.Order.clothModels());

        $('#cart-modal-ex').modal('show');

    };



    /************************************************************************************************************************/
    /***************************************MORPHING THE SHIRT MODEL FUNCTIONS***********************************************/
    /************************************************************************************************************************/


    model.showCustomerMeasurement = function () {


        $('#shirtMeasurementsModal').modal('show');


    };

    model.closeCustomerMeasurementModal = function () {

        $('#shirtMeasurementsModal').modal('hide');

    };


    return model;

};