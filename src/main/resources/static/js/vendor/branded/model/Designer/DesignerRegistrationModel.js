

function designerRegistrationModel(data) {

    var model = {

        brandName : ko.observable(''),

        image : ko.observable(''),

        website : ko.observable(''),

        email : ko.observable(''),

        phoneNumber : ko.observable(''),

        bankDetail : {

            bankName: ko.observable(''),

            bankCode: ko.observable(''),

            accountName : ko.observable(''),

            accountNumber : ko.observable(''),


        },

        brandPhrase : ko.observable(''),

        location : ko.observable(''),

        employees : ko.observableArray([]),


    };



    /**********************************************************************************************/
    /*************************************BANK FUNCTIONS*******************************************/
    /**********************************************************************************************/

    model.bankList = ko.observableArray([]);

    model.selectedBank = ko.observable('');

    model.selectedBank.subscribe(function (bank) {

        if(bank != undefined) {

            model.bankDetail.bankName(bank.bankName());

            model.bankDetail.bankCode(bank.bankCode());
        }
    });


    /**********************************************************************************************/
    /*************************************BANK FUNCTIONS END***************************************/
    /**********************************************************************************************/




    model.pricingList = ko.mapping.fromJS(data.pricingList);



    model.showGoogleMap = function () {

        $('#googleMapModal').modal('show');

    };



    model.getActiveServices = function () {

        var filteredArray =  ko.utils.arrayFilter(model.pricingList(), function(priceList) {

           return priceList.canDo() === true;

        });

        /****************************************************************************************************/
        /************Removes the gender enum problem KO has and puts a normal string as gender***************/
        /****************************************************************************************************/

        var filteredArrayAsJS =  ko.toJS(filteredArray);

        var newFilteredArrayInstance = ko.mapping.fromJS(filteredArrayAsJS);

        ko.utils.arrayForEach(newFilteredArrayInstance(), function (pricing) {

            pricing.clothingType.gender = pricing.clothingType.gender.$name();

        });

        return newFilteredArrayInstance();

    };

    model.employee = {

        firstName : ko.observable(''),

        lastName : ko.observable(''),

        email : ko.observable(''),

        password : ko.observable(''),

        re_password : ko.observable(''),


    };

    model.isRegistering = ko.observable(false);



    model.addEmployee = function () {

        model.validateFashionBrand(function (isValid,msg) {

            if(!isValid){

                toast("error",msg);

                return;

            }else {

                model.validateEmployee(function (isValid, msg) {

                    if(!isValid){

                        toast("error",msg);

                        return;

                    }else{

                        $('#addFounderModal').modal('hide');

                        model.employees.push(ko.toJS(model.employee));

                        console.log(model.employee.firstName());

                        model.employee.firstName('');

                        model.employee.lastName('');

                        model.employee.email('');

                        model.employee.password('');

                        model.employee.re_password('');

                        toast("success","Founder successfully added");

                    }
                })
            }

        });

    };

    model.updateEmployee = function () {

        model.validateFashionBrand(function (isValid,msg) {

            if(!isValid){

                toast("error",msg);

                return;

            }else {

                model.validateEmployee(function (isValid, msg) {

                    if(!isValid){

                        toast("error",msg);

                        return;

                    }else{

                        $('#editFounderModal').modal('hide');

                        model.employees.push(ko.toJS(model.employee));

                        console.log(model.employee.firstName());

                        model.employee.firstName('');

                        model.employee.lastName('');

                        model.employee.email('');

                        model.employee.password('');

                        model.employee.re_password('');

                        toast("success","Founder successfully updated");

                    }
                })
            }

        });

    };
    
    model.editEmployee = function (employee) {

        model.employee.firstName(employee.firstName);

        model.employee.lastName(employee.lastName);

        model.employee.email(employee.email);

        model.employee.password(employee.password);

        model.employee.re_password(employee.re_password);

        $('#editFounderModal').modal('show');
        
    };
    
    model.deleteEmployee = function (employee) {

        console.log(employee);

        model.employees.remove(employee);

        toast("success", "Founder has been removed successfully");

        /**model.employees.remove(function (employee) {

            return employee.email() === employee.email();
            
        });**/

    };


    model.validateFashionBrand = function (callback) {

        if(model.brandName() === ""){

            return callback(false, "Enter brand name");

        }

        if(model.email() === ""){

            return callback(false, "Input email address");

        }

        if(model.phoneNumber() === ""){

            return callback(false, "Enter a phone number");

        }

        if(model.location() === ""){

            return callback(false, "Enter a location");

        }


        return callback(true, "Success");


    };

    model.validateEmployee = function (callback) {

        if(model.employee.firstName() === ""){

            console.log(model.employee.firstName());

            return callback(false, "Enter first name");

        }

        if(model.employee.lastName() === ""){

            return callback(false, "Enter last name");

        }

        if(model.employee.email() === ""){

            return callback(false, "Input email address");

        }

        if(model.employee.password() === ""){

            return callback(false, "Enter a password");

        }

        if(model.employee.password() != model.employee.re_password()){

            return callback(false, "Password mismatch");

        }

        return callback(true, "Success");

    };
    
    model.removeCircularDependency = function ()
    {
        var fashionBrand = {

            brandName : model.brandName(),

            website : model.website(),

            email : model.email(),

            phoneNumber : model.phoneNumber(),

            brandPhrase : model.brandPhrase(),

            location : model.location(),

            pricing : ko.toJS(model.getActiveServices()),

            bankDetail : model.bankDetail,

            employees : model.employees(),


        };

        return fashionBrand;

    };

    model.fileUpload = function(data, e)
    {
        var _URL = window.URL || window.webkitURL;
        var file    = e.target.files[0];
        var reader  = new FileReader();
        img = new Image();

        img.onload = function () {

            if(Number(this.width) === Number(512) && Number(this.height) === Number(512)){

            }else{

                model.image('');

                toast("error", "image dimension must be 512x512");

            }

        };

        reader.onloadend = function (onloadend_e)
        {
            var result = reader.result; // Here is your base 64 encoded file. Do with it what you want.

            //model.image(result);

        };

        if(file)
        {
            reader.readAsDataURL(file);

            img.src = _URL.createObjectURL(file);

            model.image(file);
        }
    };

    model.register = function (item, event) {

        model.isRegistering(true);

        model.validateFashionBrand(function (isValid,msg) {

            if(!isValid){

                toast("error",msg);

                model.isRegistering(false);

                return;

            }else if((model.employees().length) === Number(0)) {

                toast("error","Please add a founder");

                model.isRegistering(false);

            }else{

                var data = new FormData();

                data.append( 'file', model.image());

                data.append('model',JSON.stringify(ko.toJS(model.removeCircularDependency())));

                $.ajax({

                    url: base_url + "designer/register/upload",
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: data,

                    success: function(response) {


                        model.isRegistering(false);

                        if(response.isSuccessful) {

                            $('#registrationSuccessModal').modal('show');

                            $('#registrationSuccessModalMessage').text(data.responseMessage);

                        }else {

                            toast("error",response.responseMessage);

                        }

                    },

                    error: function (response) {

                        model.isRegistering(false);

                        toast("error",response.responseMessage);

                    }



                });

            }

        });

    };

    model.initBankList = function () {

        getBanks(function (status, list) {

            if(status === 'success'){

                var tempBankArray = [];

                $('.mdb-select').material_select('destroy');

                list.forEach(function (bank) {

                    tempBankArray.push(ko.mapping.fromJS(bank))

                });

                model.bankList(tempBankArray);

                $('.mdb-select').material_select();

                console.log(model.bankList())

            }else {

                console.log("error while pulling down list")
            }
        })

    };

    return model;

}