

var registerModel = function(){


    var model = {

        firstName : ko.observable(''),

        lastName : ko.observable(''),

        phoneNumber : ko.observable(''),

        email : ko.observable(''),

        address : ko.observable(''),

        password : ko.observable(''),

        re_password : ko.observable(''),

        shirtMeasurement : {

            topLength : ko.observable(0),

            chest : ko.observable(0),

            neck : ko.observable(0),

            shoulder : ko.observable(0),

            sleevelength : ko.observable(0),

            roundWrist : ko.observable(0),

            collar : ko.observable(0),

            shortSleeveLength : ko.observable(0),


        },


        trouserMeasurement : {

            fullLength : ko.observable(0),

            waist : ko.observable(0),

            knee : ko.observable(0),

            hip : ko.observable(0),

            thigh : ko.observable(0),

            ankle : ko.observable(0),

            shortsLength : ko.observable(0),

        }

    };

    model.isRegistering = ko.observable(false);


    model.validateAllTabs = function (callback) {

        var valid = true;

        var panelList = ['panel1','panel2','panel3','panel4'];

        for(var index_panelList = 0; index_panelList < panelList.length ; index_panelList++){

            var panel = document.getElementById(panelList[index_panelList]);

            var formInputsArray = panel.querySelectorAll('input,select,textarea');

            for (var index = 0; index < formInputsArray.length; index++) {

                if(model.password() != model.re_password()){

                    model.showRegistrationFormValidationMessages("Password does not correlate");

                    valid = false;

                }

                if (formInputsArray[index].value.trim() == "" || !formInputsArray[index].checkValidity()) {

                    model.showRegistrationFormValidationMessages(formInputsArray[index].title);

                    valid = false;
                }

                if(!valid){

                    var selector = '#register-tab a[href="#' + panelList[index_panelList] + '"]';

                    $(selector).tab('show');

                    callback(valid);

                    return;

                }
            }
        }

        callback(valid);

    };


    model.register = function(){

        model.isRegistering(true);

        model.validateAllTabs(function (isvalid) {

            model.isRegistering(false);

            if(isvalid){

                post(base_url + "welcome/signup",ko.toJSON(model),function (data) {

                    if(data.isSuccessful){

                        $('#registrationSuccessModal').modal('show');

                        /**setInterval(function(){

                            window.location.replace(base_url + "log");


                        }, 3000); **/

                    }else{

                        toast("error",data.responseMessage);

                    }

                    model.isRegistering(false);

                });

            }

        });


    };

    /****************************************************************************/
    /***************************TAB SWITCH METHODS*******************************/
    /****************************************************************************/


    if($('#register-tab a[href="#panel1"]').hasClass("active")){

        $('#firstStepModal').modal('show');

    }

    model.showTab2 = function(){

        var valid = true;

        var panel = document.getElementById("panel1");

        var formInputsArray = panel.querySelectorAll('input,select,textarea');

        for (var index = formInputsArray.length - 1; index >= 0; index--) {

            if(model.password() != model.re_password()){

                model.showRegistrationFormValidationMessages("Password does not correlate");

                valid = false;

            }

            if (formInputsArray[index].value.trim() == "" || !formInputsArray[index].checkValidity()) {

                model.showRegistrationFormValidationMessages(formInputsArray[index].title);

                valid = false;
            }

        }

        if(valid){$('#register-tab a[href="#panel2"]').tab('show'); }

    };

    model.showTab3 = function () {

        var valid = true;

        var panel = document.getElementById("panel2");

        var formInputsArray = panel.querySelectorAll('input,select,textarea');

        for (var index = formInputsArray.length - 1; index >= 0; index--) {


            if (formInputsArray[index].value.trim() == "" || !formInputsArray[index].checkValidity()) {

                model.showRegistrationFormValidationMessages(formInputsArray[index].title);

                valid = false;
            }

        }

        if(valid){

            $('#shirtMeasurementModal').modal('show');

            $('#register-tab a[href="#panel3"]').tab('show');
        }


    };

    model.showLastTab = function () {


        var valid = true;

        var panel = document.getElementById("panel3");

        var formInputsArray = panel.querySelectorAll('input,select,textarea');

        for (var index = formInputsArray.length - 1; index >= 0; index--) {


            if (formInputsArray[index].value.trim() == "" || !formInputsArray[index].checkValidity()) {

                model.showRegistrationFormValidationMessages(formInputsArray[index].title);

                valid = false;
            }

        }

        if(valid){

            $('#trouserMeasurementModal').modal('show');

            $('#register-tab a[href="#panel4"]').tab('show');

        }
    };

    model.showRegistrationFormValidationMessages = function (message) {

        shake("#user-registration-form");

        toast("error", message);

    };



    /****************************************************************************/
    /***************************REDIRECTION METHODS******************************/
    /****************************************************************************/


    model.goToLoginPage = function() {

        window.location.replace(base_url + "login");

    };

    model.goHome = function() {

        window.location.replace(base_url + "home");

    };


    return model;

};