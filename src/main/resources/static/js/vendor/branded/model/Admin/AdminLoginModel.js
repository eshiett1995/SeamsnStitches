
var adminLoginModel = function(){

    var model = {

        email : ko.observable(''),

        password : ko.observable(''),

    };

    model.isLoginIn = ko.observable(false);

    model.validate = function (callback) {

        if(model.email() === ""){

            return callback(false,"Enter an email");

        }

        if(model.password() === ""){

            return callback(false,"Enter a password");

        }

        return callback(true,"validated!");

    };

    model.login = function(){

        model.isLoginIn(true);

        model.validate(function (isvalid, msg) {

            if(!isvalid){

                toast("error",msg);

                model.isLoginIn(false);

                return;


            }else{


                post(base_url + "admin/login",ko.toJSON(model),function (data) {

                    if(data.isSuccessful){

                        window.location.replace(base_url + "admin/dashboard");

                    }else{

                        model.isLoginIn(false);

                        toast("error",data.responseMessage);

                    }



                })

            }

        });




    };

    return model;

};