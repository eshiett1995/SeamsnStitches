
var fibreDashBoardModel = function(data){

    var model = {};

    model.name = ko.observable("");


    model.addNewFibre = function () {

        var fibreModel = {};

        fibreModel.name = model.name();

         post(base_url + "admin/save/fibre", ko.toJSON(fibreModel), function (data) {

            if(data.isSuccessful){

                toast("success","Fibre successfully saved");

            }else{

                toast("error","Oops!! an error has occurred");
            }

        });

    };

    model.logout = function () {


    };



    model.delete = function () {



    };



    model.initDataTable = function () {

        var table = $('#datatables').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "admin/query/fibre",
                "data": function ( data ) {
                    //process data before sent to server.

                }},
            "columns": [
                { "data": "id", "name" : "ID" , "title" : "ID"},
                { "data": "name", "name" : "Name" , "title" : "Name"},
                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {



                        var actionhtml='<button id="viewBtn" type="button" class="btn purple-gradient btn-sm" onclick=\'viewData('+JSON.stringify(full)+')\'>' + 'View' + '</button>';
                        return actionhtml;
                    }},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="viewBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'deleteData('+JSON.stringify(full)+', this )\' >' + 'Delete' + '</button>';
                        return actionhtml;

                    }},
            ]




        } );




      /**  var table = $('#datatables').DataTable();

        $('#datatables').on('click', '.btn', function(){
            // Get row data
            var data = table.row($(this).closest('tr')).data();

            alert('Name: ' +  data[0]);
        }); **/

    };



    return model;

};