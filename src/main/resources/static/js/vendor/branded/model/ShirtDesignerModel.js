
function shirtDesignerModel(data) {

    var model = {};





    /****************************************************************************************/
    /***************************FASHION BRAND FUNCTIONS AND VARIABLE*************************/
    /****************************************************************************************/

    model.clothingTypeId = ko.observable(1);

    model.latitude = ko.observable(0);

    model.longitude = ko.observable(0);

    model.distance = ko.observable(0);

    model.tempLatitude = ko.observable(6.444);

    model.tempLongitude = ko.observable(6.443);

    model.tempDistance = ko.observable(22);

    model.fashionBrandNameSearchKey = ko.observable("");

    model.tempFashionBrandNameSearchKey = ko.observable("");

    model.fashionBrandPage = {

        isFetching : {

            name: ko.observable(false),

            distance : ko.observable(false)
        },

        searchType : ko.observable("distance"), //distance, name

        pageable : ko.observable(ko.mapping.fromJS(data.fashionBrandPage))

    };
    
    model.showGoogleMap = function () {

        $('#googleMapModal').modal('show')

    };


    model.fashionbrand = ko.observable(ko.mapping.fromJS(data.fashionbrand));

    model.fashionbrand().website('');

    model.fashionbrand().brandName('');

    model.fashionbrand().location('');

    model.fashionbrand().rating(0);



    /****************************************************************************************/
    /***************************MATERIAL FUNCTIONS AND VARIABLE*************************/
    /****************************************************************************************/

    model.materialPage = {

        fashionBrand : ko.observable(),

        pageable : ko.observable(ko.mapping.fromJS(data.materialPage)),

    };


    /****************************************************************************************/
    /*****************FASHION BRAND AND MATERIALS PANELS TAB INIT TRIGGER METHODS************/
    /****************************************************************************************/

    $('a[data-toggle="fashion-brand-by-distance-tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab


        $("#search-fashion-brand-response-distance-msg").text("Fetching fashion brand............");

        model.fashionBrandPage.searchType("distance");

        model.fashionBrandPage.pageable().content([]);

        model.fashionBrandPage.isFetching.distance(true);

        var url = "generic/brand/paginated/" + model.clothingTypeId() + "/" + 0 + "/" + model.latitude() + "/" + model.longitude() + "/" + model.distance();

        get(base_url + url, function (response) {



            if(response.responseModel.isSuccessful){

                response.fashionBrandPage.content.forEach(function (item) {

                    model.fashionBrandPage.pageable().content.push(ko.mapping.fromJS(item));

                });

                //response.fashionBrandPage.content = model.fashionBrandPage.pageable().content();

                 //model.fashionBrandPage.pageable.content([]);

                model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                initToolTip();

            }else{

                toast("error",response.responseModel.responseMessage);

            }

            model.fashionBrandPage.isFetching.distance(false);


        });

    });


    $('a[data-toggle="fashion-brand-by-name-tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab

        $("#search-fashion-brand-response-name-msg").text("Fetching fashion brand............");

        model.fashionBrandPage.searchType("name");

        model.fashionBrandPage.pageable().content([]);

        model.fashionBrandPage.isFetching.name(true);

        post(base_url + '/generic/paginated/brand/' +  0, JSON.stringify({searchKey :  ""}), function (response) {


            if(response.responseModel.isSuccessful){


                $("#search-fashion-brand-response-name-msg").text(response.responseModel.responseMessage);

                model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                initToolTip();

            }else{

                toast("error",response.responseModel.responseMessage);

            }

            model.fashionBrandPage.isFetching.name(false);

        });

    });

    /****************************************************************************************/
    /*****************FASHION BRAND FETCH MATERIALS AND BRANDS API METHODS*******************/
    /****************************************************************************************/


    model.scrollingFashionBrandsList = function (data,event) {

        var element = event.target;

        // added the -100 so that the api can get loaded before the user reaches the end

        if ((element.scrollHeight - $(element).scrollTop()) - 100 <= $(element).height()){

            //checks if this is the last page of the scrollable page so that the method can be returned and resources don't get wasted
            if((model.fashionBrandPage.pageable().number() + 1) === model.fashionBrandPage.pageable().totalPages()){

                return;
            }

            // if the api is already fetching something, nothing should happen when the user scrolls to the button of the panel
            if(model.fashionBrandPage.isFetching.name() || model.fashionBrandPage.isFetching.distance()){

                return;

            }else{

                if(model.fashionBrandPage.searchType() === "distance"){

                    //sets the is fetching distance to true, so that it does get called multiple times when the scroll bar reaches the end of the panel
                    model.fashionBrandPage.isFetching.distance(true);

                    model.fetchFashionBrandByDistance(); //call to fetch the fashion brand by distance;

                }else if(model.fashionBrandPage.searchType() === "name"){

                    //sets the is fetching name to true, so that it does get called multiple times when the scroll bar reaches the end of the panel
                    model.fashionBrandPage.isFetching.name(true);

                    model.fetchFashionBrandByName(model.fashionBrandNameSearchKey())

                }else{

                    toast("error", "search param was'nt specified");

                }

            }

        }


    };

    model.initSearchForFashionBrandByDistance = function () {

        //this check if the latitude and longitude of search has been changed
        if(model.longitude() === model.tempLongitude() && model.latitude() === model.tempLatitude() && model.distance() === model.tempDistance()){

            return;

        }else{

            model.longitude(ko.mapping.toJSON(model.tempLongitude()));

            model.latitude(ko.mapping.toJSON(model.tempLatitude()));

            model.distance(ko.mapping.toJSON(model.tempDistance()));

            model.fashionBrandPage.isFetching.distance(true);

            var url = "generic/brand/paginated/" + model.clothingTypeId() + "/" + 0 + "/" + model.latitude() + "/" + model.longitude() + "/" + model.distance();

            get(base_url + url, function (response) {

                if(response.responseModel.isSuccessful){

                    $("#search-fashion-brand-response-distance-msg").text(response.responseModel.responseMessage);

                    model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                    initToolTip();


                }else{

                    toast("error",response.responseModel.responseMessage);

                }

                model.fashionBrandPage.isFetching.distance(false);

            });
        }

    };

    model.initSearchForFashionBrandByName = function () {

        //this check if the searchKey and temporary search key are the same (this is to disable multi search using the same parameter)
        if(model.searchKey() === model.tempSearchKey()){

            return;

        }else{

            model.fashionBrandPage.isFetching.name(true);

            post(base_url + '/generic/paginated/brand/' +  0, JSON.stringify({searchKey :  name}), function (response) {

                if(response.responseModel.isSuccessful){

                    $("#search-fashion-brand-response-distance-msg").text(response.responseModel.responseMessage);

                    model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                }else{

                    toast("error",response.responseModel.responseMessage);

                }

                model.fashionBrandPage.isFetching.name(false);

            });

        }

    };

    model.fetchFashionBrandByDistance = function () {

        var url = "generic/brand/paginated/" + model.clothingTypeId + "/" + model.fashionBrandPage.pageable().number() + 1 + "/" + model.latitude() + "/" + model.longitude() + "/" + model.distance();


        get(base_url + url, function (response) {

            if(response.responseModel.isSuccessful){

                response.fashionBrandPage.content.forEach(function (item) {

                    model.fashionBrandPage.pageable().content.push(ko.mapping.fromJS(item));

                });

                response.fashionBrandPage.content = model.fashionBrandPage.pageable().content();

                model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                initToolTip();

                console.log(model.fashionBrandPage.pageable());

            }else{

                toast("error",response.responseModel.responseMessage);

            }

            model.fashionBrandPage.isFetching.distance(false);

        });

    };

    model.fetchFashionBrandByName = function (name) {

        post(base_url + '/generic/paginated/brand/' +  (model.fashionBrandPage.pageable().number() + 1), JSON.stringify({searchKey :  name}), function (response) {

            if(response.responseModel.isSuccessful){

                response.fashionBrandPage.content.forEach(function (item) {

                    model.fashionBrandPage.pageable().content.push(ko.mapping.fromJS(item));

                });

                response.fashionBrandPage.content = model.fashionBrandPage.pageable().content();

                model.fashionBrandPage.pageable(ko.mapping.fromJS(response.fashionBrandPage));

                initToolTip();

                console.log(model.fashionBrandPage.pageable());

            }else{

                toast("error",response.responseModel.responseMessage);

            }

            model.fashionBrandPage.isFetching.name(false);

        })
    };

    model.getDistanceBetweenPoints = function (latitude1,longitude1, latitude2, longitude2,unit) {

       var calculatedDistance = distance(latitude1,longitude1,latitude2,longitude2,unit);

       var unitAbbreviation = unit === "K" ?  "Km" : "m";

       return (Math.round(calculatedDistance * 100) / 100)  + " " + unitAbbreviation;
    };

    model.showBrandDetail = function (fashionBrand, event) {

        event.stopPropagation();

        console.log(fashionBrand);

        model.fashionbrand(fashionBrand);

        $('#brandDetailModal').modal('show');

    };

    // Method called when a fashion brand has been selected
    model.fetchBrandsMaterial = function (fashionBrand, event) {

        model.addPriceOnFashionBrandChanged(fashionBrand);

        fashionBrand.pricing()[0].dateCreated(new Date(fashionBrand.pricing()[0].dateCreated()));

        event.stopPropagation();

        //------------ saves the fashion brand to the mesh----------------------//
        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        tempShirtModel.FashionBrand = ko.mapping.toJS(fashionBrand);

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));
        //------------------------------------------------------------------------//

        get(base_url + "generic/paginated/material/" + model.clothingTypeId() + "/" + fashionBrand.id() + "/" + 0, function (response) {


            if(response.responseModel.isSuccessful){

                if(response.materialPage.content.length < 1){

                    $("#search-fashion-brand-material-response-msg").text(response.responseModel.responseMessage);

                    toast("success", "Material has been successfully updated");

                }else{

                    model.materialPage.pageable(ko.mapping.fromJS(response.materialPage));

                    toast("success", response.responseModel.responseMessage);

                }


            }else{

                toast("error",response.responseModel.responseMessage);

            }

        })

    };



    /************************************************************************************************************************/
    /*********************************************UTILITY E-COMMERCE FUNCTIONS***********************************************/
    /************************************************************************************************************************/

    model.Orders = {

        fashionBrand : ko.observable(1),

        //fashionBrand : ko.observable(""),

        amount : ko.observable(0),

        clothType : ko.observable("COOPERATE_SHIRT"),

        status: ko.observable("PENDING"),

        clothModels : ko.observableArray([]),

    };

    model.Orders.fashionBrand(ko.mapping.fromJS(data.fashionbrand));

    model.ClothModel = {

        quantity : ko.observable(1),

        image : ko.observable(""),

        amount : ko.observable(0),

        modelStructure : ko.observable(""),
    };

    model.increaseQuantity = function (clothModel) {

        clothModel.quantity(clothModel.quantity() + 1);


    };

    model.reduceQuantity = function (clothModel) {

        if(clothModel.quantity > 0) {

            clothModel.quantity(clothModel.quantity() - 1);

        }


    };

    model.resetRender = function () {

        renderer.setSize(window.innerWidth, window.innerHeight);

        camera.aspect = window.innerWidth/window.innerHeight;

        camera.updateProjectionMatrix();

        renderer.render( scene, camera);

    };

    model.takeScreenshot = function () {

        camera.aspect = 100 / 100;
        camera.updateProjectionMatrix();
        renderer.setSize(  100, 100 );

        renderer.render( scene, camera, null, false );

        var img = new Image();

        img.src = renderer.domElement.toDataURL();

        model.resetRender();

        return renderer.domElement.toDataURL();

    };

    model.addToCart = function () {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to add to cart ?',
            buttons: {
                Yes: {
                    text: 'Yes',
                    btnClass: 'btn-blue',
                    action: function(){

                        model.ClothModel.image(model.takeScreenshot());

                        //model.ClothModel.modelStructure(JSON.parse(localStorage.shirtModelStructure));

                        model.ClothModel.modelStructure(localStorage.shirtModelStructure);


                        if(localStorage.Cart === undefined){

                            var clothModelArray = [];

                            clothModelArray.push(ko.toJS(model.ClothModel));

                            localStorage.setItem("Cart", JSON.stringify(clothModelArray));

                        }else{

                            var clothModelArray = [];

                            clothModelArray = JSON.parse(localStorage.getItem("Cart"));

                            clothModelArray.push(ko.toJS(model.ClothModel));

                            localStorage.setItem("Cart", JSON.stringify(clothModelArray));
                        }

                        toast("success", "Shirt has been added to cart");

                        $.confirm({

                            title: 'Confirm',
                            type: 'purple',
                            //theme: 'black',
                            backgroundDismiss: false,
                            content: 'Whats the next step?',
                            buttons: {
                                makeNewOrder:  {
                                    text: 'Make new order',
                                    btnClass: 'btn-blue',
                                    keys: ['enter', 'shift'],


                                    action: function(){

                                        $.confirm({

                                            title: 'Previous design ?',
                                            type: 'purple',
                                            backgroundDismiss: false,
                                            content: 'Should we reset your last design to the default or do you want to edit it for a new design?',

                                            buttons: {

                                                reset: {
                                                    text: 'Reset',
                                                    btnClass: 'btn-blue',
                                                    keys: ['enter', 'shift'],
                                                    action: function(){

                                                    }
                                                },

                                                leaveIt: {
                                                    text: 'Leave it',
                                                    btnClass: 'btn-blue',
                                                    keys: ['enter', 'shift'],
                                                    action: function(){

                                                    }
                                                }
                                            },
                                            confirmButton: 'Reset to default',
                                            cancelButton: 'Leave it',

                                            confirm: function(){

                                                //todo do the reseting here
                                            },
                                            cancel: function(){


                                            }
                                        });
                                    }
                                },

                                checkout: {
                                    text: 'checkout',
                                    btnClass: 'btn-blue',
                                    keys: ['enter', 'shift'],
                                    action: function(){

                                        model.viewCart();

                                    }
                                }
                            }


                        });

                    }
                },
                No: {
                    text: 'No',
                    btnClass: 'btn-blue',
                    action: function(){

                    }
                }
            }
        });



    };

    model.resetClothModel = function () {



    };

    model.viewCart = function () {

        model.Orders.clothModels([]);

        var clothModelArray = JSON.parse(localStorage.getItem("Cart"));

        console.log(JSON.parse(localStorage.getItem("Cart")));

        clothModelArray.forEach(function (clothModel) {

            model.Orders.clothModels.push(ko.mapping.fromJS(clothModel));

        });

        $('#cart-modal-ex').modal('show');

    };

    model.initTransaction =function () {

        console.log(JSON.parse(localStorage.shirtModelStructure));

        post(base_url + "transaction/save", ko.toJSON(model.Orders.clothModels()), function (response) {

            $('#cart-modal-ex').modal('hide');

            if(response.responseModel.isSuccessful){

                if(response.responseModel.responseMessage === "Please login"){

                    $('#loginModal').modal('show');

                }else {

                    /**swal(
                        'Transaction Successful!',
                        'You have successfully paid for the cloths',
                        'success'
                    )**/

                    payWithPayant();
                }

            }else{

                swal(
                    'Transaction Unsuccessful!',
                    'You have successfully paid for the cloths',
                    'error'
                )

            }



        })

    };

    model.payWithPaystack = function(data){


        var handler = PaystackPop.setup({

            key: 'pk_test_3a7af4a93d785ab7fb183ee27eeae4be3755340e',
            email: "eshiett1995@gmail.com",
            amount: 500000,
            metadata: {
                cartid: 20300,
                orderid: 2403480348,
                custom_fields: [
                    {
                        display_name: "Paid on",
                        variable_name: "paid_on",
                        value: 'Website'
                    },
                    {
                        display_name: "Paid via",
                        variable_name: "paid_via",
                        value: 'Inline Popup'
                    }
                ]
            },
            callback: function(response){
                // post to server to verify transaction before giving value
                var verifying = $.get( '/verify.php?reference=' + response.reference);
                verifying.done(function( data ) { /* give value saved in data */ });
            },
            onClose: function(){

                $('body').css("overflow","hidden");
            }
        });
        handler.openIframe();

    };



    /************************************************************************************************************************/
    /**************************************DYNAMIC PRICING E-COMMERCE FUNCTIONS***********************************************/
    /************************************************************************************************************************/

    model.addPriceOnFashionBrandChanged = function (fashionBrand) {

        var foundPrice = ko.utils.arrayFirst(fashionBrand.pricing(), function(prices) {

            return prices.clothingType.name() === parts.ClothType && prices.clothingType.gender() === parts.Gender;

        });

        model.ClothModel.amount(foundPrice.price());
    };

    model.addPriceOnMaterialChanged = function (material) {

        var yardsNeeded = 2;

        console.log(material);

        model.ClothModel.amount(model.ClothModel.amount() + (material.price() * yardsNeeded));

    };

    // this resets the price of the cloth model in certain situations (order has been purchased, model has been reset)
    model.resetPrice = function () {

        model.ClothModel.amount(0)

    };


    /************************************************************************************************************************/
    /*********************************************THREE.JS INITIALIZATION METHODS***********************************************/
    /************************************************************************************************************************/


    var userRotation = new THREE.Vector3();

    var userZoom = new THREE.Vector3();

    var lastMousePosition;

    var group = new THREE.Group();

    var lerpyness = 0.1;

    var renderer; var scene; var camera;

    var maxTrans  = {x:0, y:0, z:0}; var minTrans  = {x:0, y:0, z:0}; var translate2Center = {x:0, y:0, z:0};


    model.init = function () {

        var s = JSON.stringify(parts);

        if (typeof(Storage) !== "undefined") {

            if(localStorage.Back === undefined){localStorage.setItem("Back", "normal");}

            if(localStorage.BackPad === undefined){localStorage.setItem("BackPad", "normal");}

            if(localStorage.Collar === undefined){localStorage.setItem("Collar", "normal");}

            if(localStorage.Sleeve === undefined){localStorage.setItem("Sleeve", "long");}

            if(localStorage.Placket === undefined){localStorage.setItem("Placket", "normal");}

            if(localStorage.Cuff === undefined){localStorage.setItem("Cuff", "normal");}


            if(localStorage.shirtModelStructure === undefined ){

                localStorage.setItem("shirtModelStructure", JSON.stringify(parts));

            }


        } else {

            alert("Please update your browser, SnS cannot work on this version.");

        }

    };



    model.clamp =function (val,min, max) {

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.scrollWheelClamp =function (val,min, max) {

        return Math.min(Math.max(val, min?min:0), max?max:1);

    };

    model.lerp = function(va,vb,vv){return va+((vb-va)*vv);};

    model.loadModel = function (){


        for (var property in JSON.parse(localStorage.shirtModelStructure).Structure) {


            if (parts.Structure.hasOwnProperty(property)) {

                var option = JSON.parse(localStorage.shirtModelStructure).Structure[property].selected;

                if(option != undefined) {

                    parts.Structure[property].group_type[option].forEach(function (item) {


                        var temp = {};

                        temp.x = item.coordinate.x;
                        temp.y = item.coordinate.y;
                        temp.z = item.coordinate.z;

                        if (temp.x > maxTrans.x) maxTrans.x = temp.x;
                        else if (temp.x < minTrans.x) minTrans.x = temp.x;
                        if (temp.y > maxTrans.y) maxTrans.y = temp.y;
                        else if (temp.y < minTrans.y) minTrans.y = temp.y;
                        if (temp.z > maxTrans.z) maxTrans.z = temp.z;
                        else if (temp.z < minTrans.z) minTrans.z = temp.z;

                    });

                }
            }

        }

        translate2Center.x = minTrans.x + (maxTrans.x-minTrans.x)/2;
        translate2Center.y = minTrans.y + (maxTrans.y-minTrans.y)/2;
        translate2Center.z = minTrans.z + (maxTrans.z-minTrans.z)/2;



        for (var property in JSON.parse(localStorage.shirtModelStructure).Structure) {

            if (parts.Structure.hasOwnProperty(property)) {

                if(JSON.parse(localStorage.shirtModelStructure).Structure[property].selected != undefined) {

                    var option = JSON.parse(localStorage.shirtModelStructure).Structure[property].selected;

                    JSON.parse(localStorage.shirtModelStructure).Structure[property].group_type[option].forEach(function (item) {


                        // TODO this is so that the right n left pocket dont get loaded when the user only picked the left pocket
                        if(property === "Pocket" && JSON.parse(localStorage.shirtModelStructure).Structure[property].side === "L" ){

                            if(item.group_role === "right-pocket-thread" || item.group_role === "right-pocket"){

                                return;
                            }

                        }

                        var loader = new THREE.BufferGeometryLoader();

                        loader.load(
                            '../../'+item.model,

                            function (geometry) {

                                var uv = new THREE.TextureLoader().load("../../models/Material/shirt/DiffuseMap/" + "blue" + ".jpg");

                                var bump = new THREE.TextureLoader().load("../../models/Material/shirt/BumpMap/" + "bb" + ".jpg");

                                uv.anisotropy = renderer.getMaxAnisotropy();

                                var material;

                                if(item.type === "thread" || item.type === "flat-button-thread"){

                                    material = new THREE.MeshStandardMaterial({color:0x000000, metalness:0});

                                }else if(item.type === "front-left-tuxedo-pattern" || item.type === "front-right-tuxedo-pattern" || item.type === "front-left-white-tie-no-stud-pattern" || item.type === "front-right-white-tie-no-stud-pattern" || item.type === "front-right-white-tie-for-stud-pattern" || item.type === "front-left-white-tie-for-stud-pattern"){

                                    material = new THREE.MeshStandardMaterial({color:0xffffff, metalness: 0.0, bumpMap: bump, bumpScale : 0.002});


                                }else{

                                    material = new THREE.MeshStandardMaterial({map: uv, metalness: 0.0, roughness : 0.5, bumpMap: bump, bumpScale : 0.002});

                                }



                                material.side = THREE.DoubleSide;

                                var object = new THREE.Mesh(geometry, material);


                                object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                                object.position.y = item.coordinate.y - translate2Center.y;
                                object.position.z = item.coordinate.z - translate2Center.z;
                                object.part = item.part;
                                object.type = item.type;
                                object.group = item.group;
                                object.group_role = item.group_role;


                                //object.position.x = item.coordinate.x;
                                //object.position.y = item.coordinate.y;
                                //object.position.z = item.coordinate.z;

                                group.add(object);
                            }
                        );

                    });
                }

            }


        }


         group.applyMatrix( new THREE.Matrix4().makeTranslation(
            translate2Center.x , translate2Center.y, translate2Center.z)
        );

        group.position.x = 0;
        group.position.y = 0.2;

// var box = new THREE.Box3().setFromObject( object );
        //box.center( object.position ); // this re-sets the mesh position
        //object.position.multiplyScalar( - 1 );



        scene = new THREE.Scene();

        var light = new THREE.AmbientLight( 0x404040,0.9 ); // soft white light
        scene.add( light );

        var spotLight = new THREE.SpotLight( 0xffffff );
        spotLight.position.set( 0.92, 0, -100 );


        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 500;
        spotLight.shadow.camera.far = 4000;
        spotLight.shadow.camera.fov = 30;


        var spotLight2 = new THREE.SpotLight( 0xffffff );
        spotLight2.position.set( 1, 20, 20.14);


        spotLight2.castShadow = true;

        spotLight2.intensity =0.95;

        spotLight2.shadow.mapSize.width = 1024;
        spotLight2.shadow.mapSize.height = 1024;

        spotLight2.shadow.camera.near = 500;
        spotLight2.shadow.camera.far = 4000;
        spotLight2.shadow.camera.fov = 30;



        scene.add( spotLight );

        scene.add( spotLight2);

        scene.add(group);



        camera = new THREE.PerspectiveCamera(30, window.innerWidth/window.innerHeight, 0.1,1000);

        renderer = new THREE.WebGLRenderer({ antialias: true });

        renderer.setPixelRatio( window.devicePixelRatio );

        renderer.shadowMapEnabled = true;

        renderer.shadowMapSoft = true;

        // var controls = new THREE.OrbitControls( camera,renderer.domElement );

        // controls.update();

        renderer.setClearColor(0xffffff);

        renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(renderer.domElement);

        window.addEventListener('resize', function () {

            var width = window.innerWidth;
            var height = window.innerHeight;
            renderer.setSize(width, height);
            camera.aspect = width/height;
            camera.updateProjectionMatrix();

        });




        //camera.position.x = -0.43;

        //camera.position.z = 1.78;

        //camera.position.y = 0.51;

         camera.lookAt(group.position);

        userZoom.z = 1.8;
        camera.position.lerp(userZoom,lerpyness);

        //$('#x').val(camera.position.x);
        //$('#y').val(camera.position.y);
        //$('#z').val(camera.position.z);

        function addEventListeners(){

            var mouseDown = false;
            renderer.domElement.addEventListener('mousedown',function(evt){mouseDown=true; });

            renderer.domElement.addEventListener('mouseup',function(evt){mouseDown=false;});

            renderer.domElement.addEventListener('mousemove',function(evt){


                if(!lastMousePosition)lastMousePosition = new THREE.Vector2(evt.clientX,evt.clientY);
                if(mouseDown){
                    userRotation.y = model.clamp(userRotation.y+(evt.clientX-lastMousePosition.x)*0.01,-Math.PI*5,Math.PI*5);
                    userRotation.x= model.clamp(userRotation.x+(evt.clientY-lastMousePosition.y)*0.01,-Math.PI*0.5,Math.PI*0.5);
                }
                lastMousePosition.set(evt.clientX,evt.clientY);
            });

            window.addEventListener('mousewheel',function(evt){

                userZoom.z= model.scrollWheelClamp(userZoom.z+evt.wheelDelta*-0.01,1.8,15);
            });


            var obj = renderer.domElement;//document.getElementById('id');
            var lastTouch;
            obj.addEventListener('touchmove', function(event) {
                // If there's exactly one finger inside this element
                if (event.targetTouches.length == 1) {
                    var touch = event.targetTouches[0];
                    // Place element where the finger is
                    //obj.style.left = touch.pageX + 'px';
                    //obj.style.top = touch.pageY + 'px';
                    if(!lastTouch)lastTouch = new THREE.Vector2(touch.pageX,touch.pageY);

                    userRotation.y = model.clamp(userRotation.y+(touch.pageX-lastTouch.x)*0.01,-Math.PI*5,Math.PI*5);

                    userRotation.x = model.clamp(userRotation.x+(touch.pageY-lastTouch.y)*0.01,-Math.PI*0.5,Math.PI*0.5);

                    lastTouch.set(touch.pageX,touch.pageY);
                    event.preventDefault();
                }
            }, false);



        }

        var update = function () {

          // group.rotation.y += 0.001;

        };

        var render = function () {

            renderer.render(scene,camera);
        };

        var GameLoop = function () {

            camera.position.lerp(userZoom,lerpyness);

            requestAnimationFrame(GameLoop);

            update();

            if(group){
                group.rotation.x = model.lerp(group.rotation.x,userRotation.x,lerpyness);
                group.rotation.y = model.lerp(group.rotation.y,userRotation.y,lerpyness);
            }

            render();

        };


        GameLoop();

        addEventListeners();

        $(".fakeloader").fakeLoader({
            timeToHide:3000,
            bgColor:"#e74c3c",
            //spinner:"spinner2",

        });


    };









    /************************************************************************************************************************/
    /***************************************MORPHING THE SHIRT MODEL FUNCTIONS***********************************************/
    /************************************************************************************************************************/

    model.changeToShortSleeve = function () {

       if(JSON.parse(localStorage.shirtModelStructure).Structure["Sleeve"].selected === "short"){

            return;
       }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "sleeve" || group.children[i].part === "cuff" ) {

                group.remove(group.children[i]);

            }
        };

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Sleeve.selected;

        var diffuse;

        if(selectedItem != undefined){

             diffuse = tempShirtModel.Structure.Sleeve.group_type[selectedItem][0].diffuse;

        }else{

             diffuse = tempShirtModel.Structure.Sleeve.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.Sleeve.selected = "short";

        tempShirtModel.Structure.Cuff.selected = undefined;

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Sleeve",diffuse);

    };

    model.changeToLongSleeve = function () {

        if(JSON.parse(localStorage.shirtModelStructure).Structure["Sleeve"].selected === "long"){

            return;
        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "sleeve") {

                group.remove(group.children[i]);

            }

        }


        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Sleeve.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Sleeve.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Sleeve.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.Sleeve.selected = "long";

        tempShirtModel.Structure.Cuff.selected = "normal";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Sleeve",diffuse);

        model.addMesh("Cuff",diffuse);


    };

    model.changeTexture = function (material) {

       console.log(JSON.parse(localStorage.shirtModelStructure));


        model.addPriceOnMaterialChanged(material);

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part != "button") {

                group.children[i].material.map  = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + "rainbow" + '.jpg');//"../models/Material/shirt/DiffuseMap/" + material.imageURL() + ".jpg";

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        tempShirtModel.Material = ko.mapping.toJS(material);

        for (var property in tempShirtModel.Structure) {

            if (tempShirtModel.Structure.hasOwnProperty(property)) {

                var option = tempShirtModel.Structure[property].selected;



                if(tempShirtModel.Structure[property].group_type[option] != undefined) {

                    tempShirtModel.Structure[property].group_type[option].forEach(function (item) {


                        if (item.part != "button" || item.part != "thread") {

                            item.diffuse.has_diffuse = true;

                        }

                    })
                }
            }
        }

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        console.log(JSON.parse(localStorage.shirtModelStructure));

    };

    model.addLeftPocket = function () {

        if(JSON.parse(localStorage.shirtModelStructure).Structure["Pocket"].side === "L"){

           return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].group_role === "right-pocket-thread" || group.children[i].group_role === "right-pocket") {

                group.remove(group.children[i]);
            }

        }











        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Sleeve.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Sleeve.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Sleeve.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.Pocket.selected = "normal";

        if( tempShirtModel.Structure.Pocket.side === "R") {

            tempShirtModel.Structure.Pocket.side = "L&R";

        }else{ tempShirtModel.Structure.Pocket.side = "L"; }

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Pocket",diffuse);

    };

    model.addBothPocket = function () {

        if(JSON.parse(localStorage.shirtModelStructure).Structure["Pocket"].side === "L&R"){

            return;

        }

        //for (var i = group.children.length - 1; i >= 0; i--) {

        //    if (group.children[i].part === "sleeve") {

        //       group.remove(group.children[i]);

        //   }

        // }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Pocket.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Pocket.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Pocket.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.Pocket.selected = "normal";

        tempShirtModel.Structure.Pocket.side = "L&R";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Pocket",diffuse);

    };

    model.removeBothPocket = function () {



        if(JSON.parse(localStorage.shirtModelStructure).Structure["Pocket"].side === undefined){

            alert("is undefined");

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].group_role === "left-pocket" || group.children[i].group_role === "left-pocket-thread" || group.children[i].group_role === "right-pocket" || group.children[i].group_role === "right-pocket-thread") {

             group.remove(group.children[i]);

            }

         }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        tempShirtModel.Structure.Pocket.selected = undefined;

        tempShirtModel.Structure.Pocket.side = undefined;

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));


    };

    model.addClassicBottomCut = function () {

        if(JSON.parse(localStorage.shirtModelStructure)["Back"].selected.match(/classic/)){

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Back.selected;

        var diffuse = tempShirtModel.Back.group_type[selectedItem][0].diffuse;

        /************************************************/
        /************************************************/



        tempShirtModel.Back.selected = "classic_normal";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        for (var property in JSON.parse(localStorage.shirtModelStructure)) {

            if(property === "Back") {

                var groupType = JSON.parse(localStorage.shirtModelStructure)[property].selected;

                parts[property].group_type[groupType].forEach(function (item) {

                        var loader = new THREE.BufferGeometryLoader();

                        loader.load(
                            item.model,

                            function (geometry) {

                                var uv = new THREE.TextureLoader().load('../models/Material/shirt/DiffuseMap/' + diffuse + '.jpg');

                                var material = new THREE.MeshLambertMaterial({map: uv});

                                material.side = THREE.DoubleSide;

                                var object = new THREE.Mesh(geometry, material);


                                object.position.x = item.coordinate.x - translate2Center.x + 0.2;
                                object.position.y = item.coordinate.y - translate2Center.y;
                                object.position.z = item.coordinate.z - translate2Center.z;
                                object.part = item.part;
                                object.type = item.type;
                                object.group = item.group;
                                object.group_role = item.group_role;


                                group.add(object);
                            }
                        );


                });
            }
        }


    };

    model.addModernBottomCut = function () {

        if (JSON.parse(localStorage.shirtModelStructure)["Back"].selected.match(/modern/)) {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Back.selected;

        var diffuse = tempShirtModel.Back.group_type[selectedItem][0].diffuse;

        /************************************************/
        /************************************************/


        tempShirtModel.Back.selected = "modern_normal";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Back",diffuse);
    };

    model.addDesignEpaulettes = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["ShoulderStrap"].selected === "designer-epaulettes") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].group === "uniform-epaulettes-9.5cm" || group.children[i].group === "uniform-epaulettes-11.5cm" ) {

                group.remove(group.children[i]);

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.ShoulderStrap.selected;

        if(selectedItem != undefined){

            var diffuse = tempShirtModel.Structure.ShoulderStrap.group_type[selectedItem][0].diffuse;

        }else{

            var diffuse = tempShirtModel.Structure.ShoulderStrap.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.ShoulderStrap.selected = "designer-epaulettes";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("ShoulderStrap",diffuse);

    };

    model.addUniformEpaulettes_9point5cm = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["ShoulderStrap"].selected === "uniform-epaulettes-9.5cm") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].group === "designer-epaulettes" || group.children[i].group === "uniform-epaulettes-11.5cm") {

                group.remove(group.children[i]);

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.ShoulderStrap.selected;

        if(selectedItem != undefined){

            var diffuse = tempShirtModel.Structure.ShoulderStrap.group_type[selectedItem][0].diffuse;

        }else{

            var diffuse = tempShirtModel.Structure.ShoulderStrap.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.ShoulderStrap.selected = "uniform-epaulettes-9.5cm";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("ShoulderStrap",diffuse);

    };

    model.addUniformEpaulettes_11point5cm = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["ShoulderStrap"].selected === "uniform-epaulettes-11.5cm") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].group === "designer-epaulettes" || group.children[i].group === "uniform-epaulettes-9.5cm") {

                group.remove(group.children[i]);

            }

        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.ShoulderStrap.selected;

        if(selectedItem != undefined){

            var diffuse = tempShirtModel.Structure.ShoulderStrap.group_type[selectedItem][0].diffuse;

        }else{

            var diffuse = tempShirtModel.Structure.ShoulderStrap.diffuse;

        }

        /************************************************/
        /************************************************/


        tempShirtModel.Structure.ShoulderStrap.selected = "uniform-epaulettes-11.5cm";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("ShoulderStrap",diffuse);

    };

    model.noBackDetails = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "modern_normal" || JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "classic_normal") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }


        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Back.selected;

        var diffuse;

        if(selectedItem != undefined){

             diffuse = tempShirtModel.Structure.Back.group_type[selectedItem][0].diffuse;

        }else{

             diffuse = tempShirtModel.Structure.Back.diffuse;

        }

        /************************************************/
        /************************************************/


        if(tempShirtModel.Structure.Back.selected === "modern_center_fold" || tempShirtModel.Structure.Back.selected === "modern_back_darts" || tempShirtModel.Structure.Back.selected === "modern_side_fold"){

            tempShirtModel.Structure.Back.selected = "modern_normal";

        }else{

            tempShirtModel.Structure.Back.selected = "classic_normal";

        }



        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Back",diffuse);


    };

    model.addCenterFolds = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "modern_center_fold" || JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "classic_center_fold") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }


        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Back.selected;

        if(selectedItem != undefined){

            var diffuse = tempShirtModel.Structure.Back.group_type[selectedItem][0].diffuse;

        }else{

            var diffuse = tempShirtModel.Structure.Back.diffuse;

        }

        /************************************************/
        /************************************************/


        if(tempShirtModel.Structure.Back.selected === "modern_normal" || tempShirtModel.Structure.Back.selected === "modern_back_darts" || tempShirtModel.Structure.Back.selected === "modern_side_fold"){

            tempShirtModel.Structure.Back.selected = "modern_center_fold";

        }else{

            tempShirtModel.Structure.Back.selected = "classic_center_fold";

        }



        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Back",diffuse);


    };

    model.addSideFolds = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "modern_side_fold" || JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "classic_side_fold") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }


        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Back.selected;

        var diffuse;

        if(selectedItem != undefined){

             diffuse = tempShirtModel.Structure.Back.group_type[selectedItem][0].diffuse;

        }else{

             diffuse = tempShirtModel.Structure.Back.diffuse;

        }

        /************************************************/
        /************************************************/


        if(tempShirtModel.Structure.Back.selected === "modern_normal" || tempShirtModel.Structure.Back.selected === "modern_center_fold" || tempShirtModel.Structure.Back.selected === "modern_back_darts"){

            tempShirtModel.Structure.Back.selected = "modern_side_fold";

        }else{

            tempShirtModel.Structure.Back.selected = "classic_side_fold";

        }



        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Back",diffuse);


    };

    model.addBackDart = function () {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "modern_back_darts" || JSON.parse(localStorage.shirtModelStructure).Structure["Back"].selected === "classic_back_darts") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "back") {

                group.remove(group.children[i]);

            }

        }


        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Back.selected;

        if(selectedItem != undefined){

            var diffuse = tempShirtModel.Structure.Back.group_type[selectedItem][0].diffuse;

        }else{

            var diffuse = tempShirtModel.Structure.Back.diffuse;

        }

        /************************************************/
        /************************************************/


        if(tempShirtModel.Structure.Back.selected === "modern_normal" || tempShirtModel.Structure.Back.selected === "modern_center_fold" || tempShirtModel.Structure.Back.selected === "modern_side_fold"){

            tempShirtModel.Structure.Back.selected = "modern_back_darts";

        }else{

            tempShirtModel.Structure.Back.selected = "classic_back_darts";

        }



        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Back",diffuse);


    };

    model.addBusinessClassicCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "business_classic") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "business_classic";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addBusinessSuperiorCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "business_superior") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "business_superior";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addButtonDownClassicCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "button_down_classic") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "button_down_classic";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addButtonDownModernCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "button_down_modern") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem][0].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "button_down_modern";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addClubCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "club") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "club";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addClubModernCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "club_modern") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "club_modern";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addCutAwayClassicCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "cut_away_classic") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "cut_away_classic";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addCutAwayExtremeCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "cut_away_extreme") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "cut_away_extreme";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addCutAwayModernCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "cut_away_modern") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "cut_away_modern";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addCutAwaySuperiorCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "cut_away_superior") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "cut_away_superior";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addCutAwayTwoButtonCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "cut_away_two_button") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "cut_away_two_button";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addTurnDownSuperiorCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "turn_down_superior") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "turn_down_superior";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addWingCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "wing") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "wing";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addMaoCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "mao") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "mao";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.addPinCollar = function() {

        if (JSON.parse(localStorage.shirtModelStructure).Structure["Collar"].selected === "pin") {

            return;

        }

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.remove(group.children[i]);
            }
        }

        var tempShirtModel = JSON.parse(localStorage.shirtModelStructure);

        /************************************************/
        /** get appropriate texture for newly selected **/
        /************************************************/

        var selectedItem = tempShirtModel.Structure.Collar.selected;

        var diffuse;

        if(selectedItem != undefined){

            diffuse = tempShirtModel.Structure.Collar.group_type[selectedItem].diffuse;

        }else{

            diffuse = tempShirtModel.Structure.Collar.diffuse;

        }

        tempShirtModel.Structure.Collar.selected = "pin";

        localStorage.setItem("shirtModelStructure", JSON.stringify(tempShirtModel));

        model.addMesh("Collar",diffuse);


    };

    model.showMaterialDetails = function (material) {

        $("#material-name").text(material.name() + ", " + material.color());

         $("#material-property").text(material.weave.name() + ", " + material.fiberPercentage() + "%  " + material.fibre.name());

        $("#material-description").text(material.detail());

        $("#material-price").text("N" + material.price());

        $("#material-weight").text(material.weight());

        $("#material-yarn").text(material.yarn.warp() + "/" + material.yarn.weft());

        $("#material-weave").text(material.weave.name());



        $('#materialDetailModal').modal('show');

    };



    /************************************************************/
    /****************CLOTH CONTRAST METHOD***********************/
    /************************************************************/


    model.addCollarContrast= function (material) {

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar") {

                group.children[i].material.map = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + material.imageURL() + '.jpg');

            }
        }
    };

    model.addInnerCollarBandContrast= function (material) {

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar" && group.children[i].group_role === "inner-collar-band") {

                group.children[i].material.map = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + material.imageURL() + '.jpg');

            }
        }
    };

    model.addInnerCollarFlapContrast= function (material) {

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "collar" && group.children[i].group_role === "inner-collar-flap") {

                group.children[i].material.map = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + material.imageURL() + '.jpg');

            }
        }
    };

    model.addOuterPlacketBandContrast= function (material) {

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "placket" && group.children[i].group_role === "outer-placket-band") {

                group.children[i].material.map = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + material.imageURL() + '.jpg');

            }
        }
    };

    model.addInnerPlacketBandContrast= function (material) {

        for (var i = group.children.length - 1; i >= 0; i--) {

            if (group.children[i].part === "placket" && group.children[i].group_role === "inner-placket-band") {

                group.children[i].material.map = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + material.imageURL() + '.jpg');

            }
        }
    };

    
    /************************************************************/
    /****************GENERIC METHODS*****************************/
    /************************************************************/
     
    model.addMesh = function (part, imageName) {

        for (var property in JSON.parse(localStorage.shirtModelStructure).Structure) {

            if(property === part) {

                var groupType = JSON.parse(localStorage.shirtModelStructure).Structure[property].selected;

                parts.Structure[property].group_type[groupType].forEach(function (item) {



                    if(part === "Pocket" && JSON.parse(localStorage.shirtModelStructure).Structure[property].side === "L" ) {

                        if (item.group_role === "left-pocket" || item.group_role === "left-pocket-thread") {

                            var loader = new THREE.BufferGeometryLoader();

                            loader.load(
                                "../../" + item.model,

                                function (geometry) {

                                    var uv = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + "blue" + '.jpg');

                                    var material = new THREE.MeshLambertMaterial({map: uv});

                                    material.side = THREE.DoubleSide;

                                    var object = new THREE.Mesh(geometry, material);


                                    object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                                    object.position.y = item.coordinate.y - translate2Center.y;
                                    object.position.z = item.coordinate.z - translate2Center.z;
                                    object.part = item.part;
                                    object.type = item.type;
                                    object.group = item.group;
                                    object.group_role = item.group_role;


                                    group.add(object);
                                }
                            );
                        }


                    }else if(part === "Pocket" && JSON.parse(localStorage.shirtModelStructure).Structure[property].side === "L&R") {

                        if(item.group_role === "left-pocket" || item.group_role === "left-pocket-thread" || item.group_role === "right-pocket" || item.group_role === "right-pocket-thread"){

                            var loader = new THREE.BufferGeometryLoader();

                            loader.load(

                                "../../" + item.model,

                                function (geometry) {

                                    var uv = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + "blue" + '.jpg');

                                    var material = new THREE.MeshLambertMaterial({map: uv});

                                    material.side = THREE.DoubleSide;

                                    var object = new THREE.Mesh(geometry, material);


                                    object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                                    object.position.y = item.coordinate.y - translate2Center.y;
                                    object.position.z = item.coordinate.z - translate2Center.z;
                                    object.part = item.part;
                                    object.type = item.type;
                                    object.group = item.group;
                                    object.group_role = item.group_role;


                                    group.add(object);
                                }
                            );

                        }


                    }else{


                        var loader = new THREE.BufferGeometryLoader();

                        loader.load(
                            "../../" + item.model,

                            function (geometry) {

                                var uv = new THREE.TextureLoader().load('../../models/Material/shirt/DiffuseMap/' + "blue" + '.jpg');

                                var material = new THREE.MeshLambertMaterial({map: uv});

                                material.side = THREE.DoubleSide;

                                var object = new THREE.Mesh(geometry, material);


                                object.position.x = item.coordinate.x - translate2Center.x + 0.4;
                                object.position.y = item.coordinate.y - translate2Center.y;
                                object.position.z = item.coordinate.z - translate2Center.z;
                                object.part = item.part;
                                object.type = item.type;
                                object.group = item.group;
                                object.group_role = item.group_role;


                                group.add(object);
                            }
                        );

                    }

                });
            }
        }
        
    };



    /************************************************************/
    /*********LOGIN AND REGISTRATION METHODS*********************/
    /************************************************************/

    model.goToLoginPage = function () {

        window.location.href = "/login"
    };


    model.goToSignupPage = function () {

        window.location.href = "/signup"
    };



    /************************************************************/
    /****************FASHION BRANDS METHODS**********************/
    /************************************************************/




    return model;

};