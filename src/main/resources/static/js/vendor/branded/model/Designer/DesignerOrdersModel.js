
var designerOrdersModel = function(data){

    var mapping = {
        'ignore': ["order",]
    };

    var model = ko.mapping.fromJS(data, mapping);

    model.order = ko.observable(ko.mapping.fromJS(data.order));



    model.isMakingRequest = ko.observable(false);

    model.task = {

        employee : ko.observable(),

        details : ko.observable(""),

    };


    model.task.employee(model.employee);


    model.editedTask = {

        id : ko.observable(0),

        employee : ko.observable(),

        details : ko.observable(),

    };



    model.editTask = function (task) {

        model.editedTask.id(task.id());

        model.editedTask.employee(task.employee);

        model.editedTask.details(task.details());



    };

    model.editedTask.employee(model.employee);


    model.tasks = ko.observableArray([]);

    model.addTask = function () {

        if(model.task.details() === ""){

            toast("error", "Please enter the detail");

            return;

        }

        var jsTaskObject = ko.mapping.toJS(model.task);

        var KoTaskObject = ko.mapping.fromJS(jsTaskObject);


        /*************Adds an id to the new task to enable the server know its new****************/

        KoTaskObject.id = ko.observable(0);

        var foundTask = ko.utils.arrayFirst(model.tasks(), function(item) {


            return item.employee.lastName() === KoTaskObject.employee.lastName() && item.employee.firstName() === KoTaskObject.employee.firstName() || item.employee.email() === KoTaskObject.employee.email();

        });

        if(foundTask === undefined || foundTask === null){

            model.tasks.push(KoTaskObject);

            console.log(KoTaskObject);

            toast("success","Task added for" + " " + KoTaskObject.employee.firstName() + " " + KoTaskObject.employee.lastName())

        }else{

            toast("error", "Employee already has a task");

        }



    };


    model.updateTask = function () {

        var foundTask = ko.utils.arrayFirst(model.tasks(), function(item) {

            return item.employee.lastName() === model.editedTask.employee().lastName() && item.employee.firstName() === model.editedTask.employee().firstName() || item.employee.email() === model.editedTask.employee().email();

        });

        if(foundTask === undefined){


        }else if(model.editedTask.details().trim() === ""){

            toast("error", "Description cannot be empty");

        }else{

            var dummyTask = ko.mapping.toJS(foundTask);

            var updatedTask = ko.mapping.fromJS(dummyTask);

            updatedTask.details(model.editedTask.details());

            model.tasks.replace( foundTask, updatedTask );

            toast("success", "Task updated successfully");

        }

    };
    
    model.deleteTask = function () {

        var foundTask = ko.utils.arrayFirst(model.tasks(), function(item) {

            return item.employee.lastName() === model.editedTask.employee().lastName() && item.employee.firstName() === model.editedTask.employee().firstName() || item.employee.email() === model.editedTask.employee().email();

        });

        if(foundTask != undefined){

            model.tasks.remove( foundTask );

            toast("success", "deleted");

        }
        
    };


    model.dashboardModel.employee.repassword = ko.observable("");

    model.isNotAdmin = ko.computed(function () {

        return model.dashboardModel.employee.position.$name() != "ADMIN";

    });


    model.logout = function () {

        window.location.href = base_url + "designer/logout"

    };


    model.newEmployee = {

        firstName : ko.observable(""),

        lastName : ko.observable(""),

        email : ko.observable(""),

        fashionBrand: ko.observable(model.dashboardModel.employee.fashionBrand),

        position: ko.observable(""),

        password: ko.observable(""),

        repassword : ko.observable(""),



    };




    model.noOfServices = ko.computed(function() {

        var count = 0;

        ko.utils.objectForEach(model.dashboardModel.employee.fashionBrand.services, function (key, value) {

            if(key != "id"){

                count = value() == true ? count + 1 : count + 0;
            }


        });

        return count;

    }, this);


    var tempModel = ko.mapping.fromJS(data);

    tempModel.dashboardModel.employee.repassword = ko.observable("");

    model.changePricing = function () {

        $('#pricingModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.pricing) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.pricing).replace("/","")){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "pricing has been successfully update");

                    //tempModel.dashboardModel.employee.fashionBrand = ko.mapping.fromJS(data.fashionBrand);


                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeServices = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand.services) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand.services)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "services has been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.changeDetails = function () {

        $('#servicesModal').modal('hide');

        if(ko.toJSON(model.dashboardModel.employee.fashionBrand) != ko.toJSON(tempModel.dashboardModel.employee.fashionBrand)){

            toast("info", "saving update.");

            model.updateFashionBrand(model.dashboardModel.employee.fashionBrand, function (data) {

                if(data.isSuccessful){

                    toast("success", "Fashion Brand detail have been successfully update");

                }else{ toast("error", data.responseMessage);}

            });

        }


    };

    model.addNewEmployee = function () {

        if(!model.validatePassword()){

            return;

        }

        toast("info", "Adding new Employee.");

        post(base_url + "designer/add/employee", ko.toJSON(model.newEmployee), function (data) {

            if(data.isSuccessful){

                toast("success", "Employee successfully added");

                model.newEmployee.firstName("");

                model.newEmployee.lastName("");

                model.newEmployee.email("");

                model.newEmployee.password("");

                model.newEmployee.repassword("");

            }else{

                toast("error", data.responseMessage)

            }

        });

    };



    model.updateFashionBrand = function (employee, callback) {


        post(base_url + "designer/update/fashionbrand",ko.toJSON(employee), function (data) {

            callback(data);

        } );



    };

    model.validatePassword = function () {

        if(String(model.newEmployee.password().trim()) === String("")){

            toast("error", "Please enter a password");

            return false;

        }

        if(String(model.newEmployee.repassword().trim()) === String("")){

            toast("error", "Please retype the password");

            return false;

        }

        if(String(model.newEmployee.password()) != String(model.newEmployee.repassword())){

            toast("error", "inconsistent password");

            return false;

        }

        return true;
    };


    model.initDataTable = function () {

        $('#datatables').DataTable({

            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + "designer/query/orders",
                "data": function ( data ) {
                    //process data before sent to server.
                }},
            "columns": [
                { "data": "id", "name" : "ID", "title" : "ID"  },
                { "data": "dateCreated", "name" : "Time" , "title" : "Time"},
                { "data": "amount", "name" : "Amount" , "title" : "Amount"},
                { "data": "status", "name" : "Status" , "title" : "Status"},
                { "data": "clothStyle", "name" : "Style" , "title" : "Style"},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="deleteBtn"  type="button" class="btn purple-gradient btn-sm" >' + 'Details' + '</button>';
                        return actionhtml;

                    }},

                { "class":"",
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {

                        var actionhtml='<button id="viewBtn" data-bind="click: delete()" type="button" class="btn purple-gradient btn-sm" onclick= \'view('+JSON.stringify(full)+', this )\' >' + 'View' + '</button>';
                        return actionhtml;

                    }},
            ]

        } );




        $('#datatables tbody').on('click', '#deleteBtn', function (){
            var $row = $(this).closest('tr');
            var data =  $('#datatables').DataTable().row($row).data();


            get(base_url + "/designer/order/" + data['id'], function (response) {


                var newOrder = ko.mapping.fromJS(response);

                model.order(newOrder.order);


                model.tasks([]);

                newOrder.tasks().forEach(function(task) {

                    model.tasks.push(task);

                });


                $('#ordersDetailModal').modal('show');


            });



        });


        $('#datatables tbody').on('click', '#viewBtn', function (){

            var $row = $(this).closest('tr');

            var data =  $('#datatables').DataTable().row($row).data();

            console.log('data', data);
            console.log('Record ID is', data['id']);

            // Add your code here

            var win = window.open(base_url + "/designer/viewer/" + data['id'], '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }

        });

    };

    model.saveTaskOnDatabase = function () {

        model.isMakingRequest(true);

        /*********** convert enums to plain text ******************/

        model.tasks().forEach(function (t) {

            //console.log(ko.isObservable(t.employee.position));

            if(ko.isObservable(t.employee.position)){


            }else {

                var position = t.employee.position.$name();

                t.employee.position = ko.observable(position);


            }

        });


       post(Base_URL + "/designer/save/tasks/" + model.order().id(), ko.toJSON(model.tasks()), function (response) {

          if(response.isSuccessful){

              toast("success", "Successfully saved");

              $('#datatables').DataTable().ajax.reload();

          }else{

              toast("error", "Oops!, connection error");

          }

           model.isMakingRequest(false);

       })

    };


    return model;

};