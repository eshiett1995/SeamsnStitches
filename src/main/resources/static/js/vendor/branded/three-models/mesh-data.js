var parts = {

    ClothType : "COOPERATE SHIRT",

    Gender : "Male",

    FashionBrand: {},

    Material: {},

    Structure : {

        Back: {

            diffuse: "rainbow",

            selected: "modern_normal",

            group_type: {

                modern_normal: [

                    {
                        part: "back",
                        type: "normal",
                        group: "modern",
                        group_role: "normal-back",
                        sides: 1,
                        model: "models/shirt/back/back-modern-normal.json",
                        coordinate: {x: 0, y: 0, z: 0},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }

                ],

                modern_center_fold: [

                    {
                        part: "back",
                        type: "center-fold",
                        group: "modern",
                        group_role: "center-fold-back",
                        sides: 1,
                        model: "models/shirt/back/back-modern-center-fold.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }
                ],

                modern_back_darts: [

                    {
                        part: "back",
                        type: "back-darts",
                        group: "modern",
                        group_role: "back-darts-back",
                        sides: 1,
                        model: "models/shirt/back/back-modern-back-darts.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }
                ],

                modern_side_fold: [

                    {
                        part: "back",
                        type: "side-fold",
                        group: "modern",
                        group_role: "side-fold-back",
                        sides: 1,
                        model: "models/shirt/back/back-modern-side-fold.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }
                ],


                classic_normal: [ //todo this pricing will probably be larger knowing that the modern back is longer than the classic

                    {
                        part: "back",
                        type: "normal",
                        group: "normal",
                        group_role: "normal-back",
                        sides: 1,
                        model: "models/shirt/back/back-classic-normal.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }

                ],

                classic_center_fold: [

                    {
                        part: "back",
                        type: "center-fold",
                        group: "classic",
                        group_role: "center-fold-back",
                        sides: 1,
                        model: "models/shirt/back/back-classic-center-fold.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }

                ],

                classic_side_fold: [

                    {
                        part: "back",
                        type: "side-fold",
                        group: "classic",
                        group_role: "center-fold-back",
                        sides: 1,
                        model: "models/shirt/back/back-classic-side-fold.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }

                ],

                classic_back_darts: [

                    {
                        part: "back",
                        type: "back-darts",
                        group: "classic",
                        group_role: "back-darts-back",
                        sides: 1,
                        model: "models/shirt/back/back-classic-back-darts.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 12.5
                    }

                ]

            }

        },

        BackPad: {

            diffuse: "rainbow",

            selected: "normal",

            group_type: {

                "normal": [


                    {
                        part: "back-pad",
                        type: "normal",
                        group: "normal",
                        group_role: "normal",
                        sides: 1,
                        model: "models/shirt/back-pad.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}},
                        pricePercentage: 7.5
                    }

                ]

            }

        },

        Collar: {

            diffuse: "rainbow",

            selected: "mao",

            group_type: {

                business_classic: [

                    {
                        part: "collar",
                        type: "band",
                        group: "business_classic",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/business-classic/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "business_classic",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-classic/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "business_classic",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/business-classic/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "business_classic",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-classic/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_classic",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-classic/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_classic",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-classic/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "business_classic",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/business-classic/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_classic",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/business-classic/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                business_superior: [

                    {
                        part: "collar",
                        type: "band",
                        group: "business_superior",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/business-superior/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "business_superior",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-superior/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "business_superior",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/business-superior/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "business_superior",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-superior/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_superior",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-superior/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_superior",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/business-superior/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "business_superior",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/business-superior/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "business_superior",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/business-superior/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                button_down_classic: [

                    {
                        part: "collar",
                        type: "band",
                        group: "button_down_classic",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/button-down-classic/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "button_down_classic",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-classic/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "button_down_classic",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/button-down-classic/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "button_down_classic",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-classic/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_classic",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-classic/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_classic",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-classic/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_classic",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/button-down-classic/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_classic",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/button-down-classic/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_classic",
                        group_role: "right-collar-button",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/button-down-classic/right-collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_classic",
                        group_role: "right-collar-button-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/button-down-classic/right-collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_classic",
                        group_role: "left-collar-button",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/collars/button-down-classic/left-collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_classic",
                        group_role: "left-collar-button-thread",
                        sides: 1,
                        side: "left",
                        model: "models/shirt/collars/button-down-classic/left-collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                button_down_modern: [

                    {
                        part: "collar",
                        type: "band",
                        group: "button_down_modern",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/button-down-modern/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "button_down_modern",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-modern/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "button_down_modern",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/button-down-modern/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "button_down_modern",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-modern/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_modern",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-modern/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_modern",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/button-down-modern/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_modern",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/button-down-modern/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_modern",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/button-down-modern/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_modern",
                        group_role: "right-collar-button",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/button-down-modern/right-collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_modern",
                        group_role: "right-collar-button-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/button-down-modern/right-collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "button_down_modern",
                        group_role: "left-collar-button",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/collars/button-down-modern/left-collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "button_down_modern",
                        group_role: "left-collar-button-thread",
                        sides: 1,
                        side: "left",
                        model: "models/shirt/collars/button-down-modern/left-collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                club: [

                    {
                        part: "collar",
                        type: "band",
                        group: "club",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/club/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "club",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "club",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/club/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "club",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "club",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/club/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/club/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                club_modern: [

                    {
                        part: "collar",
                        type: "band",
                        group: "club-modern",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/club-modern/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "club-modern",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club-modern/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "club-modern",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/club-modern/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "club-modern",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club-modern/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club-modern",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club-modern/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club-modern",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/club-modern/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "club-modern",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/club-modern/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "club-modern",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/club-modern/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                cut_away_classic: [

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-classic",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-classic/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-classic",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-classic/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-classic",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-classic/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-classic",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-classic/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-classic",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-classic/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-classic",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-classic/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-classic",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-classic/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-classic",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-classic/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                cut_away_superior: [

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-superior",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-superior/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-superior",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-superior/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-superior",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-superior/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-superior",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-superior/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-superior",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-superior/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-superior",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-superior/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-superior",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-superior/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-superior",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-superior/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                cut_away_extreme: [

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-extreme",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-extreme/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-extreme",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-extreme/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-extreme",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-extreme/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-extreme",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-extreme/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-extreme",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-extreme/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-extreme",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-extreme/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-extreme",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-extreme/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-extreme",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-extreme/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                cut_away_modern: [

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-modern",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-modern/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-modern",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-modern/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-modern",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-modern/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-modern",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-modern/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-modern",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-modern/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-modern",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-modern/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-modern",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-modern/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-modern",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/cut-away-modern/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                cut_away_two_button: [

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-two-button",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-two-button/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "cut-away-two-button",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-two-button/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-two-button",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/cut-away-two-button/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "cut-away-two-button",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-two-button/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-two-button",
                        group_role: "collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/cut-away-two-button/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-two-button",
                        group_role: "collar-top-button",
                        sides: 2,
                        side: "top",
                        model: "models/shirt/collars/cut-away-two-button/collar-top-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-two-button",
                        group_role: "collar-top-button-thread",
                        sides: 2,
                        side: "top",
                        model: "models/shirt/collars/cut-away-two-button/collar-top-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "cut-away-two-button",
                        group_role: "collar-bottom-button",
                        sides: 2,
                        side: "bottom",
                        model: "models/shirt/collars/cut-away-two-button/collar-bottom-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "cut-away-two-button",
                        group_role: "collar-bottom-button-thread",
                        sides: 2,
                        side: "bottom",
                        model: "models/shirt/collars/cut-away-two-button/collar-bottom-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                turn_down_superior: [

                    {
                        part: "collar",
                        type: "band",
                        group: "turn_down_superior",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/turn-down-superior/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "turn_down_superior",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/turn-down-superior/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "turn_down_superior",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/turn-down-superior/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "turn_down_superior",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/turn-down-superior/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "turn_down_superior",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/turn-down-superior/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "turn_down_superior",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/turn-down-superior/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "turn_down_superior",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/turn-down-superior/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "turn_down_superior",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/turn-down-superior/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                mao: [

                    {
                        part: "collar",
                        type: "band",
                        group: "mao",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/mao/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "mao",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/mao/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "mao",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/mao/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "mao",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/mao/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "mao",
                        group_role: "collar-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/mao/collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                wing: [

                    {
                        part: "collar",
                        type: "band",
                        group: "wing",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/wing/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "wing",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/wing/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "wing",
                        group_role: "inner-right-collar-flap",
                        sides: 2,
                        side: "right-inner",
                        model: "models/shirt/collars/wing/right-inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "wing",
                        group_role: "outer-right-collar-flap",
                        sides: 2,
                        side: "right-outer",
                        model: "models/shirt/collars/wing/right-outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "wing",
                        group_role: "inner-left-collar-flap",
                        sides: 2,
                        side: "left-inner",
                        model: "models/shirt/collars/wing/left-inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "wing",
                        group_role: "outer-left-collar-flap",
                        sides: 2,
                        side: "left-outer",
                        model: "models/shirt/collars/wing/left-outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "wing",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/wing/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "wing",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/wing/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                pin: [

                    {
                        part: "collar",
                        type: "band",
                        group: "pin",
                        group_role: "inner-collar-band",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/pin/inner-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "band",
                        group: "pin",
                        group_role: "outer-collar-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/pin/outer-collar-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "pin",
                        group_role: "inner-collar-flap",
                        sides: 2,
                        side: "inner",
                        model: "models/shirt/collars/pin/inner-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "flap",
                        group: "pin",
                        group_role: "outer-collar-flap",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/pin/outer-collar-flap.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "pin",
                        group_role: "left-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/pin/left-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "pin",
                        group_role: "right-collar-thread",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/collars/pin/right-collar-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "button",
                        group: "pin",
                        group_role: "collar-button",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/pin/collar-button.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "thread",
                        group: "pin",
                        group_role: "collar-button-thread",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/pin/collar-button-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    {
                        part: "collar",
                        type: "pin-head",
                        group: "pin",
                        group_role: "right-pin-head",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/pin/right-pin-head.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "pin-head",
                        group: "pin",
                        group_role: "left-pin-head",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/collars/pin/left-pin-head.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "pin-holder",
                        group: "pin",
                        group_role: "right-pin-holder",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/collars/pin/right-pin-holder.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "pin-holder",
                        group: "pin",
                        group_role: "left-pin-holder",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/collars/pin/left-pin-holder.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "collar",
                        type: "pin-shaft",
                        group: "pin",
                        group_role: "pin-shaft",
                        sides: 1,
                        side: "null",
                        model: "models/shirt/collars/pin/pin-shaft.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ]

            }
        },

        Sleeve: {

            diffuse: "rainbow",

            selected: "long",

            group_type: {

                "long": [

                    {
                        part: "sleeve",
                        type: "long",
                        group: "long",
                        group_role: "right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/sleeve-right.json",
                        coordinate: {x: 0.443, y: 0.32, z: -0.52},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "sleeve",
                        type: "long",
                        group: "long",
                        group_role: "left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/sleeve-left.json",
                        coordinate: {x: 0.878, y: 0.5529, z: -1.0067},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                /**"long": [

                    {
                        part: "sleeve",
                        type: "short",
                        group: "short",
                        group_role: "right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/short-sleeve-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255,},
                        diffuse: "rainbow"
                    },

                    {
                        part: "sleeve",
                        type: "short",
                        group: "short",
                        group_role: "left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/short-sleeve-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255,},
                        diffuse: "rainbow"
                    },

            ], **/

                    "short": [

                    {
                        part: "sleeve",
                        type: "short",
                        group: "short",
                        group_role: "right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/short-sleeve-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "sleeve",
                        type: "short",
                        group: "short",
                        group_role: "left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/short-sleeve-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ]
            }

        },

        Placket: {

            diffuse: "rainbow",

            selected: "white_tie_placket_for_studs",

            group_type: {

                normal_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "normal-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/normal-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "normal-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/normal-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    //PLACKETS

                    {
                        part: "placket",
                        type: "placket-band",
                        group: "normal-placket",
                        group_role: "outer-placket-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/plackets/normal-placket/front-outer-placket-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "placket-band",
                        group: "normal-placket",
                        group_role: "inner-placket-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/plackets/normal-placket/front-inner-placket-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "normal-placket",
                        group_role: "left-white-tie-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/normal-placket/left-normal-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "normal-placket",
                        group_role: "right-white-tie-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/normal-placket/right-normal-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //BUTTONS

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-1",
                        sides: 7,
                        side: 1,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-2",
                        sides: 7,
                        side: 2,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-3",
                        sides: 7,
                        side: 3,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-4",
                        sides: 7,
                        side: 4,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-5",
                        sides: 7,
                        side: 5,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-6",
                        sides: 7,
                        side: 6,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "normal-placket",
                        group_role: "flat-button-7",
                        sides: 7,
                        side: 7,
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    //{ part: "thread", type: "flat-button-thread", group: "normal", group_role: "flat-button-thread-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-thread-1.json", coordinate: {x : -0.8507, y :0.0307, z : -1.249,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "normal-placket",
                        group_role: "flat-button-thread-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/normal-placket/buttons/flat-button/flat-button-thread-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                narrow_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "narrow-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/narrow-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "narrow-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/narrow-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    //PLACKETS

                    {
                        part: "placket",
                        type: "placket-band",
                        group: "narrow-placket",
                        group_role: "outer-placket-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/plackets/narrow-placket/front-outer-placket-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: "rainbow"

                    },

                    {
                        part: "placket",
                        type: "placket-band",
                        group: "narrow-placket",
                        group_role: "inner-placket-band",
                        sides: 2,
                        side: "outer",
                        model: "models/shirt/plackets/narrow-placket/front-inner-placket-band.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "narrow-placket",
                        group_role: "left-white-tie-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/narrow-placket/left-narrow-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "narrow-placket",
                        group_role: "right-white-tie-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/narrow-placket/right-narrow-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //BUTTONS

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-1",
                        sides: 7,
                        side: 1,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-2",
                        sides: 7,
                        side: 2,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-3",
                        sides: 7,
                        side: 3,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-4",
                        sides: 7,
                        side: 4,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-5",
                        sides: 7,
                        side: 5,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-6",
                        sides: 7,
                        side: 6,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "narrow-placket",
                        group_role: "flat-button-7",
                        sides: 7,
                        side: 7,
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    //{ part: "thread", type: "flat-button-thread", group: "normal", group_role: "flat-button-thread-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-thread-1.json", coordinate: {x : -0.8507, y :0.0307, z : -1.249,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "narrow-placket",
                        group_role: "flat-button-thread-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/narrow-placket/buttons/flat-button/flat-button-thread-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                hidden_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "hidden-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/hidden-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "hidden-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/hidden-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "hidden-placket",
                        group_role: "left-no-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/hidden-placket/left-hidden-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "without-placket",
                        group_role: "right-no-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/hidden-placket/right-hidden-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                without_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "without-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/without-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "without-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/without-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "without-placket",
                        group_role: "left-no-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/without-placket/left-no-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "without-placket",
                        group_role: "right-no-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/without-placket/right-no-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //BUTTONS

                    //{ part: "button", type:"flat-button", group: "normal", group_role: "flat-button-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-1.json", coordinate: {x : -0.66, y : 0.02, z : -1.27,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "without-placket",
                        group_role: "flat-button-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //{ part: "thread", type: "flat-button-thread", group: "normal", group_role: "flat-button-thread-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-thread-1.json", coordinate: {x : -0.8507, y :0.0307, z : -1.249,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "without-placket",
                        group_role: "flat-button-thread-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/without-placket/buttons/flat-button/flat-button-thread-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                tuxedo_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "tuxedo-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/tuxedo-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "tuxedo-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/tuxedo-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-left-tuxedo-pattern",
                        group: "tuxedo-placket",
                        group_role: "front-left-tuxedo-pattern",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/tuxedo-placket/front-left-tuxedo-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right-tuxedo-pattern",
                        group: "tuxedo-placket",
                        group_role: "front-right-tuxedo-pattern",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/tuxedo-placket/front-right-tuxedo-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "tuxedo-placket",
                        group_role: "left-tuxedo-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/tuxedo-placket/left-tuxedo-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "tuxedo-placket",
                        group_role: "right-tuxedo-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/tuxedo-placket/right-tuxedo-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ],

                white_tie_placket: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "white-tie-placket",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "white-tie-placket",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-left-white-tie-no-stud-pattern",
                        group: "white-tie-placket",
                        group_role: "front-left-white-tie-no-stud-pattern",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket/front-left-white-tie-no-stud-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right-white-tie-no-stud-pattern",
                        group: "white-tie-placket",
                        group_role: "front-right-white-tie-no-stud-pattern",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket/front-right-white-tie-no-stud-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "white-tie-placket",
                        group_role: "left-white-tie-placket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket/left-white-tie-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "white-tie-placket",
                        group_role: "right-white-tie-placket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket/right-white-tie-placket-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //BUTTONS

                    //{ part: "button", type:"flat-button", group: "normal", group_role: "flat-button-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-1.json", coordinate: {x : -0.66, y : 0.02, z : -1.27,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket",
                        group_role: "flat-button-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //{ part: "thread", type: "flat-button-thread", group: "normal", group_role: "flat-button-thread-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-thread-1.json", coordinate: {x : -0.8507, y :0.0307, z : -1.249,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: "rainbow",
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    ////////////////////////////////////////////////////////////////

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket/buttons/flat-button/flat-button-thread-hole-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ],

                // pivot point has been changed
                white_tie_placket_for_studs: [

                    {
                        part: "placket",
                        type: "front-left",
                        group: "white-tie-placket-for-studs",
                        group_role: "front-left",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/front-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-right",
                        group: "white-tie-placket-for-studs",
                        group_role: "front-right",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/front-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "front-left-white-tie-for-stud-pattern",
                        group: "white-tie-placket-for-studs",
                        group_role: "front-left-white-tie-for-stud-pattern",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/front-left-white-tie-for-stud-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "front-right-white-tie-for-stud-pattern",
                        group: "white-tie-placket-for-studs",
                        group_role: "front-right-white-tie-for-stud-pattern",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/front-right-white-tie-for-stud-pattern.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "left-white-tie-placket-for-stud-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/left-white-tie-placket-for-stud-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "right-white-tie-placket-for-stud-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/right-white-tie-placket-for-stud-thread.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //BUTTONS

                    //{ part: "button", type:"flat-button", group: "normal", group_role: "flat-button-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-1.json", coordinate: {x : -0.66, y : 0.02, z : -1.27,}, diffuse: "rainbow"},

                    /**{
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255,},
                        diffuse: "rainbow"
                    },**/

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },


                    //{ part: "thread", type: "flat-button-thread", group: "normal", group_role: "flat-button-thread-1", sides:7, side:"1", model: "models/shirt/buttons/flat/front-button-thread-1.json", coordinate: {x : -0.8507, y :0.0307, z : -1.249,}, diffuse: "rainbow"},

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}

                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    ////////////////////////////////////////////////////////////////

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-1",
                        sides: 7,
                        side: "1",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-1.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-2",
                        sides: 7,
                        side: "2",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-2.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-3",
                        sides: 7,
                        side: "3",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-3.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-4",
                        sides: 7,
                        side: "4",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-4.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-5",
                        sides: 7,
                        side: "5",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-5.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket-for-studs",
                        group_role: "flat-button-thread-hole-6",
                        sides: 7,
                        side: "6",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-6.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "placket",
                        type: "flat-button-thread-hole",
                        group: "white-tie-placket",
                        group_role: "flat-button-thread-hole-7",
                        sides: 7,
                        side: "7",
                        model: "models/shirt/plackets/white-tie-placket-for-studs/buttons/flat-button/flat-button-thread-hole-7.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }

                ]

            }

        },

        Cuff: {

            diffuse: "rainbow",

            selected: "normal",

            group_type: {

                "normal": [


                    {
                        part: "cuff",
                        type: "normal",
                        group: "normal",
                        group_role: "inner-right-cuff",
                        sides: 2,
                        side: "inner-right",
                        model: "models/shirt/inner-right-cuff.json",
                        coordinate: {x: -0.37, y: 0.27, z: -1.277},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                    },

                    {
                        part: "cuff",
                        type: "normal",
                        group: "normal",
                        group_role: "outer-right-cuff",
                        sides: 2,
                        side: "outer-right",
                        model: "models/shirt/outer-right-cuff.json",
                        coordinate: {x: 0.154, y: 0.394, z: -1.132},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                    },

                    {
                        part: "cuff",
                        type: "normal",
                        group: "normal",
                        group_role: "inner-left-cuff",
                        sides: 2,
                        side: "inner-left",
                        model: "models/shirt/inner-left-cuff.json",
                        coordinate: {x: -0.287, y: 0.432, z: -2.3105},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                    },

                    {
                        part: "cuff",
                        type: "normal",
                        group: "normal",
                        group_role: "outer-left-cuff",
                        sides: 2,
                        side: "outer-left",
                        model: "models/shirt/outer-left-cuff.json",
                        coordinate: {x: -0.839, y: 0.287, z: -0.881},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                    }


                ]

            }

        },

        Pocket: {

            diffuse: "rainbow",

            selected: undefined,

            side: undefined,

            group_type: {

                "normal": [

                    {
                        part: "pocket",
                        type: "normal",
                        group: "normal",
                        group_role: "right-pocket",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/pocket/normal/normal-right-pocket.json",
                        coordinate: {x: -0.53, y: 0.04, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "pocket",
                        type: "normal",
                        group: "normal",
                        group_role: "left-pocket",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/pocket/normal/normal-left-pocket.json",
                        coordinate: {x: -0.18, y: 0.198, z: -1.13},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "normal",
                        group: "normal",
                        group_role: "right-pocket-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/pocket/normal/normal-right-pocket-thread.json",
                        coordinate: {x: -0.53, y: 0.04, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "normal",
                        group: "normal",
                        group_role: "left-pocket-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/pocket/normal/normal-left-pocket-thread.json",
                        coordinate: {x: -0.18, y: 0.198, z: -1.13},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ]

            }

        },

        ShoulderStrap: {

            diffuse: "rainbow",

            selected: undefined,

            side: undefined,

            group_type: {

                "designer-epaulettes": [

                    {
                        part: "strap",
                        type: "normal",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/design/design-epaulettes-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap-button",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/design/design-epaulettes-button-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap-button-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/design/design-epaulettes-button-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/design/design-epaulettes-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    /***************************************************RIGHT SIDE ***********************************************************************/

                    {
                        part: "strap",
                        type: "normal",
                        group: "designer-epaulettes",
                        group_role: "right-shoulder-strap",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/design/design-epaulettes-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "designer-epaulettes",
                        group_role: "right-shoulder-strap-button",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/design/design-epaulettes-button-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "designer-epaulettes",
                        group_role: "right-shoulder-strap-button-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/design/design-epaulettes-button-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "designer-epaulettes",
                        group_role: "right-shoulder-strap-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/design/design-epaulettes-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                "uniform-epaulettes-9.5cm": [

                    {
                        part: "strap",
                        type: "normal",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "left-shoulder-strap",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "left-shoulder-strap-button",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-button-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap-button-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-9.5cm/design-epaulettes-button-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "designer-epaulettes",
                        group_role: "left-shoulder-strap-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-9.5cm/design-epaulettes-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    /***************************************************RIGHT SIDE ***********************************************************************/

                    {
                        part: "strap",
                        type: "normal",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "right-shoulder-strap",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "right-shoulder-strap-button",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-button-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "right-shoulder-strap-button-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-button-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "uniform-epaulettes-9.5cm",
                        group_role: "right-shoulder-strap-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-9.5cm/uniform-epaulettes-9.5cm-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }
                ],

                "uniform-epaulettes-11.5cm": [

                    {
                        part: "strap",
                        type: "normal",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "left-shoulder-strap",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "left-shoulder-strap-button",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-button-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "left-shoulder-strap-button-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-button-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "left-shoulder-strap-thread",
                        sides: 2,
                        side: "left",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-thread-left.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    /***************************************************RIGHT SIDE ***********************************************************************/

                    {
                        part: "strap",
                        type: "normal",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "right-shoulder-strap",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "button",
                        type: "normal",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "right-shoulder-strap-button",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-button-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "button-thread",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "right-shoulder-strap-button-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-button-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    },

                    {
                        part: "thread",
                        type: "strap-thread",
                        group: "uniform-epaulettes-11.5cm",
                        group_role: "right-shoulder-strap-thread",
                        sides: 2,
                        side: "right",
                        model: "models/shirt/epaulettes/uniform-11.5cm/uniform-epaulettes-11.5cm-thread-right.json",
                        coordinate: {x: -0.534, y: 0.029, z: -1.255},
                        diffuse: {has_diffuse: true, diffuse_type: {}, default_diffuse_image: "rainbow", diffuse_object : {}},
                        contrast:{has_contrast: false, contrast_diffuse : {}}
                    }


                ]

            }

        }

    }
};

