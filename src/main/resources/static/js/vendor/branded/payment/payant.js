

var payant_test_url = "https://api.demo.payant.ng";

function get(url, callback) {

    $.ajax({
        url: url,
        //data: { signature: authHeader },
        type: "GET",
       // beforeSend: function(xhr){xhr.setRequestHeader('X-Test-Header', 'test-value');},
        success: function(response) { callback(response); },

        error: function(response) { callback(response); }
    });
}


function payWithPayant() {

    var handler = Payant.invoice({

        "key": "0159475a7f934d8f802a0270f22c90d06ba04ebd",

        "client": {
            "first_name": "Albert",
            "last_name": "Jane",
            "email": "jane@alberthospital.com",
            "phone": "+2348012345678"
        },
        "due_date": "12/30/2016",
        "fee_bearer": "account",
        "items": [
            {
                "item": ".Com Domain Name Registration",
                "description": "alberthostpital.com",
                "unit_cost": "2500.00",
                "quantity": "1"
            }
        ],
        callback: function(response) {

            console.log(response);

        },
        onClose: function() {
            console.log('Window Closed.');
        }
    });

    handler.openIframe();
}

function getBanks(callback) {

    get("https://api.payant.ng/banks", function (result) {

        callback(result.status,result.data);

    })
}