$(document).click(function() {

    $('.option-panel').hide();

    $('.contrast-options-panel').hide();


    hidemenus();

    //$('.option-panel').removeAttr("hidden");

});

$(".option-panel").click(function(e) {
    e.stopPropagation(); // This is the preferred method.
    return false;        // This should not be used unless you do not want
                         // any click events registering inside the div
});

function showbrandspanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#brands-options-panel').show();
}

function showfitpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#fit-options-panel').show();
}

function showfabricpanel() {
    hidemenus();

    $('.option-panel').hide();

    $('#fabric-options-panel').show();


}

function showsleevepanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#sleeve-options-panel').show();

}

function showcollarpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#collar-options-panel').show();

}

function showcuffpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#cuff-options-panel').show();

}

function showshoulderstrappanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#shoulder-strap-options-panel').show();


}

function showplacketpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#placket-options-panel').show();


}

function showpocketmenu() {

    hidemenus();

    $('.option-panel').hide();

    $('#menu-pocket').removeClass("hide");

    $('#menu-pocket').css("margin-left", "60px");

    $('#main-menu').css("margin-left", "-65px");



}

function showplacementpanel() {

    $('.option-panel').hide();

    $('#placement-options-panel').show();


}

function showstylepanel() {

    $('.option-panel').hide();

    $('#style-options-panel').show();


}

function showbackdetailpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#back-details-options-panel').show();


}

function showbottomcutpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#bottom-cut-options-panel').show();


}

function showbuttonpanel() {

    hidemenus();

    $('.option-panel').hide();

    $('#button-options-panel').show();


}

function showbuttonthreadmenu() {

    hidemenus();

    $('.option-panel').hide();

    $('#menu-button-thread').removeClass("hide");

    $('#menu-button-thread').css("margin-left", "60px");

    $('#main-menu').css("margin-left", "-65px");



}

function showbuttonholethreadpanel() {

    $('.option-panel').hide();

    $('#button-hole-thread-options-panel').show();


}

function showbuttonthreadpanel() {


    $('.option-panel').hide();

    $('#button-hole-thread-options-panel').show();


}

function showcontrastmenu() {

    hidemenus();

    $('.option-panel').hide();

    $('#menu-contrast').removeClass("hide");

    $('#menu-contrast').css("margin-left", "60px");

    $('#main-menu').css("margin-left", "-65px");


}

function hidemenus() {

    $('.option-panel').hide();

    $('.menu').css("margin-left", "-165px");

    $('#main-menu').css("margin-left", "0px");

}

function showcontrastpanel(string) {

    $('.option-panel').hide();

    $('#collar-contrast-panel').show();

    $('#contrast-title').text(string);

}